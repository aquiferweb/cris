<?php

require('header.php');

require('navmenutop.php');


if(isset($_GET['sortby'])){
	$sortby = $_GET['sortby'];
}else{
	$sortby = 'category';
}
if(isset($_GET['direction'])){
	$direction = $_GET['direction'];
}else{
	$direction = 'ASC';
}


?>

<div id=container style=''>


<?php
if(isset($_POST['ypid'])){

	$score = array();
	foreach($_POST as $key=>$value){
		if($key == "date"){
			$$key = date("Y-m-d",strtotime($value));
		}else if(substr($key,0,5) == 'score'){
			$mid = explode("_",$key);
			$mdata[$mid[1]] = array('score' => $value, 'notes' => $_POST['notes_'.$mid[1]]);
		}else{
			$$key = mres($value);
		}
	}

	$count = 0;
	$recorded_by = $_SESSION['CrisUserID'];
	foreach($mdata as $k => $v){
		$q = "INSERT INTO measures_data SET measure_id = '$k', participant_id = '$ypid', date= '$date', score = '".$v['score']."', interviewed_by = '$interviewed_by', recorded_by = '$recorded_by', notes='".$v['notes']."', project_id = '$project_id'";	
		if($r = mysql_query($q)){
			$count++;
			//echo "<p>Measure $mid = ".$v."</p>";
		}else{echo "<div class='alert alert-warning'>Problem recording measures data.".mysql_error()."</div>";}
	}
	
	echo "<div id=summary>$count measures successfully recorded</div>";

}else{
	$q = "SELECT firstname,lastname,id FROM participants ORDER BY lastname ASC";
	if($r = mysql_query($q)){
		while($yp = mysql_fetch_array($r)){
			$yplist .= "{label:'".$yp['firstname']." ".$yp['lastname']."',value: '".$yp['id']."'},";
		}
		$yplist = trim($yplist, ",");
	}else{echo "<div class='alert alert-warning'>Problem getting young people.".mysql_error()."</div>";}


?>
<style>

label.start-hidden{display:none;}
input[type=date].start-hidden{display:none;}
select.start-hidden{display:none;}
div.start-hidden{display:none;}
.session_input textarea{width:auto;}
.remove_measure{cursor:pointer;color:red;font-weight:bold;}

</style>
<form id=recordmeasure class=addnewform method=post >
	<h2>Record Measure Interview</h2>
	<div class='session_input'>
		<label>Young Person:</label>
		<input type='text' name='yp' class='form-control' id=yp />
		<input type='hidden' name='ypid' id=ypid />
		<label class='start-hidden'>Date of interview:</label>
		<input type='date' name='date' class='start-hidden date form-control' value='<?php echo date('Y-m-d',time()); ?>' /><br />
		<label class='start-hidden'>Interview carried out by</label>
		
			<?php
				//get worker / volunteers
				$q = "SELECT * FROM staff WHERE status = '1' ORDER BY lastname ASC";
				$dbworkers = array();
				if($r = mysql_query($q)){
					echo "<select id=interviewed_by name=interviewed_by class='start-hidden form-control'>";
					echo "<option selected disabled>- Select Worker -</option>";
					while($worker = mysql_fetch_array($r)){
						echo "<option value='".$worker['id']."'>".$worker['firstname']." ".$worker['lastname']."</option>";
					}
					echo "</select>";
				}else{echo "Error getting staff. ".mysql_error();}
				
			?>
		<br />
		<label class='start-hidden'>Assign to project:</label>		
		
			<?php
				//get projects
				$q = "SELECT id, name FROM projects WHERE active = '1' ORDER BY name ASC";
				$projects = array();
				if($r = mysql_query($q)){
					echo "<select id=project_id name=project_id class='start-hidden form-control'>";
					echo "<option selected disabled>- Select Project -</option>";
					while($project = mysql_fetch_array($r)){
						echo "<option value='".$project['id']."'>".$project['name']."</option>";
					}
					echo "</select>";
				}else{echo "Error getting projects. ".mysql_error();}
				
			?>
		<br />
		
	</div>	
	<div class='session_input start-hidden'>
		
		
		<?php
			$q = "SELECT * FROM measures WHERE active = 1 ORDER BY category ASC";
			$measures = array();
			if($r = mysql_query($q)){
				while($measure = mysql_fetch_array($r)){
					$measures[] = array("id" => $measure['id'], "description" => $measure['description'], "cat" => $measure['category'], "text" => $measure['measure_text']); 			
				}
			
			}else{echo "<div class='alert alert-warning'>Problem getting measures. ".mysql_error()."</div>";}
		?>
		<button type=button class='btn btn-primary btn-med hidden-print' id=clearform>Clear Form</button>
		<button type=button class='btn btn-primary btn-med hidden-print' id=uselastmeasures>Use last measures for this YP</button>
		<div id=currentmeasures>
			<table class='cre8table table'>
			<thead><tr><th>Measure</th><th>Score</th><th>Notes</th><th>Remove</th></tr></thead>
			<tbody><tr class='choosemeasure'><td colspan=4>
				<select id=choosemeasure style='width:100%;' class=hidden-print>
					<option value='' selected disabled>-- Choose a measure to add to the list --</option>
			<?php 
				foreach($measures as $k=>$v){echo "<option value='".$v['id']."'>".$v['cat']." - ".$v['text']. " - ".$v['description']."</option>";}
			?>
			</select>
			</table>
		</div>
		
		<button type=button id=printform class='btn btn-primary btn-med hidden-print' onclick='window.print();'>Print Blank Form</button><button class='btn btn-primary btn-med hidden-print' type=submit id=submitmeasures name=submitmeasures>Submit Measure Interview</button>
	</div>
	</form>	
	
	
<?php
}
?>
</div>
<?php
?>
<script src='<?php echo ROOT_PATH;?>/js/measures_functions.js'></script>
<script>
yplist = [<?echo $yplist ?>];
$('#yp').autocomplete({
		source: yplist,
		select: function( event, ui ) {
			event.preventDefault();
       		id = ui.item.value;
			$('#yp').val(ui.item.label);
			$('#ypid').val(id);
			$('.start-hidden').show();
		}
		
});
</script>
<?php
require('footer.php');
?>