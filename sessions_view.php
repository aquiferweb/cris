<?php

require('header.php');

require('navmenutop.php');



if(isset($_GET['sortby'])){
	$sortby = $_GET['sortby'];
}else{
	$sortby = 'id';
}
if(isset($_GET['direction'])){
	$direction = $_GET['direction'];
}else{
	$direction = 'ASC';
}
// Get data
//get worker / volunteers
$q = "SELECT * FROM staff WHERE status = '1' ORDER BY lastname ASC";
$dbworkers = array();
if($r = mysql_query($q)){
	while($worker = mysql_fetch_array($r)){
		$dbworkers[$worker['id']] = $worker;
	}
}else{echo "Error getting staff. ".mysql_error();}

//get yp
$q = "SELECT * FROM participants ORDER BY lastname ASC";
$yps = array();
if($r = mysql_query($q)){
	while($yp = mysql_fetch_array($r)){
		$dbyps[$yp['id']] = $yp;
	}
}else{echo "Error getting yp. ".mysql_error();}

//get projects
$q = "SELECT * FROM projects WHERE active = '1' ORDER BY name ASC";
$dbprojects = array();
if($r = mysql_query($q)){
	while($project = mysql_fetch_array($r)){
		$dbprojects[$project['id']] = $project;
	}
}else{echo "Error getting projects. ".mysql_error();}

//get session_types
$q = "SELECT session_types.id, session_types.session_name, projects.name FROM session_types INNER JOIN projects ON session_types.project_id = projects.id ORDER BY projects.name ASC";
$dbsts = array();
if($r = mysql_query($q)){
	while($st = mysql_fetch_array($r)){
		$dbsts[$st['id']] = $st;
	}
}else{echo "Error getting session types. ".mysql_error();}


if(isset($_POST['submit'])){

	foreach($_POST as $k => $v){
		if(is_array($v)){
			if(!($$k)) $$k = array();
			foreach($v as $kk => $vv){
				array_push($$k,mres($vv));
			}
		}else{
			$kk = mres($v);
		}
	}
	
	// get basic session details
	$q = "SELECT sessions.id AS sid, session_types.session_name, sessions.session_date, staff.firstname, staff.lastname, projects.name, sessions.location,
	(SELECT GROUP_CONCAT(SUBSTRING(participants.firstname,1,1),SUBSTRING(participants.lastname,1,1) SEPARATOR ',') FROM sessions 
	LEFT JOIN session_attendance ON sessions.id = session_attendance.session_id 
	LEFT JOIN participants ON session_attendance.participant_id = participants.id
	WHERE sessions.id = sid) AS yplist	
	FROM sessions
	LEFT JOIN session_types ON sessions.session_type = session_types.id
	LEFT JOIN projects ON session_types.project_id = projects.id
	LEFT JOIN staff ON sessions.staff_leader = staff.id
	";
	
	$where = "";
	if($_POST['sb_proj']){
		
		if($_POST['pvs'] == 'proj'){
			// search by project
			$str = "";
			foreach($projects as $p){
				
				$str .= " OR session_types.project_id = '$p'";
				$searchreport .= " in the ".$dbprojects[$p]['name']." project,";
			}
			$str = trim($str, " OR");
			
		}else if($_POST['pvs'] == 'sess'){
			// search by session_type
			foreach($session_types as $s){
				$str .= " OR sessions.session_type = '$s'";
				$searchreport .= " with the ".$dbsts[$s]['name']." - ".$dbsts[$s]['session_name']." session type,";
			}
			$str = trim($str, " OR");	
		}	
		if($where == ''){
			$where = " WHERE (".$str.")";
		}else{
			$where .= " AND (".$str.")";
		}
	}
	
	
	
	if($_POST['sb_date']){
		$str = "";
		if(isset($_POST['date_from']) && $_POST['date_from'] != ''){
			$str .= "sessions.session_date >= '".$_POST['date_from']."'";
			$searchreport .= " between the dates ".$_POST['date_from'];
		}else{
			echo "Please choose a valid 'from' date!";
		}
		if(isset($_POST['date_to']) && $_POST['date_to'] != ''){
			$str .= " AND sessions.session_date <= '".$_POST['date_to']."'";
			$searchreport .= " and ".$_POST['date_to'].",";
		}else{
			$str .= " AND sessions.session_date <= DATE(NOW())";
			$searchreport .= " and ".date("Y-m-d",time()).",";
		}
		if($where == ''){
			$where = " WHERE (".$str.")";
		}else{
			$where .= " AND (".$str.")";
		}
	}
	
	
	if($_POST['sb_worker'] || $_POST['sb_yp']){
	
		
		$q2 = "SELECT sessions.id AS sessid FROM sessions 
		INNER JOIN session_attendance ON sessions.id = session_attendance.session_id
		LEFT JOIN staff ON session_attendance.staff_id = staff.id
		LEFT JOIN participants ON session_attendance.participant_id = participants.id ";
	
		$where2 = "";
		$str = "";
		
		if($_POST['sb_worker']){
			$searchreport .= " where staff member(s) ";
			$sr = "";
			foreach($workers as $worker){
				$str .= " OR staff.id = '".$worker."'";
				$sr .= " or ".$dbworkers[$worker]['firstname']." ".$dbworkers[$worker]['lastname'].",";
			}
			$str = trim($str, " OR");
			if($where2 == ''){
				$where2 = " WHERE (".$str.")";
			}else{
				$where2 .= " AND (".$str.")";
			}		
			$sr = trim($sr,",");
			$sr = trim($sr," or");
			$sr .= " were present,";
			$searchreport .= $sr;
		}
		if($_POST['sb_yp']){
			
			$searchreport .= " where the young person(s) ";
			$sr = "";
			foreach($yps as $yp){
				$str .= " OR participants.id = '".$yp."'";
				$sr .= " or ".$dbyps[$yp]['firstname']." ".$dbyps[$yp]['lastname'].",";
			}
			$str = trim($str, " OR");
			if($where2 == ''){
				$where2 = " WHERE (".$str.")";
			}else{
				$where2 .= " AND (".$str.")";
			}		
			$sr = trim($sr,",");
			$sr = trim($sr," or");
			$sr .= " were present,";
			$searchreport .= $sr;
		}
		
		$q2 .= $where2;
		//echo $q2;
		if($where == ''){
			$where = " WHERE sessions.id IN (".$q2.")";
		}else{
			$where .= " AND sessions.id IN (".$q2.")";
		}	
		
		// echo table of results
		/*echo "<br /><br />".$q2;
		$r2 = mysql_query($q2) or die(mysql_error());
		echo "<table>";
		while($row = mysql_fetch_array($r2)){
			echo "<tr>";
			
			echo "<td>".$row['sessid']."</td><td>".$row['staffid']."</td><td>".$row['ypid']."</td>";
			
			echo "</tr>";
		}
		echo "</table>";*/
	}
	
	$sessions = array();
	$q = $q.$where." ORDER BY sessions.session_date DESC";
	//echo "<br /><br />".$q;
	if($r = mysql_query($q)){

		while($session = mysql_fetch_array($r)){
			$sessions[] = $session;
		}
	
	}else{ echo "Error accessing sessions. $q $where".mysql_error();}
	//$searchreport .= " --- ".$q;
	

	/*// search by worker
	$where .= " staff.id = '$staffid'";*/
	
	//INNER JOIN session_attendance ON sessions.id = session_attendance.session_id 

}

?>

<div id=container>


<div id=toolbar class=hidden-print>
	<div class=first>
		<span style="font-weight: bold;font-size: 200%;vertical-align: middle;">Sessions</span>
	</div>
	<div>
		<button class='btn btn-med btn-primary link-btn' onclick='window.print();'>Print</button>
	</div>
	<div>
		<a href="<?php echo ROOT_PATH;?>/sessions_add.php"><button class='btn btn-med btn-primary link-btn'>Record Session</button></a>
	</div>
</div>


<div class="modal fade" id=editsession role="dialog" aria-labelledby="editsessionlabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id='editsessionlabel'>View/Edit session</h4>
      </div>
      <div class="modal-body">
			<div id='summarytable' class=edittoggle></div>
			<input id=isadmin type=hidden value="<?php echo $admin; ?>" />
			<?php if($admin){
			?>
			<form id=editsessionform method=post class=edittoggle style='display:none;'>
				<input type=hidden name=editedbyid id=editedbyid value='' />
				<input type=hidden name=sessionid id=sessionid value='' />
				<?php include('sessions_form.php'); ?>
			</form>
			<?php
			}
			?>
      </div>
      <div class="modal-footer">
         <?php if($admin){echo "<button type='button' class='btn btn-primary' onclick=\"$('.edittoggle').toggle();\"><span class='edittoggle'>Edit</span><span class='edittoggle' style='display:none;'>Cancel Editing</span></button>";} ?>
        <button type="button" class="btn btn-primary" onclick="printtable();">Print</button>
        <?php if($admin){echo '<button type="button" class="btn btn-primary edittoggle" id="submiteditsession" style="display:none;">Save Changes</button>';} ?>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id=searchsessionsdiv>
	<form id=searchsessions method=post class=hidden-print>
	<fieldset>
	<legend>Show sessions:</legend>
	<div id=searchoptions>
		<label for=sb_proj>Search by Project / Session Type</label><input type=checkbox name=sb_proj id=sb_proj <?php echo $_POST['sb_proj'] ? "checked":$_POST['sb_proj']; ?> onchange="$('#pvsdiv').fadeToggle('fast');$('#pvsoptdiv').fadeToggle('fast');" />
		<span id=pvsdiv style='<?php echo $_POST['sb_proj'] ? "display:inline-block;":"display:none;"; ?>'>
			<input type=radio name=pvs id=pvs_p <?php echo ($_POST['pvs'] == 'proj' || $_POST['pvs'] == "") ? "checked":""; ?> value='proj' onChange="$('#projectdiv').toggle();$('#sessiondiv').toggle();" /> Project 
			<input type=radio name=pvs id=pvs_s <?php echo $_POST['pvs'] == 'sess' ? "checked":""; ?> value='sess' onChange="$('#projectdiv').toggle();$('#sessiondiv').toggle();" /> Session Type
		</span>
		<div id=pvsoptdiv style='<?php echo $_POST['sb_proj'] ? "display:block;":"display:none;"; ?>'>
			<div id=projectdiv class=searchopt style='<?php echo ($_POST['pvs'] == 'proj' || $_POST['pvs'] == "") ? "display:block;":"display:none;"; ?>'>
				
				<select multiple id=projects name=projects[]>
					<?php
						foreach($dbprojects as $project){
							echo "<option value='".$project['id']."' ";
							if(isset($projects)){echo in_array($project['id'],$projects) ? "selected":"";}							
							echo " >".$project['name']."</option>";
						}
					?>
				</select>
			</div>
			<div id=sessiondiv class=searchopt style='<?php echo $_POST['pvs'] == 'sess' ? "display:block;":"display:none;"; ?>'>
				
				<select multiple id=session_types name=session_types[]>
					<?php
						foreach($dbsts as $st){
							echo "<option value='".$st['id']."'";
							if(isset($session_types)){echo in_array($st['id'],$session_types) ? "selected":"";}
							echo " >".$st['name']." - ".$st['session_name']."</option>";
						}
					?>
				</select>
			</div>
		</div>
		<label for=sb_worker>Search by Worker / Volunteer</label><input type=checkbox name=sb_worker id=sb_worker <?php echo $_POST['sb_worker'] ? "checked":""; ?> onChange="$('#workerdiv').fadeToggle('fast');" /><br />
		<div id=workerdiv class=searchopt style='<?php echo $_POST['sb_worker']  ? "display:block;":"display:none;"; ?>'>
			
			<select multiple id=workers name=workers[]>
				<?php
					foreach($dbworkers as $worker){
						echo "<option value='".$worker['id']."'";
						if(isset($workers)){echo in_array($worker['id'],$workers) ? "selected":"";}
						echo " >".$worker['firstname']." ".$worker['lastname']."</option>";
					}
				?>
			</select>
		</div>
		<label for=sb_yp>Search by Young Person</label><input type=checkbox name=sb_yp id=sb_yp <?php echo $_POST['sb_yp'] ? "checked":""; ?> onChange="$('#ypdiv').fadeToggle('fast');"  /><br />
		<div id=ypdiv class=searchopt style='<?php echo $_POST['sb_yp']  ? "display:block;":"display:none;"; ?>'>
			
			<select multiple id=yps name=yps[]>
				<?php
					foreach($dbyps as $yp){
						echo "<option value='".$yp['id']."'";
						if(isset($yps)){echo in_array($yp['id'],$yps) ? "selected":"";}
						echo " >".$yp['firstname']." ".$yp['lastname']."</option>";
					}
				?>
			</select>
		</div>
		<label for=sb_date>Search between dates</label><input type=checkbox name=sb_date id=sb_date <?php echo $_POST['sb_date'] ? "checked":""; ?> onChange="$('#datediv').fadeToggle('fast');"  /><br />
		<div id=datediv class=searchopt style='<?php echo $_POST['sb_date']  ? "display:block;":"display:none;"; ?>'>
			
			<label for=date_from>From:</label><input name=date_from id=date_from class=date type=date value="<? echo isset($_POST['date_from']) ? date('Y-m-d',strtotime($_POST['date_from'])) : date('Y-m-d',strtotime("-2 weeks"));?>" /><br />
			<label for=date_to>To:</label><input name=date_to id=date_to class=date type=date value="<? echo isset($_POST['date_to']) ? date('Y-m-d',strtotime($_POST['date_to'])) : date('Y-m-d');?>" /><br />
		</div>
		<div style='text-align:center;'><button type=submit class='btn btn-med btn-primary'>Submit</button></div>
		<input type=hidden name=submit value=1 />
	</div>
	
	
	<div class=floatbreak></div>
	
	</fieldset>
	</form>
	
	<div id=showresults>
		<?php
		if($_POST['submit'] == 1){
			$searchreport = trim($searchreport,",");
			echo "<p>Searched for sessions ".$searchreport.".</p>";
			//echo "<p>Query was $q.</p><br />";
			
			if(count($sessions) > 0){	
				echo "<p><b>".count($sessions)." sessions found</b> <br />Click on a session to view the full details.</p>";
				echo "<table class=cre8table id=sessionresults>";
				echo "<tr><th>ID</th><th>Project</th><th>Session</th><th>Date</th><th>Led by</th><th>YP</th><th>Location</th></tr>";
				foreach($sessions as $session){
					echo "<tr class='clickable_session' id='session_".$session['sid']."'><td>".$session['sid']."</td><td>".$session['name']."</td><td>".$session['session_name']."</td><td>".date('d/m/y',strtotime($session['session_date']))."</td><td>".$session['firstname']." ".$session['lastname']."</td><td>".$session['yplist']."</td><td>".$session['location']."</td></tr>";
				}
				echo "</table>";
			}else{
				echo "<p><b>No sessions found!</b></p>";
			}
			
		}
		?>
	</div>
</div>


<div class=floatbreak></div>
</div>

<script src='<?php echo ROOT_PATH;?>/js/sessions_functions.js'></script>
<?php
require('footer.php');
?>