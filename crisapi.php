<?php
session_start();
require('config_cris.php');
$login = check_login();
dbconnect();
$adminflag = check_admin();

if(isset($_REQUEST['q'])){
	$action = $_REQUEST['q'];
	//echo $q;
	switch($action){
	
	// ****************************************
		case 'getstaff':
			foreach($_POST as $key=>$value){
					$$key = mres($value);				
			}
			$q = "SELECT staff.id,staff.firstname,staff.lastname,staff.username,staff.password,staff.admin,staff.staff,staff.cre8works,staff.volunteer,staff.role,staff.timestamp,staff.date_started,staff.line_manager,staff.status,staff.category,staff.job_title,staff.address,staff.postcode,staff.telephone,staff.mobile,staff.email,staff.app_form_date,staff.app_form_seen_by,staff.selfdec_form_date,staff.selfdec_form_seen_by, staff.interview_date, staff.interview_staff, staff.dbs_number, staff.dbs_expiry_date, staff.ref1_date, staff.ref1_seen_by, staff.ref2_date, staff.ref2_seen_by, staff.vocational_quals, staff.induction, staff.agreement_given, staff.driving_licence, staff.qualifications, staff.experience, staff.health_notes, staff.notes, lm.firstname AS lmfn, lm.lastname AS lmln
			FROM staff 
			LEFT JOIN staff AS lm ON lm.id = staff.line_manager 
			WHERE staff.id = '$id'";
			//	id	firstname	lastname	username	password	last_logged_in	admin	staff	cre8works	volunteer	role	timestamp	date_started	line_manager	status 0 - inactive, 1 - active, 2 - expressed interest	category	job_title	projects	address	postcode	telephone	mobile	email	app_form_date	app_form_seen_by	selfdec_form_date	selfdec_form_seen_by	interview_date	interview_staff	dbs_number	dbs_expiry_date	ref1_date	ref1_seen_by	ref2_date	ref2_seen_by	vocational_quals	induction	agreement_given	driving_licence	qualifications	experience	health_notes	notes

			if($r = mysql_query($q)){
				$record = mysql_fetch_array($r);
				
				$staff = ($record['staff'] ? 'Yes':'No');
				$vol = ($record['volunteer'] ? 'Yes':'No');
				$driving_licence = ($record['driving_licence'] ? 'Yes':'No');
				$induction = ($record['induction'] ? 'Yes':'No');
				switch($record['status']){
					case 0: $status = 'Inactive'; break;
					case 1: $status = 'Active'; break;
					case 2: $status = 'Expressed Interest'; break;
				}
				$record['summary'] = "
					<table class=table id=summarytable><tbody>
					<tr><td class=field>Name:</td><td>".$record['firstname']." ".$record['lastname']."</td><td class=field>Status:</td><td>".$status."</td></tr>
					<tr><td class=field>Staff:</td><td>".$staff."</td><td class=field>Volunteer:</td><td>".$vol."</td></tr>
					<tr><td class=field>Date Started:</td><td>".date('d/m/y',strtotime($record['date_started']))."</td><td class=field>Line Manager:</td><td>".$record['lmfn']." ".$record['lmln']."</td></tr>
					<tr><td class=field>Email:</td><td>".$record['email']."</td><td class=field>Job Title:</td><td>".$record['job_title']."</td></tr>
					<tr><td class=field>Phone:</td><td>".$record['telephone']."</td><td class=field>Mobile:</td><td>".$record['mobile']."</td></tr>
					<tr><td class=field>Address:</td><td colspan=3>".$record['address']." ".$record['postcode']."</td></tr>
					<tr><td class=field>Application Date:</td><td>".date('d/m/y',strtotime($record['app_form_date']))."</td><td class=field>Seen by:</td><td>".$record['app_form_seen_by']."</td></tr>
					<tr><td class=field>Self Declaration Date:</td><td>".date('d/m/y',strtotime($record['selfdec_form_date']))."</td><td class=field>Seen by:</td><td>".$record['selfdec_form_seen_by']."</td></tr>
					<tr><td class=field>Reference 1 Date:</td><td>".date('d/m/y',strtotime($record['ref1_date']))."</td><td class=field>Ref 1 Seen by:</td><td>".$record['ref1_seen_by']."</td></tr>
					<tr><td class=field>Reference 2 Date:</td><td>".date('d/m/y',strtotime($record['ref2_date']))."</td><td class=field>Ref 2 Seen by:</td><td>".$record['ref2_seen_by']."</td></tr>
					<tr><td class=field>DBS number:</td><td>".$record['dbs_number']."</td><td class=field>DBS Expiry:</td><td>".date('d/m/y',strtotime($record['dbs_expiry_date']))."</td></tr>
					<tr><td class=field>Driving Licence Seen:</td><td>".$driving_licence."</td><td class=field>Induction:</td><td>".$induction."</td></tr>
					<tr><td class=field>Medical Details:</td><td colspan=3>".$record['health_notes']."</td></tr>
					<tr><td class=field>Notes:</td><td colspan=3>".$record['notes']."</td></tr></tbody></table>";
					//<tr><td class=field>Interview Date:</td><td>".date('d/m/y',strtotime($record['interview_date']))."</td><td class=field>Interview by:</td><td>".$record['interview_staff']."</td></tr>
				echo json_encode($record);
			}else{
				echo "failed to get resource. ".mysql_error();
			}
				
		break;

	// ****************************************
	
		case 'getyp':
			foreach($_POST as $key=>$value){
					$$key = mres($value);				
			}
			$q = "SELECT 
			participants.id,
			participants.dob,
			participants.start_date,
			participants.firstname,
			participants.lastname,
			participants.address,
			participants.postcode,
			participants.area,
			participants.yp_phone,
			participants.guardian_name,
			participants.school_year,
			participants.school,
			participants.gender,
			participants.guardian_signature_date,
			participants.guardian_phone,
			participants.photo_consent,
			participants.facebook_consent,
			participants.health_form_received,
			participants.date_health_form_received,
			participants.medical,
			participants.allergies,
			participants.notes,
			participants.caf,
			participants.key_worker,
			participants.referrer,
			staff.firstname AS kwfn,
			staff.lastname AS kwln 
			FROM participants
			LEFT JOIN staff ON staff.id = participants.key_worker		 
			WHERE participants.id = '$id'";
			if($r = mysql_query($q)){
				$record = mysql_fetch_array($r);
				
				if($table == 'participants'){
					// get consented projects
					if($res = mysql_query("SELECT projects.name, projects_consented.date, projects_consented.id AS cid, projects.id AS pid 
					FROM projects_consented 
					LEFT JOIN projects ON projects.id = projects_consented.project_id 
					WHERE projects_consented.participant_id = '$id' 
					ORDER BY projects_consented.date DESC")){
						while($row = mysql_fetch_array($res)){
							$record['projects_consented'][] = $row;
						}
					
					}else{
						echo "Failed to get consentlist ".mysql_error();
					}
					
					// get interested projects
					if($res = mysql_query("SELECT projects.name, projects_interested.date, projects_interested.id AS cid, projects.id AS pid 
					FROM projects_interested
					LEFT JOIN projects ON projects.id = projects_interested.project_id 
					WHERE projects_interested.participant_id = '$id' 
					ORDER BY projects_interested.date DESC")){
						while($row = mysql_fetch_array($res)){
							$record['projects_interested'][] = $row;
						}
					
					}else{
						echo "Failed to get consentlist ".mysql_error();
					}

				
				}
				
				$record['summary'] = "";
				
					//id	crypt_name	dob	start_date	firstname	lastname	address	postcode	area	yp_phone	guardian_name	school_year	school	gender	guardian_signature_date	guardian_phone	photo_consent	facebook_consent	health_form_received	medical	allergies	notes	issues	caf	key_worker	referrer
					
					// build summary table
				$age = floor((time() - strtotime($record['dob'])) / (60*60*24*365.25));
				$caf = ($record['caf'] ? 'Yes':'No');
				$health_form_received = ($record['health_form_received'] ? 'Yes':'No');
				$facebook_consent = ($record['facebook_consent'] ? 'Yes':'No');
				$photo_consent = ($record['photo_consent'] ? 'Yes':'No');
				$dhfr = ($record['date_health_form_received'] != '0000-00-00' ? "- ".date('d/m/y',strtotime($record['date_health_form_received'])):"- date unknown");
				if(!$record['health_form_received']) $dhfr = "";
				if(date("n") > 8){$sep_year = date('Y');}else{$sep_year = date('Y')-1;}
				$year = floor(((strtotime("1st Sep ".$sep_year)-strtotime($record['dob']))/(60*60*24*365.25))-4);
				$summary = "
					<table class=table id=summarytable><tbody>
					<tr><td class=field>Name:&nbsp;&nbsp;<button class=hidden-print onClick='$(this).parent().next(\"td\").toggle();' style='font-size:10px'>Show / Hide</button></td><td>".$record['firstname']." ".$record['lastname']."</td><td class=field>Gender:&nbsp;&nbsp;<button class=hidden-print onClick='$(this).parent().next(\"td\").toggle();' style='font-size:10px'>Show / Hide</button></td><td>".$record['gender']."</td></tr>
					<tr><td class=field>DOB:&nbsp;&nbsp;<button class=hidden-print onClick='$(this).parent().next(\"td\").toggle();' style='font-size:10px'>Show / Hide</button></td><td>".date('d/m/y',strtotime($record['dob']))."</td><td class=field>Age:&nbsp;&nbsp;<button class=hidden-print onClick='$(this).parent().next(\"td\").toggle();' style='font-size:10px'>Show / Hide</button></td><td>".$age."</td></tr>
					<tr><td class=field>School Year:&nbsp;&nbsp;<button class=hidden-print onClick='$(this).parent().next(\"td\").toggle();' style='font-size:10px'>Show / Hide</button></td><td>Year ".($year + $record['school_year'])."</td><td class=field>Area:&nbsp;&nbsp;<button class=hidden-print onClick='$(this).parent().next(\"td\").toggle();' style='font-size:10px'>Show / Hide</button></td><td>".$record['area']."</td></tr>
					<tr><td class=field>Guardian Name:&nbsp;&nbsp;<button class=hidden-print onClick='$(this).parent().next(\"td\").toggle();' style='font-size:10px'>Show / Hide</button></td><td colspan=3>".$record['guardian_name']."</td></tr>				
					<tr><td class=field>YP phone:&nbsp;&nbsp;<button class=hidden-print onClick='$(this).parent().next(\"td\").toggle();' style='font-size:10px'>Show / Hide</button></td><td>".$record['yp_phone']."</td><td class=field>Guardian phone:&nbsp;&nbsp;<button class=hidden-print onClick='$(this).parent().next(\"td\").toggle();' style='font-size:10px'>Show / Hide</button></td><td>".$record['guardian_phone']."</td></tr>
					<tr><td class=field>Address&nbsp;&nbsp;<button class=hidden-print onClick='$(this).parent().next(\"td\").toggle();' style='font-size:10px'>Show / Hide</button>:</td><td colspan=3>".$record['address']." ".$record['postcode']."</td></tr>
					<tr><td class=field>Start Date:&nbsp;&nbsp;<button class=hidden-print onClick='$(this).parent().next(\"td\").toggle();' style='font-size:10px'>Show / Hide</button></td><td>".date('d/m/y',strtotime($record['start_date']))."</td><td class=field>Key Worker:&nbsp;&nbsp;<button class=hidden-print onClick='$(this).parent().next(\"td\").toggle();' style='font-size:10px'>Show / Hide</button></td><td>".$record['kwfn']." ".$record['kwln']."</td></tr>
					<tr><td class=field>Health Form:&nbsp;&nbsp;<button class=hidden-print onClick='$(this).parent().next(\"td\").toggle();' style='font-size:10px'>Show / Hide</button></td><td colspan=3>$health_form_received $dhfr</td></tr>
					<tr><td class=field>Facebook consent:&nbsp;&nbsp;<button class=hidden-print onClick='$(this).parent().next(\"td\").toggle();' style='font-size:10px'>Show / Hide</button></td><td>$facebook_consent</td><td class=field>Photo Consent:&nbsp;&nbsp;<button class=hidden-print onClick='$(this).parent().next(\"td\").toggle();' style='font-size:10px'>Show / Hide</button></td><td>$photo_consent</td></tr>
					<tr><td class=field>Medical Details:&nbsp;&nbsp;<button class=hidden-print onClick='$(this).parent().next(\"td\").toggle();' style='font-size:10px'>Show / Hide</button></td><td colspan=3>".nl2br($record['medical'])."</td></tr>
					<tr><td class=field>Allergies:&nbsp;&nbsp;<button class=hidden-print onClick='$(this).parent().next(\"td\").toggle();' style='font-size:10px'>Show / Hide</button></td><td colspan=3>".nl2br($record['allergies'])."</td></tr>
					<tr><td class=field>Notes:&nbsp;&nbsp;<button class=hidden-print onClick='$(this).parent().next(\"td\").toggle();' style='font-size:10px'>Show / Hide</button></td><td colspan=3>".nl2br($record['notes'])."</td></tr>
					<tr><td class=field>Referrer:&nbsp;&nbsp;<button class=hidden-print onClick='$(this).parent().next(\"td\").toggle();' style='font-size:10px'>Show / Hide</button></td><td colspan=3>".$record['referrer']."</td></tr>
					<tr><td class=field>Registered Projects:&nbsp;&nbsp;<button class=hidden-print onClick='$(this).parent().next(\"td\").toggle();' style='font-size:10px'>Show / Hide</button></td><td colspan=3>";
				if($record['projects_consented']){
					foreach($record['projects_consented'] as $project){$summary .= $project['name']." - ".$project['date']."<br />";}
				}
				$summary .= "</td></tr>";
				$summary .= "<tr><td class=field>Expressed Interest:&nbsp;&nbsp;<button class=hidden-print onClick='$(this).parent().next(\"td\").toggle();' style='font-size:10px'>Show / Hide</button></td><td colspan=3>";
				if($record['projects_interested']){
					foreach($record['projects_interested'] as $project){$summary .= $project['name']." - ".$project['date']."<br />";}
				}
					
				$summary .= "</td></tr>	</tbody></table>";

				$record['summary'] = $summary;
				echo json_encode($record);
			}else{
				echo "failed to get resource. ".mysql_error();
			}
		
		break;
	// ****************************************
		case 'getcontact':
			$table = mysql_real_escape_string($_POST['table']);
			$id = mysql_real_escape_string($_POST['id']);
			$idfield =  mysql_real_escape_string($_POST['idffield']);
			$q = "SELECT contacts.id, contacts.title, contacts.firstname, contacts.lastname ,contacts.organisation, contacts.website, contacts.email, contacts.address, contacts.postcode, contacts.telephone, contacts.job_title, contacts.notes, contacts.category, contact_type.type FROM contacts LEFT JOIN contact_type ON contact_type.id = contacts.category WHERE contacts.id = '$id'";
			if($r = mysql_query($q)){
				$record = mysql_fetch_array($r);
				//	id	title	firstname	lastname	organisation	website	email	address	address2	address3	postcode	telephone	job_title	category	notes
				$summary = "
					<table class=table id=summarytable><tbody>
					<tr><td class=field>Title:</td><td>".$record['title']."</td><td class=field>Name:</td><td>".$record['firstname']." ".$record['lastname']."</td></tr>
					<tr><td class=field>Organisation:</td><td>".$record['organisation']."</td><td class=field>Website:</td><td>".$record['website']."</td></tr>
					<tr><td class=field>Email:</td><td>".$record['email']."</td><td class=field>Phone:</td><td>".$record['telephone']."</td></tr>
					<tr><td class=field>Address:</td><td colspan=3>".$record['address']." ".$record['postcode']."</td></tr>
					<tr><td class=field>Notes:</td><td colspan=3>".nl2br($record['notes'])."</td></tr>";
				$summary .= "<tr><td class=field>Categories:</td><td colspan=3>";
				if(is_integer($record['category']) || $record['category'] == ""){
					$summary .= $record['category'] == "" ? "none selected" : $record['type'];
				}else{	
					
					$categories = explode("::",$record['category']);
					$categories = implode(",",$categories);
					$query = "SELECT contact_type.type FROM contact_type WHERE id IN($categories)";
					$result = mysql_query($query);
					while($ctype = mysql_fetch_array($result)){$summary .= $ctype['type']."<br />";}
				}

				$summary .= "</td></tr>	</tbody></table>";
				$record['summary'] = $summary;
				echo json_encode($record);
			}else{
				echo "failed to get resource. ".mysql_error();
			}
			break;
	// ****************************************
		case 'deleterow':
			//echo "blob ".$adminflag;
			if($adminflag){
				foreach($_POST as $key=>$value){
						$$key = mres($value);				
				}
				$q = "DELETE FROM $tablename WHERE id = '$id' LIMIT 1";
				if($r = mysql_query($q)){
					echo "Deleted record id $id from $tablename";
				}else{
					echo "Failed to delete record id $id from $tablename Full query was $q - error is ".mysql_error();
				}
				if($tablename == 'participants'){
					$q = "DELETE FROM projects_consented WHERE participant_id = '$id'";
					if($r = mysql_query($q)){
						echo "Deleted participant consent records. ";
					}else{
						echo "Failed to delete participant consent records Full query was $q - error is ".mysql_error();
					}
					
					$q = "DELETE FROM projects_interested WHERE participant_id = '$id'";
					if($r = mysql_query($q)){
						echo "Deleted participant interest records. ";
					}else{
						echo "Failed to delete participant interest records Full query was $q - error is ".mysql_error();
					}	
				}
			}else{
				echo "Not allowed. User not authorised to delete records.";
			}
			
		break;

		
	// ****************************************
		case 'getlateststaff':	
			foreach($_POST as $key=>$value){
					$$key = mres($value);			
			}
			// get latest 4 sessions
			$r = mysql_query("SELECT id,staff_leader FROM sessions WHERE session_type = '$sessiontypeid' ORDER BY session_date DESC LIMIT 4");
			$gotleader = false;
			$sessionstr = "";
			while($sessions = mysql_fetch_array($r)){
				$sessionstr .= ", '".$sessions['id']."'";
				if(!$gotleader) $leader = $sessions['staff_leader'];
			}
			$sessionstr = "(".trim($sessionstr,", ").")";
			if($sessionstr != "()"){
				// get participant ids from latest session
				$q = "	SELECT staff.id,staff.firstname, staff.lastname FROM staff
						INNER JOIN session_attendance ON session_attendance.staff_id = staff.id 
						WHERE session_attendance.session_id IN $sessionstr
						AND session_attendance.staff_id != '0' ORDER BY firstname ASC";
				if($r = mysql_query($q)){
					while($yp = mysql_fetch_array($r)){		
						$pids[] = array('id'=>$yp['id'],'name'=>$yp['firstname']." ".$yp['lastname']);	
					}
					$pids = super_unique($pids,'id');
					$return['leader'] = $leader;
					$return['staff'] = $pids;
					echo json_encode($return);	
				}else{
					echo json_encode("failed to get resource. ".mysql_error());
					exit;
				}
			}else{
				echo json_encode("no sessions found");
			}
		break;
		
// ****************************************
		case 'getconsented':	
		
			foreach($_POST as $key=>$value){
					$$key = mres($value);				
			}

			// get all consented yp from a project
			$q = "	SELECT participants.id,participants.firstname, participants.lastname, projects_consented.project_id FROM participants 
					INNER JOIN projects_consented ON projects_consented.participant_id = participants.id
					WHERE projects_consented.project_id = (SELECT project_id FROM session_types WHERE id = '$sessiontypeid' LIMIT 1)
					ORDER BY firstname ASC
					";
			if($r = mysql_query($q)){
				while($yp= mysql_fetch_array($r)){
					$pids[] = array('id'=>$yp['id'],'name'=>$yp['firstname']." ".$yp['lastname'],'project'=>$yp['project_id']);	
				}		
				$pids = super_unique($pids,'id');
				echo json_encode($pids);
			}else{
				echo "failed to get resource. ".mysql_error();
				
			}
		break;
		
// *********************************************
		case "getlatest":
			foreach($_POST as $key=>$value){
					$$key = mres($value);			
			}
			// get latest 5 sessions
			$r = mysql_query("SELECT id FROM sessions WHERE session_type = '$sessiontypeid' ORDER BY session_date DESC LIMIT 4");
			$sessionstr = "";
			while($sessions = mysql_fetch_array($r)){
				$sessionstr .= ", '".$sessions['id']."'";
			}
			$sessionstr = "(".trim($sessionstr,", ").")";		
			if($sessionstr != "()"){
				// get participant ids from latest session
				$q = "	SELECT participants.id,participants.firstname, participants.lastname FROM participants 
						INNER JOIN session_attendance ON session_attendance.participant_id = participants.id 
						WHERE session_attendance.session_id IN $sessionstr
						AND session_attendance.participant_id != '0' ORDER BY firstname ASC";
				if($r = mysql_query($q)){
					while($yp = mysql_fetch_array($r)){		
						$pids[] = array('id'=>$yp['id'],'name'=>$yp['firstname']." ".$yp['lastname']);	
					}
					$pids = super_unique($pids,'id');
					echo json_encode($pids);	
				}else{
					echo "failed to get resource. ".mysql_error();				
				}
			}else{
				echo json_encode("no sessions found");
			}
		break;
		

// *********************************************
		case "getlastmeasures":
			foreach($_POST as $key=>$value){
					$$key = mres($value);			
			}
			// get latest measures
			$r = mysql_query("SELECT measure_id,date FROM measures_data WHERE date = (SELECT MAX(date) FROM measures_data WHERE participant_id = '$ypid') AND participant_id = '$ypid'");
			while($measure = mysql_fetch_array($r)){
				$measures[] = array("mid" =>$measure['measure_id'],"date" => $measure['date']);
				//$last_date = $measure['date'];
			}
			//$measures['last_recorded'] = $last_date;
			echo json_encode($measures);	
				
		break;
		
// *********************************************
		case "showypmeasures":
			foreach($_POST as $key=>$value){
					$$key = mres($value);			
			}

			// get measures ordered by date
			$r = mysql_query("SELECT yp.firstname AS ypfn, yp.lastname AS ypln, measures_data.measure_id,measures_data.date,measures_data.score,measures.measure_text,measures.description,measures.category, rstaff.firstname AS rfirstname, rstaff.lastname AS rlastname,istaff.firstname  AS ifirstname, istaff.lastname AS ilastname, measures_data.notes 
			FROM measures_data 
			LEFT JOIN measures ON measures.id = measures_data.measure_id 
			LEFT JOIN staff AS rstaff ON measures_data.recorded_by = rstaff.id
			LEFT JOIN staff AS istaff ON measures_data.interviewed_by = istaff.id 
			LEFT JOIN participants AS yp ON measures_data.participant_id = yp.id
			WHERE participant_id = '$ypid' ORDER BY date DESC, measure_id ASC") or die(mysql_error());
			while($measure = mysql_fetch_array($r)){
				$dmeasures[] = $measure;
				//$last_date = $measure['date'];
				$dates[] = $measure['date'];
			}
			
			
			// get latest measures by id
			$r = mysql_query("SELECT yp.firstname AS ypfn, yp.lastname AS ypln, measures_data.measure_id,measures_data.date,measures_data.score,measures.measure_text,measures.description,measures.category, rstaff.firstname AS rfirstname, rstaff.lastname AS rlastname,istaff.firstname  AS ifirstname, istaff.lastname AS ilastname, measures_data.notes 
			FROM measures_data 
			LEFT JOIN measures ON measures.id = measures_data.measure_id 
			LEFT JOIN staff AS rstaff ON measures_data.recorded_by = rstaff.id 
			LEFT JOIN staff AS istaff ON measures_data.interviewed_by = istaff.id 
			LEFT JOIN participants AS yp ON measures_data.participant_id = yp.id
			WHERE participant_id = '$ypid' ORDER BY measure_id ASC, date ASC") or die(mysql_error());
			while($measure = mysql_fetch_array($r)){
				$measures[] = $measure;
				//$last_date = $measure['date'];
			}
			
			
			$dataset = array_unique($dates);
			$return['m'] = $measures;
			$return['d'] = $dmeasures;
			//$measures['last_recorded'] = $last_date;
			echo json_encode($return);	
				
		break;
	
	
// *********************************************
		case "getsessiontypes":
			$projectid = $_POST['projectid'];
			$stypes = array();
			$q = "SELECT * FROM session_types WHERE project_id = '$projectid'";
			if($r = mysql_query($q)){
				while($stype = mysql_fetch_array($r)){
					$stypes[] = array('id'=> $stype['id'], 'session_name'=>$stype['session_name'],'group_activity'=>$stype['group_activity']);
				}
				echo json_encode($stypes);
			}else{
				echo json_encode("error - failed to get session types. ".mysql_error());
			}
			
				
		break;
		
// *********************************************
		case "getsession":
			$sid = $_POST['id'];
			

			
			$q = "SELECT 
			projects.id AS projectid,
			projects.name,
			session_types.session_name, 
			sessions.user_id AS recbyid, 
			recby.firstname AS recbyfn, 
			recby.lastname AS recbyln, 
			sessions.staff_leader AS sleaderid, 
			sleader.firstname AS sleaderfn, 
			sleader.lastname AS sleaderln,
			sessions.session_type,
			sessions.session_date,	
			sessions.session_end_date,	
			sessions.session_start_time, 
			sessions.session_end_time,	
			sessions.location,	
			sessions.notes, 
			sessions.action,	
			sessions.action_text,	
			sessions.timestamp,	
			sessions.notify,	
			sessions.sg, 
			sessions.sgdetails
			FROM sessions 
			LEFT JOIN session_types ON session_types.id = sessions.session_type
			LEFT JOIN staff AS recby ON recby.id = sessions.user_id
			LEFT JOIN staff AS sleader ON sleader.id = sessions.staff_leader
			LEFT JOIN projects ON projects.id = session_types.project_id
			WHERE sessions.id = '$sid' LIMIT 1";
			
			if($r = mysql_query($q)){
				$session = mysql_fetch_array($r);
				foreach($session as $k=>$v){
					$session[$k] = stripslashes($v);
				}
				$yps = array();
				$workers = array();
				$q = "
				SELECT session_attendance.participant_id, 
				session_attendance.staff_id, 
				participants.lastname AS ypln,
				participants.firstname AS ypfn,
				staff.firstname AS stafffn,
				staff.lastname AS staffln
				FROM session_attendance 
				LEFT JOIN staff ON staff.id = session_attendance.staff_id
				LEFT JOIN participants ON participants.id = session_attendance.participant_id
				WHERE session_attendance.session_id = '$sid'";
				if($r = mysql_query($q)){
					while($s = mysql_fetch_array($r)){
						if($s['participant_id'] != '0') $yps[] = array('ypid' => $s['participant_id'], 'ypfn' => $s['ypfn'], 'ypln' => $s['ypln']);
						if($s['staff_id'] != '0') $workers[] = array('staffid' => $s['staff_id'], 'stafffn' => $s['stafffn'], 'staffln' => $s['staffln']);
					}
					$session['yps'] = $yps;
					$session['workers'] = $workers;
				}else{
					echo json_encode("error - failed to get attendance. ".mysql_error());
				}
				
				
				$stypes = array();
				$q = "SELECT * FROM session_types WHERE project_id = '".$session['projectid']."'";
				if($r = mysql_query($q)){
					while($stype = mysql_fetch_array($r)){
						$stypes[] = array('id'=> $stype['id'], 'session_name'=>$stype['session_name'],'group_activity'=>$stype['group_activity']);
					}
					$session['sister_sessiontypes'] = $stypes;
				}else{
					echo json_encode("error - failed to get session types. ".mysql_error());
				}
				
				$uploaded_files = array();
				/*
				$q = "SELECT * FROM uploaded_files WHERE session_id = '$sid'";
				if($r = mysql_query($q)){
					while($file = mysql_fetch_array($r)){
						$uploaded_files[] = array('id'=>$file['id'],'filename'=> $file['filename'], 'type'=>$file['filetype'], 'date' => $file['date_uploaded'],'notes'=>$file['notes']);
					}
					$session['uploaded_files'] = $uploaded_files;
				}else{
					echo json_encode("error - failed to get uploaded files. ".mysql_error());
				}*/
				
				// build summary table
				$sgfollow = $session['sg'] ? "YES -":"NO";
				$actions = $session['action'] ? $session['action_text'] : "None";
				$summary = "<table class=table id=summarytable><tbody>
					<tr><td class=field>Project:</td><td>".$session['name']."</td><td class=field>Session Type:</td><td>".$session['session_name']."</td></tr>
					<tr><td class=field>Start Date:</td><td>".date('d/m/y',strtotime($session['session_date']))." ".$session['session_start_time']."</td><td class=field>End Date:</td><td>".date('d/m/y',strtotime($session['session_end_date']))." ".$session['session_end_time']."</td></tr>
					<tr><td class=field>Location:</td><td colspan=3>".$session['location']."</td></tr>
					<tr><td class=field>Session Leader:</td><td>".$session['sleaderfn']." ".$session['sleaderln']."</td><td class=field>Recorded By:</td><td>".$session['recbyfn']." ".$session['recbyln']."<br />".$session['timestamp']."</td></tr>
					<tr><td class=field>Staff:</td><td>".count($workers)."</td><td class=field>Young People:</td><td>".count($yps)."</td></tr>
					<tr><td colspan=2>";
				foreach($workers as $staff){$summary .= $staff['stafffn']." ".$staff['staffln']."<br/>";}
				$summary .="</td><td colspan=2>";
				foreach($yps as $yp){$summary .= $yp['ypfn']." ".$yp['ypln']."<br/>";}
				$summary .= "</td></tr>
					<tr><td class=field>Notes:</td><td colspan=3>".$session['notes']."</td></tr>
					<tr><td class=field>Uploads:</td><td colspan=3>";
				foreach($uploaded_files as $fl){$summary .= "<a href='uploads/".$fl['filename']."' target='_blank'>".$fl['filename']." ".$fl['type']."</a><br />";}
				foreach($uploaded_files as $fl){$ft = explode('/',$fl['type']); if(strcmp($ft[0], 'image') == 0){ $summary .= "<a href='uploads/".$fl['filename']."' target='_blank'><img src='uploads/".$fl['filename']."' class=session_img /></a>";}}
				
				$summary .= "</td></tr>						
					<tr><td class=field>Actions:</td><td colspan=3>".$actions."</td></tr>	
					<tr><td class=field>Safeguarding:</td><td colspan=3>$sgfollow ".$session['sgdetails']."</td></tr>
				</tbody></table>
				";
				$session['summarytable'] = $summary;
			}else{
				echo json_encode("error - failed to get session. ".mysql_error());
			}
			
			// get yp who are registered for project
			
			// get all consented yp from a project
			$q = "	SELECT participants.id,participants.firstname, participants.lastname, projects_consented.project_id FROM participants 
					INNER JOIN projects_consented ON projects_consented.participant_id = participants.id
					WHERE projects_consented.project_id = (SELECT project_id FROM session_types WHERE id = '".$session['session_type']."' LIMIT 1)
					ORDER BY firstname ASC
					";
			if($r = mysql_query($q)){
				
				while($yp = mysql_fetch_assoc($r)){
					$pids[] = array('id'=>$yp['id'],'name'=>$yp['firstname']." ".$yp['lastname'],'project'=>$yp['project_id']);	
					
				}	
				

				$pids = super_unique($pids,'id');
				$session['registered'] = $pids;
			}else{
				echo json_encode("failed to get resource. ".mysql_error());
				
			}
			
			echo json_encode($session);
				
		break;
// ********************************************************

		case "editsession":
		
			foreach($_POST as $key=>$value){
				if($key == "session_date" || $key == 'session_end_date'){
					$$key = date("Y-m-d",strtotime($value));
				}else{
					$$key = mres($value);
				}
			}
			

			// edit session

			$q = "UPDATE sessions SET ".
			"editedby = '".$_SESSION['CrisUserID']."', ".
			"editeddate = '".date("Y-m-d H:i:s")."', ".
			"session_type = '$session_type', ".
			"staff_leader = '$staff_leader', ".
			"session_date = '$session_date', ".
			"session_end_date = '$session_end_date', ".
			"session_start_time = '$session_start_time', ".
			"session_end_time = '$session_end_time', ".
			"location = '$location', ".
			"notes = '$notes', ".
			"action = '$actionrequired', ".
			"action_text = '$actions', ".
			"sg = '$sg', ".
			"sgdetails = '$sgdetails'
			WHERE sessions.id = '$sessionid'";

			if($r = mysql_query($q)){
				echo "Session Successfully edited. $q";
			}else{
				echo "Error when recording session. ".mysql_error();
				exit;
			}
			
			// handle file uploads
			
			$file_uploaded = false;
			$uploaded_files = array();
			$i = 1;
			$q = "SELECT sessions.id, session_types.session_name, sessions.session_date, staff.firstname, staff.lastname, projects.name, sessions.location  FROM sessions
			LEFT JOIN session_types ON sessions.session_type = session_types.id
			LEFT JOIN projects ON session_types.project_id = projects.id
			LEFT JOIN staff ON sessions.staff_leader = staff.id
			WHERE sessions.id = '$session_id'
			";
			if($r = mysql_query($q)){	$session_info = mysql_fetch_array($r);}else{ echo "ARG ".mysql_error();}

			while($_FILES['upload_'.$i]['name']){

				if(!$_FILES['upload_'.$i]['error']){
					// no errors
					//print_r($session_info);
					//now is the time to modify the future file name and validate the file
					$new_file_name = $session_info['name']."-".$session_info['session_name']."-".time()."-".$session_id."-".strtolower(str_replace(" ","_",$_FILES['upload_'.$i]['name'])); //rename file
					if($_FILES['upload_'.$i]['size'] > (5120000)) //can't be larger than 5 MB
					{
						$valid_file = false;
						echo "<div class='alert alert-error hidden-print'>One of your files was too large. Files should be less than 5Mb in size.</div>";
						//$message = 'Oops!  Your file\'s size is too large.';
					}else{
						$valid_file = true;
					}
					//if the file has passed the test
					if($valid_file)
					{
						//move it to where we want it to be
						if(move_uploaded_file($_FILES['upload_'.$i]['tmp_name'], 'uploads/'.$new_file_name)){
							
							
							$file_uploaded = true;
						}else{
							// problem moving file
							echo "<div class='alert alert-error hidden-print'>Error when moving the uploaded file.</div>";
							
						}
					}
				
				}else{
					// errors with file upload
					switch ($_FILES['upload_'.$i]['error']) {
						case UPLOAD_ERR_OK:
							break;
						case UPLOAD_ERR_NO_FILE:
							echo "<div class='alert alert-error hidden-print'>No file sent.</div>";
						case UPLOAD_ERR_INI_SIZE:
						case UPLOAD_ERR_FORM_SIZE:
							echo "<div class='alert alert-error hidden-print'>Exceeded filesize limit.</div>";
						default:
							echo "<div class='alert alert-error hidden-print'>Unknown Error.</div>";
					}
					echo "<div class='alert alert-error hidden-print'>Error with upload. Error = ".$_FILES['upload_'.$i]['error']."</div>";
				}
				
				// add file info to database
				if($file_uploaded){
					$q = "INSERT INTO uploaded_files SET 
					session_id = '$session_id',
					uploaded_by = '".$_SESSION['CrisUserID']."',
					date_uploaded = '".date('Y-m-d H:i:s',time())."',
					filename = '$new_file_name',
					filetype = '".$_FILES['upload_'.$i]['type']."'";
					if($r = mysql_query($q)){
						echo "<div class='alert alert-success hidden-print'>File $i successfully uploaded and added to database - <a href='uploads/".$new_file_name."'>$new_file_name - ".round($_FILES['upload_'.$i]['size'] / 1024,2)." KB</a></div>";
						
						$uploaded_files[] = array('filename' => $new_file_name, 'type' => $_FILES['upload_'.$i]['type']);
					}else{
						echo "<div class='alert alert-error hidden-print'>Error adding filename to database</div>";
					}
				
				}
				
				$i++;	
			}
			
			
			// delete existing staff and yp attendance
			$q = "DELETE FROM session_attendance WHERE session_id = '$sessionid'";
			if($r = mysql_query($q)){}else{echo "error deleting old attendance records. ".mysql_error();}
			
			// record attendance
			// staff
			$staff = explode("::",$staff_list);
			foreach($staff as $staffid){
				if( $r != mysql_query("INSERT INTO session_attendance SET session_id = '$sessionid', participant_id = '0', staff_id = '$staffid'")){
					echo "Error when recording session attendance. ".mysql_error()."";
					exit;
				}
			}
			
			// yp
			$yp = explode("::",$parti_list);
			foreach($yp as $ypid){
				if( $r != mysql_query("INSERT INTO session_attendance SET session_id = '$sessionid', participant_id = '$ypid', staff_id = '0'")){
					echo "Error when recording session attendance. ".mysql_error()."";
					exit;
				}
			}

			build_active_table();
			
			return;		
		break;
// **********************************************************************

		case 'changestaff':
			$query = "";
			foreach($_POST as $key=>$value){

					$$key = mres($value);
					
					if($key != 'id' && $key !='tablename' && $key !='needlogin' && $key !='idfield' && $key !='pw2' && $key != 'q'){	
						if($key == 'pw1'){
							if($$key == '' || $$key == ' '){
								// do not update password 
							}else{
								$query .= "password = '".crypt($$key)."', ";
							}
						}else{
							$query .= "$key = '".$$key."', ";
						}
					}
					$returnvars[$key] = $value;
					
			}
			// checkboxes if unticked do not appear in the post variables, so add them here if necessary
			if($tablename == 'staff' && !$staff){$staff = '0';$query .= "staff = '".$staff."', ";$returnvars['staff'] = 0;}
			if($tablename == 'staff' && !$volunteer){$volunteer = '0';$query .= "volunteer = '".$volunteer."', ";$returnvars['volunteer'] = 0;}
			if($tablename == 'staff' && !$driving_licence){$driving_licence = '0';$query .= "driving_licence = '".$driving_licence."', ";$returnvars['driving_licence'] = 0;}
			if($tablename == 'staff' && !$induction){$induction = '0';$query .= "induction = '".$induction."', ";$returnvars['induction'] = 0;}

			$q1 = "UPDATE $tablename SET ";
			$query = $q1.$query;
			$query = trim($query,', ');

			$query .= " WHERE id = '$id'";
			if($r = mysql_query($query)){
				$returnvars['report'] = 'Success';
				echo json_encode($returnvars);

			}else{
				echo "failed to update record $id in $tablename. Full query was $q - error is ".mysql_error();
			}
		break;
		
// **********************************************************************
		case 'changecontact':
			$query = "UPDATE contacts SET ";

			foreach($_POST as $key=>$value){

					$$key = mres($value);
					if($key != 'id' && $key !='contactid' && $key !='submit' && $key !='q'){	
						if($key == 'contact_type'){
							$ct = "";
							foreach($value as $l => $b){
								$ct .= "$b::";
							}
							$ct = trim($ct,"::");
							$query .= "category = '".$ct."', ";
						}else{
						
							$query .= "$key = '".$$key."', ";
						}
					}
					
			}

			$query = trim($query,', ');

			$query .= " WHERE id = '$contactid'";
			if($r = mysql_query($query)){
				echo "Updated contact $contactid";
			}else{
				echo "failed to update contact $contactid. Full query was $q - error is ".mysql_error();
			}
		
		break;
// ****************************************************************************************
		case "changeyp":
			$q = "UPDATE participants SET ";
			$ypid = $_POST['ypid'];
			foreach($_POST as $key=>$value){
					
					
					if($key != 'id' && $key !='ypid' && $key !='submit' && substr($key,0,5) != 'pdate' && $key != 'projectconsent' && $key != 'projectinterest' && $key != 'q'){	
						$$key = mres($value);
						if($key == 'delconsentlist'){
							$cids = explode("::",$_POST['delconsentlist']);
							foreach($cids as $cid){
								if($cid != ""){
									$q2 = "DELETE FROM projects_consented WHERE id = '$cid'";	
									if($r = mysql_query($q2)){
										$consent_result[] = "deleted $cid";
									}else{
										$consent_result[] = "failed to remove consent. Full query was $q2 - error is ".mysql_error();
									}
								}
							}
						}else if($key == 'delinterestlist'){
							$cids = explode("::",$_POST['delinterestlist']);
							foreach($cids as $cid){
								if($cid != ""){
									$q2 = "DELETE FROM projects_interested WHERE id = '$cid'";	
									if($r = mysql_query($q2)){
										$interest_result[] = "deleted $cid";
									}else{
										$interest_result[] = "failed to remove expression of interest. Full query was $q2 - error is ".mysql_error();
									}
								}
							}
						}else{
							$q .= "$key = '".$$key."', ";
						}

					}
				$returnvars[$key] = $value;
			}

			$q = trim($q,', ');

			$q .= " WHERE id = '$ypid'";
						
			
			if($r = mysql_query($q)){
				$returnvars['report'] = 'Success';
				$returnvars['consent'] = $consent_result;
				$returnvars['interest'] = $interest_result;
				echo json_encode($returnvars);
			}else{
				echo json_encode("failed to update yp $ypid. Full query was $q - error is ".mysql_error());
			}
		break;
// ****************************************************************************************
		case "getcolumns":
			$table = $_POST['table'];
			$tablealias = $_POST['tablealias'];
			$q = "SELECT COLUMN_NAME,COLUMN_TYPE FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA='cris' AND TABLE_NAME='$table'";
			if($r = mysql_query($q)){
				echo "<label>Choose Columns to include in the report:</label>";
				echo "<select id=choosecol>";
				switch($table){
					case 'participants':
						echo "<option value='Registered_Projects' coltype='regproj'>Registered Projects</option>
						<option value='Active_Projects' coltype='activeproj'>Active Projects</option>
						<option value='Attended_Projects' coltype='attendproj'>Attended Projects</option>
						<option value='Expressed_Interest' coltype='interestproj'>Interested Projects</option>
						<option value='Measure_Interviews' coltype='num_meas_dates'>Number of Measure Interviews</option>
						";
						
						
						break;
					case 'staff':
						echo "<option value='Projects_Attended' coltype='attendproj'>Attended Projects</option>";
						
						
						break;
					case 'projects':
						echo "<option value='Registered_Participants' coltype='regyp'>Registered Young People</option>";
						echo "<option value='Active_Participants' coltype='activeyp'>Active Young People</option>";
						echo "<option value='Attended_Participants' coltype='attendyp'>Attended Young People</option>";						
						echo "<option value='Expressed_Interest' coltype='interestyp'>Expressions of Interest</option>";

						
						break;
					default:
						break;
				}
				while($row=mysql_fetch_assoc($r)){
					echo "<option value='".$row['COLUMN_NAME']."' coltype='".$row['COLUMN_TYPE']."'>".$row['COLUMN_NAME']."</option>";				
				}
				echo "</select>";
				echo "<button type=button id=addcol>Add Selected Column</button>";
			}else{
				echo "<p class='warning'>Error accessing database. ".mysql_error()."</p>";
			}

			
			
		break;
// ********************************************************

		case "addconsent":
			if(isset($_POST['proj'])){
				$ypid = mres($_POST['yp']);
				$projid = mres($_POST['proj']);
				$date= mres($_POST['date']);
				$date = date('Y-m-d',strtotime($date));
				$projname = mres($_POST['name']);
				$q = "INSERT INTO projects_consented SET participant_id = '$ypid', project_id = '$projid', date = '$date'";
				
				if($r = mysql_query($q)){
					echo mysql_insert_id();
				}else{
					echo "failed to add consent. Full query was $q - error is ".mysql_error();
				}

			}
			
		break;

// ********************************************************

		case "addinterest":
			if(isset($_POST['proj'])){
				$ypid = mres($_POST['yp']);
				$projid = mres($_POST['proj']);
				$date= mres($_POST['date']);
				$date = date('Y-m-d',strtotime($date));
				$projname = mres($_POST['name']);
				$q = "INSERT INTO projects_interested SET participant_id = '$ypid', project_id = '$projid', date = '$date'";
				
				if($r = mysql_query($q)){
					echo mysql_insert_id();
				}else{
					echo "failed to add expression of interest. Full query was $q - error is ".mysql_error();
				}

			}
			
		break;
// ********************************************************

		case "getypautocomplete":
			// get yp list
			$yplist = '';
			$term = mres($_REQUEST['term']);
			$q = "SELECT * FROM participants WHERE firstname LIKE '%$term%' OR lastname LIKE '%$term%'";
			if($r = mysql_query($q)){	
				while($yp = mysql_fetch_array($r)){
					$yplist .= "{\"label\":\"".$yp['firstname']." ".$yp['lastname']."\",\"value\": \"".$yp['id']."\"},";
					
				}
				$yplist = trim($yplist, ",");
				echo "[".$yplist."]";
			}
		break;
// ********************************************************

		case "getstaffautocomplete":
			// get staff list
			$stafflist = '';
			$term = mres($_REQUEST['term']);
			$q = "SELECT * FROM staff WHERE firstname LIKE '%$term%' OR lastname LIKE '%$term%'";
			if($r = mysql_query($q)){	
				while($staff = mysql_fetch_array($r)){
					$stafflist .= "{\"label\":\"".$staff['firstname']." ".$staff['lastname']."\",\"value\": \"".$staff['id']."\"},";
					
				}
				$stafflist = trim($stafflist, ",");
				echo "[".$stafflist."]";
			}
		break;		

	}
}

mysql_close();
?>