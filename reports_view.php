<?php

require('header.php');
require('navmenutop.php');
?>

<div id=container>

<?php


//Load Query
if(isset($_POST['loadreport'])){
	$message = "<h2>Loaded Report '".$_POST['querytitle']."'</h2>";
	$_POST = unserialize(stripslashes(stripslashes($_POST['querystr'])));
	$_POST['submitreportform'] = true;
	


//Save Query
}elseif(isset($_POST['savereport'])){
	//print_r($_POST);
	
	$querytable = $_POST['querytable'];
	$querystr = mres($_POST['querystr']);
	$querydate = mres($_POST['date']);
	$querysummary = mres($_POST['summary']);
	$querysavedby = mres($_POST['saved_by']);
	$querytitle = mres($_POST['reportname']);
	$saveq = "INSERT INTO savedreports SET title = '$querytitle', querystr = '$querystr', date = '$querydate', summary = '$querysummary', saved_by = '$querysavedby',querytable = '$querytable'";
	if($result = mysql_query($saveq)){
		$message = "Query Saved as '$querytitle'.";
	}else{
		$message = "Error saving query. ".mysql_error();
	}
	$_POST = unserialize(stripslashes($_POST['querystr']));
	//       print_r($req);
}

//Process Query
if(isset($_POST['submitreportform'])){

	//print_r($_POST);
	
	foreach($_POST as $key=>$value){
		if(!is_array($value)){
			$$key = mres($value);
		}
	}
	
	$table = $choosetable;
	$q = "SELECT ";
	$slist = $table.".id AS pid";
	$summary = "";
	$where = "";
	$join = "";
	$group = "";
	foreach($_POST['reportfields'] as $rf){
		$rf = explode("::",$rf);
		$colname = $rf[0];
		$coltype = $rf[1];
		switch($rf[1]){
			//*******************************************
			case 'int':		
				$slist .= ",".$table.".".$colname;
				switch($_POST[$colname."_radio"]){
					case 'range':
						// range of values
						$where .= " AND (".$colname." > '".mres($_POST[$colname."_from"])."' AND ".$colname." <= '".mres($_POST[$colname."_to"])."')";
						$summary .= ", $colname greater than ".mres($_POST[$colname."_from"])." and less than ".mres($_POST[$colname."_to"])."";
						break;
					case 'specific':
						// specific values
						if($_POST[$colname."_specific"] != ''){
							$where .= " AND (".$colname." IN (".mres($_POST[$colname."_specific"])."))";
							$summary .= ", $colname = ".mres($_POST[$colname."_specific"])."";
						}else{
							$error .= "No specific value set";
						}
						break;
					case 'all':
					default:
						// show all values - no where statement for this field.
						break;
				}
				break;
			//*******************************************
			case 'varchar':
				$slist .= ",".$table.".".$colname;
				if($_POST[$colname."_varchar"] != ''){ 	
					$where .= " AND ".$colname." LIKE '%".mres($_POST[$colname.'_varchar'])."%'";	
					$summary .= ", $colname includes ".mres($_POST[$colname.'_varchar'])."";
				}
				if($_POST['project_name'] != ''){
					$plist = "";
					foreach($_POST['project_name'] as $v){
						$plist .= ",$v";
					}
					$plist = trim($plist,",");
					if($plist){
						$where .= " AND projects.id IN ($plist)";
						$summary .= ", Projects $plist";
					}
				}
				break;
			//*******************************************
			case 'date':
				$slist .= ",".$table.".".$colname;
				if($_POST[$colname."_from"] == '') $date_from = date('Y-m-d',strtotime("0000-00-00")); else $date_from = date('Y-m-d',strtotime($_POST[$colname."_from"]));
				if($_POST[$colname."_to"] == '') $date_to = date('Y-m-d',time()); else $date_to = date('Y-m-d',strtotime($_POST[$colname."_to"]));
				$where .= " AND (".$colname." > '".$date_from."' AND ".$colname." <= '".$date_to."')";
				$summary .= ", $colname greater than ".$date_from." and less than ".$date_to."";
				
				break;	
			//*******************************************
			case 'datetime':
				$slist .= ",".$table.".".$colname;
				if($_POST[$colname."_from"] == '') $date_from = date('Y-m-d',strtotime("0000-00-00")); else $date_from = date('Y-m-d',strtotime($_POST[$colname."_from"]));
				if($_POST[$colname."_to"] == '') $date_to = date('Y-m-d',time()); else $date_to = date('Y-m-d',strtotime($_POST[$colname."_to"]));
				$where .= " AND (".$colname." > '".$date_from."' AND ".$colname." <= '".$date_to."')";
				$summary .= ", $colname greater than ".$date_from." and less than ".$date_to."";
				
				break;	
			//*******************************************
			case 'tinyint':
				$slist .= ",".$table.".".$colname;
				switch($_POST[$colname."_radio"]){
					case 'yes':
						$where .= " AND (".$colname." = '1')";
						$summary .= ", $colname = TRUE";
						break;
					case 'no':
						$where .= " AND (".$colname." = '0')";
						$summary .= ", $colname = FALSE";
						break;
					case 'all':
					default:
						// show all values - no where statement for this field.
						break;
				}
				
				break;	
			//*******************************************	
			case 'text':
				$slist .= ",".$table.".".$colname;
				if($_POST[$colname."_varchar"] != ''){ 
					$where .= " AND ".$colname." LIKE '%".mres($_POST[$colname.'_varchar'])."%'";
					$summary .= ", $colname includes ".mres($_POST[$colname.'_varchar'])."";
				}			
				break;
				
			//*******************************************
			
			case 'regproj':
				//print_r($_POST);
				$slist .= ", (SELECT GROUP_CONCAT('(',projects_consented.date, ') ',projects.name SEPARATOR '<br />') FROM projects_consented LEFT JOIN projects ON projects.id = projects_consented.project_id WHERE projects_consented.participant_id = pid) AS $colname";
				
				if($_POST[$coltype."_project"] != '' && is_array($_POST[$coltype."_project"])){
					$plist = "";
					foreach($_POST[$coltype."_project"] as $v){
						$plist .= ",$v";
					}
					$plist = trim($plist,",");

					if(strlen($plist)>0){
						$where .= " AND (participants.id IN (SELECT DISTINCT participants.id FROM participants 
						INNER JOIN projects_consented ON projects_consented.participant_id = participants.id
						WHERE projects_consented.project_id IN ($plist)))";
						$summary .= ", YP has registered for projects $plist";
					}
				}
				break;
			//*******************************************
			case 'interestproj':
			
				$slist .= ", (SELECT GROUP_CONCAT('(',projects_interested.date,') ',projects.name SEPARATOR '<br />') FROM projects_interested LEFT JOIN projects ON projects.id = projects_interested.project_id WHERE projects_interested.participant_id = pid) AS $colname";
				
				if($_POST[$coltype."_project"] != '' && is_array($_POST[$coltype."_project"])){
					$plist = "";
					foreach($_POST[$coltype."_project"] as $v){
						$plist .= ",$v";
					}
					$plist = trim($plist,",");

					if(strlen($plist)>0){
						$where .= " AND (participants.id IN (SELECT DISTINCT participants.id FROM participants 
						INNER JOIN projects_interested ON projects_interested.participant_id = participants.id
						WHERE projects_interested.project_id IN ($plist)))";
						$summary .= ", YP has expressed interest in project(s) $plist";
						
					}
				}
			
				break;
			//*******************************************			
			case 'activeproj':
				//print_r($_POST);
				
				$slist .= ", (SELECT GROUP_CONCAT('(',active_participants.latest_activity_date,') ',projects.name SEPARATOR '<br />') FROM active_participants LEFT JOIN projects ON projects.id = active_participants.project_id WHERE active_participants.participant_id = pid) AS $colname";
				
				if($_POST[$coltype."_project"] != '' && is_array($_POST[$coltype."_project"])){
					$plist = "";
					foreach($_POST[$coltype."_project"] as $v){
						$plist .= ",$v";
					}
					$plist = trim($plist,",");

					if(strlen($plist)>0){
						$where .= " AND (participants.id IN (SELECT DISTINCT participants.id FROM participants 
						INNER JOIN active_participants ON active_participants.participant_id = participants.id
						WHERE active_participants.project_id IN ($plist)))";
						$summary .= ", YP is active in project(s) $plist";
						
					}
				}
			
				break;
			//*******************************************
			
			case 'attendproj':
				
				
				// get project ids ($_POST['$colype_project']) (array)
				// get all session type ids in that project $session_type_ids = (SELECT id FROM session_types WHERE project_id IN ($project_ids))
				// get all sessions belonging to one of those session types $session_ids = (SELECT id FROM sessions WHERE session_type IN ($session_type_ids))
				// check all these sessions to see if person attended (SELECT COUNT(*) FROM session_attendance WHERE (participant_id = '$pid' OR staff_id = '$sid') AND session_id IN ($session_ids);
				if($_POST[$coltype."_project"] != '' && is_array($_POST[$coltype."_project"])){
					$plist = "";
					foreach($_POST[$coltype."_project"] as $v){
						$plist .= ",$v";
					}
					$plist = trim($plist,",");
					$plistsummary = $plist;
					$summary .= ", Person has attended project(s) $plistsummary";
					if($plist != '') $plist = "WHERE project_id IN ($plist)";
					if($table == 'participants') $stafforyp = 'participant_id'; 
					if($table == 'staff') $stafforyp = 'staff_id';
					
					$slist .= ", GROUP_CONCAT('(',ap.session_date,') ',ap.name,' - ',ap.session_name SEPARATOR '<br />') AS $colname";
					$join .= " 
						LEFT JOIN (
							SELECT session_date,name,session_name,id,$stafforyp FROM (
								SELECT session_types.session_name,sessions.session_date,projects.id,projects.name,session_attendance.$stafforyp 
								FROM session_attendance 
								LEFT JOIN sessions ON sessions.id = session_attendance.session_id 
								LEFT JOIN session_types ON session_types.id = sessions.session_type 
								LEFT JOIN projects ON projects.id = session_types.project_id 
								WHERE (session_attendance.session_id IN (
									SELECT id FROM sessions WHERE session_type IN (SELECT id FROM session_types $plist )
									) 
								)
					";
					if($_POST[$coltype."_from"] && $_POST[$coltype."_to"]){
						$join .= "AND (sessions.session_date > '".$_POST[$coltype."_from"]."' AND sessions.session_date < '".$_POST[$coltype."_to"]."')";
						$summary .= " between ".$_POST[$coltype."_from"]." and ".$_POST[$coltype."_to"]."";
					}
					$join .="
								ORDER BY session_date DESC 
							) 
							AS tmptable GROUP BY tmptable.$stafforyp,tmptable.session_name 
						) AS ap ON ap.$stafforyp = ".$table.".id";
					$group .= " GROUP BY pid";
					$where .= " AND (ap.session_date <> '')";
					
					
				}
				

				break;
			//*******************************************
			case 'num_meas_dates':
				$slist .= ", (SELECT COUNT(DISTINCT date) FROM measures_data WHERE participant_id = pid) AS $colname";
				
				break;
			
			//*******************************************
			case 'regyp':
			
				$slist .= ", (SELECT GROUP_CONCAT(participants.firstname, ' ',participants.lastname SEPARATOR '<br />')
				FROM projects_consented 
				LEFT JOIN projects ON projects.id = projects_consented.project_id
				LEFT JOIN participants ON participants.id = projects_consented.participant_id
				WHERE projects_consented.project_id = pid 
				ORDER BY lastname ASC
				) AS $colname";
				
				break;
			//*******************************************
			
			case 'attendyp':
							
				$slist .= ", (SELECT GROUP_CONCAT('(',attended_participants.latest_activity_date,') ',participants.firstname,' ',participants.lastname SEPARATOR '<br />') FROM attended_participants LEFT JOIN participants ON participants.id = attended_participants.participant_id WHERE attended_participants.project_id = pid) AS $colname";
		
				break;
			//*******************************************
			
			case 'activeyp':
							
				$slist .= ", (SELECT GROUP_CONCAT('(',active_participants.latest_activity_date,') ',participants.firstname,' ',participants.lastname SEPARATOR '<br />') FROM active_participants LEFT JOIN participants ON participants.id = active_participants.participant_id WHERE active_participants.project_id = pid) AS $colname";
		
				break;
			// ****************************************
			
			case 'interestyp':
				$slist .= ", (SELECT GROUP_CONCAT('(',temp.date, ') ',temp.firstname, ' ',temp.lastname SEPARATOR '<br />') FROM (SELECT projects_interested.date, participants.firstname, participants.lastname,projects_interested.project_id FROM projects_interested LEFT JOIN participants ON participants.id = projects_interested.participant_id ORDER BY date ASC) AS temp WHERE temp.project_id = pid)  AS $colname";
				
				
			break;
		}	
		
		
	}

	
	$slist = trim($slist,",");
	$where = trim($where," AND");
	if($where != "") $where = " WHERE ".$where;
	if($sortby == '') $sortby = "pid";
	
	$q .= $slist." FROM ".$table.$join.$where.$group." ORDER BY ".$sortby;
	
	$summary = trim($summary,",");
	if($summary == '' ){
		$summary = "Searched for all ".$table;
	}else{
		"Searched for ".$table." where ".$summary;
	}
}


?>
<div id=toolbar class='hidden-print'>
	<div class=first>
		<span style="font-weight: bold;font-size: 200%;vertical-align: middle;">Reports Generator</span>
	</div>
	<div>
		<a href="<?php echo ROOT_PATH;?>/reports_view.php"><button class='btn btn-med btn-primary link-btn'>New Report</button></a>
	</div>
	<div>
		<a href="<?php echo ROOT_PATH;?>/reports_load.php"><button class='btn btn-med btn-primary link-btn'>Run a Saved Report</button></a>
	</div>
	<?php if(isset($_POST['submitreportform'])){?>
	<div>
		<form method=post action='reports_view.php' id=savereportform>
		<?php
			
			echo "<input type=hidden name=querystr value='".serialize($_POST)."' />";
			echo "<input type=hidden name=date value='".date("Y-m-d H:i:s",time())."' />";
			echo "<input type=hidden name=saved_by value='".$_SESSION['CrisUserID']."' />";
			echo "<input type=hidden name=querytable value='".$_POST['choosetable']."' />";
			echo "<input type=hidden name=summary value='".mres($summary)."' />";
		?>
		<button class='btn btn-med btn-primary link-btn' type=submit name=savereport>Save this Report</button></form>
	</div>
	<?php
	}
	?>
	<div>
		<a href="javascript:window.print()"><button class='btn btn-med btn-primary link-btn'>Print</button></a>
	</div>
	
	
</div>

<div id=container style='max-width:90%'>


<?php
if(isset($_POST['submitreportform'])){
	//print_r($_POST);
	echo "<div id=reportdetails class='hidden-print'>";
	echo $message;
	echo "<div style='display:none' id='showquery'>".htmlspecialchars($q)."</div><br /><button onclick=\"$('#showquery').toggle();\" type='button'>Show actual query</button>";;
	echo "<br />";

	echo "Search Criteria: $summary";
	
	echo ".<br />Sorted by $sortby.";
	echo "</div>";
	
	if($r = mysql_query($q)){
		echo "<br /><br />Total of ".mysql_num_rows($r)." results found";
		echo "<table id=report class=table>";
		echo "<thead><tr>";
		$i = 0;
		$hidden_index = array();
		foreach($_POST['reportfields'] as $k){
			$k = explode("::",$k);
			if($_POST[$k[0]."_show"] == 'on') {echo "<th>"; }else{ echo "<th class='hidecol'>";$hidden_index[] = $i;}
			
			echo $k[0]." (".$k[1].")</th>";
			$i++;
		}
		echo "</tr></thead>";
		while($row = mysql_fetch_assoc($r)){
			echo "<tr>";
			$i = -1;
			foreach($row as $k=>$v){
				if(in_array($i,$hidden_index)) $tdc = "<td class=hidecol arr='$hidden_index'>"; else $tdc = "<td>";
				if($k != 'pid'){
					echo $tdc.$v."</td>";
				}
				$i++;
			}
			echo "</tr>";
		}
		echo "</table>";
		//print_r($hidden_index);
	}else{
		echo "Error in Mysql query. ".mysql_error();
	}
	
	


}else{

echo "<div style='display:none'>";
$query = "SELECT * FROM projects";

if($projects = mysql_query($query)){
	echo "<select name=chooseproject[] id='chooseproject'>";
	echo "<option value='' selected>- show all -</option>";
	while($project = mysql_fetch_assoc($projects)){
		echo "<option value='".$project['id']."'>".$project['name']."</option>";
	}
	echo "</select>";
}
echo "</div>";

?>
	<h1>Generate a Report</h1>

	

		<form id=reportform method=post action=''>
			<div id=reportoptions>
				<div id=choosetablediv><label for=choosetable>Choose an area of the database to run a report on:</label><select name=choosetable id=choosetable>
					<option value='' disabled selected>-- Choose Table --</option>
					<option value='participants'>Young People</option>
					<option value='staff'>Workers / Volunteers</option>
					<option value='projects'>Projects</option>
<!--					<option value='sessions'>Sessions</option>
					<option value='journey'>Journey</option>-->
					
				</select>
				</div>
				<div id=choosecolsdiv>
				
				</div>
				<div class='floatbreak'></div>
			</div>
		
			<div id=reportfields class='sortable start-hidden'><h1></h1><h2>Report will contain the following columns</h2><p>Drag the boxes to re-order the report columns. Click on the red 'x' to remove that column.</p></div>
			<input type=submit name=submitreportform class=start-hidden value='Generate Report' />
		</form>
			
		
<?php
}
?>
</div>
<script src='<?php echo ROOT_PATH;?>/js/reports_functions.js'></script>
  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script>

</script>
<?php
require('footer.php');
?>