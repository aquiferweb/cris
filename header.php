<? 
require_once ("config_cris.php");
check_login();
dbconnect();
$admin = check_admin();
?> 

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<link href="<?php echo ROOT_PATH;?>/css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH;?>/css/style.css" />	
	<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH;?>/css/print.css" media="print" />	
	<link rel="stylesheet" type="text/css" href='<?php echo ROOT_PATH;?>/css/redmond/jquery-ui-1.10.3.custom.css' />
	<link rel="stylesheet" type="text/css" href='<?php echo ROOT_PATH;?>/css/jquery.timepicker.css' />
	<link rel="shortcut icon" href="<?php echo ROOT_PATH;?>/favicon.ico" type="image/x-icon" />

	
	<script src='<?php echo ROOT_PATH;?>/js/modernizr.custom.77328.js'></script>
	<script src="<?php echo ROOT_PATH;?>/js/jquery-1.11.1.min.js"></script>
	<script src="<?php echo ROOT_PATH;?>/js/jquery-migrate-1.2.1.min.js"></script>
	<script src='<?php echo ROOT_PATH;?>/js/jquery-ui-1.10.3.custom.min.js'></script>
	<script src='<?php echo ROOT_PATH;?>/js/jquery.timepicker.min.js'></script>
	<script src="<?php echo ROOT_PATH;?>/js/bootstrap.min.js"></script>
	<script src='<?php echo ROOT_PATH;?>/js/functions.js'></script>
	
	
<title>Cre8 Recording and Information System</title>



</head>
<body>