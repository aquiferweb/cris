<?php

require('header.php');
require('navmenutop.php');

if(isset($_GET['sortby'])){
	$sortby = $_GET['sortby'];
}else{
	$sortby = 'lastname';
}
if(isset($_GET['direction'])){
	$direction = $_GET['direction'];
}else{
	$direction = 'ASC';
}


?>
<div class="" id='loading'>
  <h1>Loading Young People - please wait...</h1>
  <img src='img/ajax-loader.gif' />
</div>




<div id=container style='display:none;'>
<div id=toolbar class=hidden-print>
	<div class=first>
		<span style="font-weight: bold;font-size: 200%;vertical-align: middle;">Young People Database</span>
	</div>
	<div>
		<a href="javascript:window.print()"><button class='btn btn-med btn-primary link-btn'>Print</button></a>
	</div>
	<div>
		<a href="<?php echo ROOT_PATH;?>/yp_add.php"><button class='btn btn-med btn-primary link-btn'>Add New Young Person</button></a>
	</div>
	<div>
		Search YP: <input type=text id=searchyp class=form-control style='display:inline-block;width:auto;' />
	</div>
	
	<div>
		<div><input type=checkbox id=showCVA class=ypfilter checked /> <img src='<?php echo ROOT_PATH;?>/img/tick.png' /> YP Registered and Active in last month</div>
		<!--<div><input type=checkbox id=showCA class=ypfilter checked /> <img src='img/exclamation.jpg' /> YP Active with out-of-date consent.</div>-->
		<!--<div><input type=checkbox id=showA class=ypfilter checked /> <img src='img/exclamationred.png' /> YP Active but not consented.</div>-->
		<div><input type=checkbox id=showCV class=ypfilter  /> <img src='<?php echo ROOT_PATH;?>/img/form.png' /> YP Registered but not active.</div>
		<!--<div><input type=checkbox id=showC class=ypfilter  /> <img src='img/oldform.png' />YP has out-of-date consent.</div>-->
	</div>
</div>
<?php

if($_REQUEST['added']){$report .= "<p>New yp added (ID = ".$_REQUEST['added'].")</p>";
echo $report;
}
?>
<p id=report></p>
<span id=updatespan style='display:none;position:absolute;padding:10px'>Updated</span>

<?php


$yplist = "";
$q = "SELECT
	participants.id,
	participants.dob,
	participants.start_date,
	participants.firstname,
	participants.lastname, 
	participants.address, 
	participants.postcode, 
	participants.area, 
	participants.yp_phone, 
	participants.guardian_name, 
	participants.school_year, 
	participants.school, 
	participants.gender, 
	participants.guardian_signature_date,
	participants.guardian_phone,
	participants.photo_consent,
	participants.facebook_consent,
	participants.health_form_received, 
	participants.medical, 
	participants.allergies, 
	participants.notes	issues,
	participants.caf, 
	participants.key_worker,
	participants.referrer
	FROM participants
	ORDER BY $sortby $direction";


if(!($r = mysql_query($q))){
	echo "Error getting young people. ".mysql_error();
}else{
	while($yprow = mysql_fetch_array($r)){
		
		// build YPdb array
		$ypid = $yprow['id'];
		$ypdb[$ypid] = $yprow;
		
		$sc = "";
		
		// Get Consented Projects
		$qu = "SELECT * FROM projects_consented WHERE participant_id = '".$ypid."' ORDER BY date DESC";
		$res = mysql_query($qu);
		$ypdb[$ypid]['registered_projects_info'] = array();
		$ypdb[$ypid]['registered_projects'] = array();
		$cps[$ypid] = array();
		while($row = mysql_fetch_array($res)){
			$ypdb[$ypid]['registered_projects_info'][] = $row;
			$ypdb[$ypid]['registered_projects'][] = $row['project_id'];
			
		}
		
		// get active projects
		$qu = "SELECT * FROM active_participants WHERE participant_id = '".$ypid."'";
		$res = mysql_query($qu) or die(mysql_error());
		$ypdb[$ypid]['active_projects'] = array();
		while($row = mysql_fetch_array($res)){
			$ypdb[$ypid]['active_projects'] [] = $row['project_id'];
		}
		
		
		// Calculate Ages and School Year
		$age = floor((time() - strtotime($yprow['dob'])) / (60*60*24*365.25));
		$ypdb[$ypid]['age'] = $age;
		if(date("n") > 8){$sep_year = date('Y');}else{$sep_year = date('Y')-1;}
		$year = floor(((strtotime("1st Sep ".$sep_year)-strtotime($yprow['dob']))/(60*60*24*365.25))-4);
		$ypdb[$ypid]['calc_year'] = $year; 

		// add to autocomplete list
		$yplist .= "{label:'".$yprow['firstname']." ".$yprow['lastname']."',value: '".$yprow['id']."'},";

	}
	$yplist = trim($yplist, ",");

	// YP Table
	echo "<table id=participants class='cre8table table' style='width:100%;'><thead>";
	
	// Setup sortable header
	echo "<tr class=stickyheader>";
	//echo sortable_header('status','Status',$sortby,$direction,2);
	//echo sortable_header('start_date','Start Date',$sortby,$direction,2);
	echo sortable_header('firstname','First Name',$sortby,$direction,2);
	echo sortable_header('lastname','Surname',$sortby,$direction,2);
	echo sortable_header('dob','Age / DOB',$sortby,$direction,2);
	echo sortable_header('school','School',$sortby,$direction,2);
	echo sortable_header('school_year','School Year',$sortby,$direction,2);
	echo "<th rowspan=2>YP Phone <br /> Guardian Phone</th>";
	echo sortable_header('guardian_name','Parent or<br />Guardian',$sortby,$direction,2);
	echo sortable_header('address','Address',$sortby,$direction,2);
	
	// get project list
	$pq = "SELECT * FROM projects WHERE active = '1' ORDER BY display_order ASC";
	if(!($projects = mysql_query($pq))){echo "Error getting project list. ".mysql_error();}else{
		$nump = mysql_num_rows($projects);
		echo "<th colspan=$nump>Projects &nbsp;&nbsp;&nbsp;<label id=checkalltext style='font-size:smaller' for=checkall>Select All</label> <input type=checkbox id=checkall checked /></th>";
		echo "</tr>";
		echo "<tr class=stickyheaderB>";
		$i = 1;
		while($project = mysql_fetch_array($projects)){
			if($project['active']){
				if($project['black_text']) {$tcol = "black";}else{$tcol = "white";}
				echo "<th col=$i class=project style='background:#".$project['colour'].";color:".$tcol."'>".$project['short_name']."<br /><input type=checkbox id=filter_".$project['id']." class=filterproj checked /></th>";
				$projectlist[$i]= array('id' => $project['id'],'name' => $project['name']);
				$i++;
			}
		}
	}
	echo "</tr>";

	echo "</thead>";
	
	$odd = false;
	echo "<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>";
	for($i = 1;$i <=$nump;$i++){
		echo "<td class=project></td>";
	}	
	echo "</tr>";
	foreach($ypdb as $yp){
	
			
		// Start table row
		echo "<tr id=yp_".$yp['id']." class='clickable_yp $sc' tabindex=".$yp['id'].">";
		//echo "<td>"; echo "</td>";
		//echo "<td>".$yp['start_date']."</td>";
		echo "<td class=firstname>".$yp['firstname']."</td>";
		echo "<td class=lastname>".$yp['lastname']."</td>";
		echo $yp['dob'] != "0000-00-00" ? "<td class='dob'>".$yp['age']." yrs old<br />".date("d/m/y",strtotime($yp['dob']))."</td>":"<td class='missing  dob'>No DOB recorded"."</td>";
		echo "<td class=school>".$yp['school']."</td>";
		echo "<td class=school_year>Year ".($yp['calc_year']+$yp['school_year']);		
		echo ($yp['school_year'] != 0 && $yp['dob'] != "0000-00-00" && $yp['school_year'] != 'n/a') ? "<br />(Actual Year ".$yp['calc_year'].")":"";		
		echo "</td>";
		echo "<td class=phone>".$yp['yp_phone']."<br />".$yp['guardian_phone']."</td>";
		echo "<td class=gname>".$yp['guardian_name']."</td>";	
		echo "<td class=address>".$yp['address']." ".$yp['postcode']."</td>";	
		for($i = 1;$i <=$nump;$i++){		
			if(in_array($projectlist[$i]['id'],$yp['registered_projects']) || in_array($projectlist[$i]['id'],$yp['active_projects'])){
				$ypactive = false;
				$ypconsent = false;
				$ypvalidconsent = false;
				$pclass="";
				
				if(in_array($projectlist[$i]['id'],$yp['active_projects'])){$ypactive = true;$pclass = " active";}
				
				foreach($yp['registered_projects_info'] as $cp){
				
					if($cp['project_id'] == $projectlist[$i]['id']){
						//registered
						$ypconsent = true;
						$pclass .= " consented";
						
						// NB: this part differentiates between active and registered yp, but the styles have been adjusted so that all registration forms (even old) ones will result in a 'registered' appearance, and activity without a registration form will still appear as 'active'. The old behaviour can be reinstated by adjusting the stylesheet for td.active.consented etc
						
						$ypvalidconsent = true;
						$pclass .= " valid";
						
						
						if(strtotime($cp['date']) > strtotime("-1 year")){
							// valid consent
							$ypvalidconsent = true;
							$pclass .= " valid";
							break;
						}else{
							// old consent
							$ypvalidconsent = false;
							$pclass .= " old";
							break;
						}
						
					}else{
						// not consented
						$ypconsent = false;
						
					}
				}
				
				if($ypactive && $ypconsent && $ypvalidconsent) $hovertxt = "Registered ".$cp['date']." and Active";
				if($ypactive && !$ypconsent) $hovertxt = "Active but not yet registered";
				if(!$ypactive && $ypconsent && $ypvalidconsent) $hovertxt = "Registered ".$cp['date']." but not currently active";
				if($ypactive && $ypconsent && !$ypvalidconsent) $hovertxt = "Active with old registration (".$cp['date'].")";
				if(!$ypactive && $ypconsent && !$ypvalidconsent) $hovertxt = "Old registration (".$cp['date'].") not currently active";
				
				echo "<td class='project projclass_".$projectlist[$i]['id']."$pclass' title='$hovertxt'>";
				echo "</td>";
			}else{
				echo "<td class='project'>";
				echo "</td>";
			}
		}	
		echo "</tr>";
		
		
	}
	
	
	echo "</table>";
	
}

?>
<div class="modal fade" id=edityp role="dialog" aria-labelledby="edityplabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id='edityplabel'>View/Edit Young Person</h4>
      </div>
      <div class="modal-body">
			<div id='summarytable' class=edittoggle></div>
			<form id=editypform class='edittoggle' style='display:none;'>
				<input type=hidden name=ypid id=ypid />
				<?php require('yp_form.php'); ?>		
			</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" onclick="$('.edittoggle').toggle();"><span class='edittoggle'>Edit</span><span class='edittoggle' style='display:none;'>Cancel Editing</span></button>
        <button type="button" class="btn btn-primary edittoggle" onclick="printtable();">Print</button>
        <button type="button" class="btn btn-primary edittoggle" style='display:none;' onclick='saveYp();'>Save Changes</button>
		<? if($admin) echo "<button type='button' class='btn btn-danger edittoggle' style='display:none;' onclick='deleteYp();'>Delete Young Person</button>";?>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script src='<?php echo ROOT_PATH;?>/js/yp_functions.js'></script>

<script>
$( document ).ready(function() {
	yplist = [<?echo $yplist ?>];
	school_list = [{label:'Macclesfield Academy',value: 'Macclesfield Academy'},{label:'Fallibroome Academy',value: 'Fallibroome Academy'},{label:'All Hallows',value: 'All Hallows'},{label:'Tytherington High School',value: 'Tytherington High School'},{label:'Adelaide',value: 'Adelaide'},{label:'St. Johns Wood',value: 'St. Johns Wood'},{label:'Cheshire East',value: 'Cheshire East'},{label:'Pupil Referral Unit (PRU)',value: 'Pupil Referral Unit (PRU)'},{label:'SEND Team',value: 'SEND Team'},{label:'Christ the King',value: 'Christ the King'},{label:'Congleton High School',value: 'Congleton High School'},{label:'Ash Grove',value: 'Ash Grove'}];
	
	$('#school').autocomplete({
		source: school_list
	});
	
	
	

	$('#searchyp').autocomplete({
		source: yplist,
		select: function( event, ui ) {
			event.preventDefault();
       		id = ui.item.value;
			$('#searchyp').val(ui.item.label);
			//console.log(id+" "+ui.item.label);
			if(id){
				$('tr#yp_'+id).focus().children('td').last().append("<img src='img/ajax-loader-sm.gif' id=ajaxload style='margin-left:10px;' />");
				getYp(id);
				
			}
      }
	});
	
	$('.ui-autocomplete').appendTo('#navsidemenu');
	
		

});
</script>
<?php
require('footer.php');
?>