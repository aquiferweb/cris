<?php

require('header.php');

require('navmenutop.php');

?>

<div id=container>

<?php
if($_GET['submit'] == 1){	
	
	$session_id = $_GET['sid'];
	$qsum = "";
	if($_GET['s'] == 1){$qsum .= "Session added to database.<br />";}else{$qsum .= "Error adding session to database ".$_GET['error']."<br />";}
	if($_GET['w'] == 1){$qsum .= "Worker attendance added to database.<br />";}else{$qsum .= "Error adding worker attendace to database ".$_GET['error']."<br />";}
	if($_GET['yp'] == 1){$qsum .= "Young person attendance added to database.<br />";}else{$qsum .= "Error adding yp attendance to database ".$_GET['error']."<br />";}
	$q = "SELECT session_types.session_name, projects.name, sessions.session_date,sessions.session_end_date, sessions.location, sessions.session_start_time, sessions.session_end_time, sessions.location, leader.email AS slem, leader.firstname AS lfn, leader.lastname AS lln, recorder.firstname AS rfn, recorder.lastname AS rln,sessions.timestamp, pleader.email AS plem, pleader.id AS plid, pleader.firstname AS plfn, pleader.lastname AS plln, sessions.notes, sessions.action, sessions.action_text, sessions.sg
	FROM sessions
	LEFT JOIN staff AS leader ON leader.id = sessions.staff_leader
	LEFT JOIN staff AS recorder ON recorder.id = sessions.user_id
	LEFT JOIN session_types ON session_types.id = sessions.session_type
	LEFT JOIN projects ON projects.id = session_types.project_id
	LEFT JOIN staff AS pleader ON pleader.id = projects.project_leader
	WHERE sessions.id = '$session_id'";

	$session_info = mysql_fetch_array(mysql_query($q)) or die(mysql_error());
	
	$ypq = "SELECT participants.firstname, participants.lastname FROM session_attendance LEFT JOIN participants ON participants.id = session_attendance.participant_id WHERE session_attendance.session_id = '$session_id' ORDER BY participants.lastname ASC";
	if($r = mysql_query($ypq)){
		while($ypp = mysql_fetch_array($r)){
			if($ypp['firstname'] != '') $yps[] = $ypp['firstname']." ".$ypp['lastname'];
		}
	}else{
		echo "<div class='alert alert-warning error'>Error retrieving yp attendance. ".mysql_error()."</div>";
	}
	
	$staffq = "SELECT staff.firstname, staff.lastname FROM session_attendance LEFT JOIN staff ON staff.id = session_attendance.staff_id WHERE session_attendance.session_id = '$session_id' ORDER BY staff.lastname ASC";
	if($r = mysql_query($staffq)){
		while($staffp = mysql_fetch_array($r)){
			if($staffp['firstname'] != '') $staffs[] = $staffp['firstname']." ".$staffp['lastname'];
		}		
	}else{

		echo "<div class='alert alert-warning error'>Error retrieving staff attendance. ".mysql_error()."</div>";
	}
	
	$sgfollow = $session_info['sg'] ? "YES -":"NO";
	$actions = $session_info['action'] ? $session_info['action_text'] : "None";
	$summary = "<div id=sessionsummary>
	<table class=table id=summarytable><thead><tr><th colspan=4>New Session Recorded</th></tr></thead>
	<tbody><tr><td colspan=4>$qsum</td></tr>
		<tr><td class=field>Project:</td><td>".$session_info['name']."</td><td class=field>Session Type:</td><td>".$session_info['session_name']."</td></tr>
		<tr><td class=field>Start Date:</td><td>".date('d/m/y',strtotime($session_info['session_date']))." ".$session_info['session_start_time']."</td><td class=field>End Date:</td><td>".date('d/m/y',strtotime($session_info['session_end_date']))." ".$session_info['session_end_time']."</td></tr>
		<tr><td class=field>Location:</td><td colspan=3>".$session_info['location']."</td></tr>
		<tr><td class=field>Session Leader:</td><td>".$session_info['lfn']." ".$session_info['lln']."</td><td class=field>Recorded By:</td><td>".$session_info['rfn']." ".$session_info['rln']."<br />".$session_info['timestamp']."</td></tr>
		<tr><td class=field>Staff:</td><td>".count($staffs)."</td><td class=field>Young People:</td><td>".count($yps)."</td></tr>
		<tr><td colspan=2>";
	foreach($staffs as $staff){$summary .= $staff."<br/>";}
	$summary .="</td><td colspan=2>";
	foreach($yps as $yp){$summary .= $yp."<br/>";}
	$summary .= "</td></tr>
		<tr><td class=field>Notes:</td><td colspan=3>".nl2br($session_info['notes'])."</td></tr>	
		<tr><td class=field>Actions:</td><td colspan=3>".$actions."</td></tr>	
		<tr><td class=field>Safeguarding:</td><td colspan=3>$sgfollow ".$session_info['sgdetails']."</td></tr>
	</tbody></table>
	<div style='text-align:right;'><button type=button onclick='window.print();' class='hidden-print btn btn-primary btn-med'>Print Summary</button><a href='".ROOT_PATH."/sessions_add.php' class='hidden-print'><button type=button class='hidden-print btn btn-primary btn-med'>Add another session</button></a><a href='".ROOT_PATH."/sessions_view.php' class='hidden-print'><button type=button class='hidden-print btn btn-primary btn-med'>Back to Sessions Page</button></a></div>
	</div>
	";	

	// Rebuild Active Participant table based on new data.
	
	build_active_table();
	
	echo $summary;
	
	if($actionrequired){
	/*
		// NOTE THIS SECTION IS NOT WORKING ON THE LOCAL SEVER
		// Investigate why mails are not able to be sent...
	
		// To send HTML mail, the Content-type header must be set
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

		// Additional headers
		$headers .= 'From: info@cre8macclesfield.org' . "\r\n";
	*/
	/*
		if($staff_leader == $session_info['plid']){
			// session leader was project leader
			mail($session_info['slem'],$session_info['session_name']." - CRIS session notification",$summary,$headers);
			echo "mail sent to: ".$session_info['slem']." $headers";
		}else{
			// session leader was different from project_leader
			mail($session_info['slem'],$session_info['session_name']." - CRIS session notification",$summary,$headers);
			mail($session_info['plem'],$session_info['session_name']." - CRIS session notification",$summary,$headers);
			echo "mail sent to: ".$session_info['slem'];
			echo "mail sent to: ".$session_info['plem'];
		}
	*/
	}
}elseif($_GET['submit'] == -1){
	echo "<h2>No session was submitted</h2>";
}else{
	// get yp list
	$q = "SELECT * FROM participants";
	if($r = mysql_query($q)){	
		while($yp = mysql_fetch_array($r)){
			$yplist .= "{label:\"".$yp['firstname']." ".$yp['lastname']."\",value: \"".$yp['id']."\"},".PHP_EOL;
		}
	}else{
		echo "<p class=error>Error selecting participant list. ".mysql_error()."</p>";
	}
	$yplist = trim($yplist, ",");
	
	// get staff list					
	$q = "SELECT * FROM staff";
	if($r = mysql_query($q)){
		while($st = mysql_fetch_array($r)){
			$stafflist .= "{label:'".$st['firstname']." ".$st['lastname']."',value: '".$st['id']."'},";
		}
	}else{
		echo "<p class=error>Error selecting staff list. ".mysql_error()."</p>";
	}
	$stafflist = trim($stafflist, ",");	
?>



	<form id=newsession action="sessions_submit.php" method=post class=addnewform enctype="multipart/form-data">
		<h2>Record New Session</h2>
		<?php include ('sessions_form.php');?>
		<div class='session_input start_hidden' style='text-align:right;'>
			<button type=submit class="btn btn-primary btn-med" id='submitNewSession'>Submit Session Record</button>
			<a href='<?php echo ROOT_PATH;?>/sessions_view.php'><button type=button class="btn btn-default btn-med">Cancel</button></a>
		</div>
	</form>
<?php
}
?>

<script>
$( document ).ready(function() {

	yplist = [<?echo $yplist ?>];
	stafflist = [<?echo $stafflist ?>];
	
	$('#addp').autocomplete({
		source: yplist,
		select: function( event, ui ) {
			event.preventDefault();
       		id = ui.item.value;
			$('#addp').val('');
			if(!isNaN(id)){
				add_p(id,ui.item.label);			
			}
		}
	});
	
	$('#add_staff').autocomplete({
		source: stafflist,
		select: function( event, ui ) {
			event.preventDefault();
       		id = ui.item.value;
			$('#add_staff').val('');
			if(!isNaN(id)){
				console.log(isNaN(id));
				add_staff(id,ui.item.label);			
			}
		}
	});
	
	location_list = [{label:'The House',value: 'The House'},{label:'The Building',value: 'The Building'},{label:'The Church',value: 'The Church'}];
	
	$('#location').autocomplete({
		source: location_list
	});
	
	// assign current logged in user to the session leader role by default
	staffid = $('#staff_leader').val();
	staffname = $("#staff_leader_name").html();
	add_staff(staffid,staffname);
	make_leader(staffid);
	//console.log(staffid+" "+staffname);
});
</script>

<script src='js/sessions_functions.js'></script>
<?php
require('footer.php');
?>