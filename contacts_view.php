<?php
require('header.php');
require('navmenutop.php');
?>


<?php

if(isset($_GET['sortby'])){
	$sortby = $_GET['sortby'];
}else{
	$sortby = 'lastname';
}
if(isset($_GET['direction'])){
	$direction = $_GET['direction'];
}else{
	$direction = 'ASC';
}


?>

<div id=container>

<?php
?>

<div id=toolbar>
	<div class=first>
		<span style="font-weight: bold;font-size: 200%;vertical-align: middle;">Contacts Database</span>
	</div>
	<div>
		<a href="javascript:window.print()"><button class='btn btn-med btn-primary link-btn'>Print</button></a>
	</div>
	<div>
		<a href="<?php echo ROOT_PATH;?>/contacts_add.php"><button class='btn btn-med btn-primary link-btn'>Add New Contact</button></a>
	</div>
	
	<div>
		Search Contacts: <input type=text id=searchcontacts class=form-control style='display:inline-block;width:auto;' />
	</div>
	<div>
		Filter by type: <select id=filter_type class=form-control multiple title="Hold down 'ctrl' to select multiple categories">
			<?php
			$type = array();
			$q = "SELECT * FROM contact_type ORDER BY id ASC";
			if($r = mysql_query($q)){
				while($type = mysql_fetch_array($r)){
					$ctype[$type['id']] = $type['type'];
					echo "<option value='".$type['id']."'>".$type['type']."</option>";

				}
			}
			?>
			</select>
			<button class='btn btn-med btn-primary' id=filter_button>Filter Results</button>
	</div>
	<div id=filtered>
		<?php
		if($_REQUEST['filter']){
			echo "Filtered by:<ul>";
			$filters = explode("::",$_REQUEST['filter']);
			foreach($filters as $k => $v){
				echo "<li fv='$v'>".$ctype[$v]."</li>";
			}
			echo "</ul><button class='btn btn-med btn-primary' id=clear_filter>Clear Filter</button>";
		}

		?>
	</div><!-- filtered -->
	
	<p style='margin-top:5px'><small>N.B. This list displays volunteers from the 'staff' database, as well as all contacts in the 'Contacts' Database, but if you need to edit a volunteer, you'll be taken to the staff database to do it.</small></p>
	
</div>


<?php

$contactlist = "";
$q = "SELECT id,category,organisation,firstname,lastname,telephone,email,address,postcode FROM contacts UNION SELECT 'staff' as id,'27' as category,'Cre8' as organisation,firstname,lastname,telephone,email,address,postcode FROM staff WHERE volunteer = '1' ORDER BY $sortby $direction";
if(!($r = mysql_query($q))){
	echo "Error getting contacts. ".mysql_error();
}else{
	echo "<table id=contacts class='table'><thead>";
	echo "<tr class=stickyheader>";
	echo sortable_header('category','Category',$sortby,$direction);
	echo sortable_header('organisation','Organisation',$sortby,$direction);
	echo sortable_header('firstname','First Name',$sortby,$direction);
	echo sortable_header('lastname','Surname',$sortby,$direction);
	echo sortable_header('telephone','Phone',$sortby,$direction);
	echo sortable_header('email','Email',$sortby,$direction);
	echo sortable_header('address','Address',$sortby,$direction);
	echo "</tr>";	
	echo "<tr class='floatheader sticky' style='display:none;'>";
	echo sortable_header('category','Category',$sortby,$direction);
	echo sortable_header('organisation','Organisation',$sortby,$direction);
	echo sortable_header('firstname','First Name',$sortby,$direction);
	echo sortable_header('lastname','Surname',$sortby,$direction);
	echo sortable_header('telephone','Phone',$sortby,$direction);
	echo sortable_header('email','Email',$sortby,$direction);
	echo sortable_header('address','Address',$sortby,$direction);
	echo "</tr></thead>";
	$odd = false;
	while($contact = mysql_fetch_array($r)){
		$sc = "";
		$categories = explode("::",$contact['category']);
		foreach($categories as $k => $v){
			$sc .= " type$v";
		}
		
		if($odd) {$sc .= " odd"; $odd = false;}else{$odd = true;}
		echo "<tr id=contact_".$contact['id']." class='clickable_contact $sc'>";

		//echo "<td>".$contact['id']."</td>";
		echo "<td>";
		foreach($categories as $k => $v){
			echo $ctype[$v]."<br />";
		}
		echo "</td>";
		echo "<td>".$contact['organisation']."</td>";
		echo "<td>".$contact['firstname']."</td>";
		echo "<td>".$contact['lastname']."</td>";
		echo "<td>".$contact['telephone']."</td>";	
		echo "<td>".$contact['email']."</td>";	
		echo "<td>".$contact['address']." ".$contact['postcode']."</td>";
		//echo "<td><button class=edit id=edit_".$contact['id'].">View / Edit</button><button class=delete id=delete_".$contact['id'].">Delete</button></td>";
		
		echo "</tr>";
		$contactlist .= "{label:\"".$contact['organisation']." ".$contact['firstname']." ".$contact['lastname']."\",value: \"".$contact['id']."\"},".PHP_EOL;
	}
	echo "</table>";
	$contactlist = trim($contactlist, ",");
}
?>
<div class="modal fade" id=editcontact role="dialog" aria-labelledby="editcontactlabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id='editstafflabel'>View/Edit Contact</h4>
      </div>
      <div class="modal-body">
			<div id='summarytable' class=edittoggle></div>
			<form id=editcontactform action="" method=Post class='edittoggle' style='display:none;'>
			<input type=hidden name=contactid id=contactid />
			
			<?php require('contacts_form.php'); ?>

			</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" onclick="$('.edittoggle').toggle();"><span class='edittoggle'>Edit</span><span class='edittoggle' style='display:none;'>Cancel Editing</span></button>
        <button type="button" class="btn btn-primary edittoggle" onclick="printtable();">Print</button>
        <button type="button" class="btn btn-primary edittoggle" style='display:none;' onclick='saveContact();'>Save Changes</button>
		<? if($admin) echo "<button type='button' class='btn btn-danger edittoggle' style='display:none;' onclick='deleteContact();'>Delete Worker</button>";?>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<script src='<?php echo ROOT_PATH;?>/js/contacts_functions.js'></script>

<script>
$( document ).ready(function() {

	contacts = [<?echo $contactlist ?>];
	
	// get table header width
	var stickyNavTop = $('table#contacts tr.stickyheader').offset().top;
	col = new Array();
	$('table#contacts tr.stickyheader th').each(function(){
		col.push($(this).css('width'));
	});
	
	// setup autocomplete
	$('#searchcontacts').autocomplete({
		source: contacts,
		select: function( event, ui ) {
			event.preventDefault();
       		id = ui.item.value;
			
			console.log(id);
			if(id){
				if(id == 'staff'){
					if(confirm("This contact is listed as a worker / volunteer and should be edited in the staff database. Would you like to go there now?")== true){
						location = "viewstaff.php";
					}
				}else{
					
					$('tr#contact_'+id).children('td').last().append("<img src='img/ajax-loader-sm.gif' id=ajaxload style='margin-left:10px;' />");
					getContactDetails(id);
					
				}
			}
		}
	});
	
	// move auto complete list to the menu, so that the hover function works properly
	$('.ui-autocomplete').appendTo('#navsidemenu');
	
	// create a sticky header when the window is scrolled down
		$(window).scroll(function() {
	  	var scrollTop = $(window).scrollTop();
		if(scrollTop > stickyNavTop) {
		  $('table#contacts tr.floatheader').show();
		  var i = 0;
			$('table#contacts tr.floatheader th').each(function(){
				$(this).css('width',col[i]);
				i++;
			}); 
		} else {
		  $('table#contacts tr.floatheader').hide();
		}
	});
		

});
</script>

<?php
require('footer.php');
?>