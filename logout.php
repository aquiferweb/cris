<? 
session_start();
session_unset('CrisUsername');
session_unset('CrisLoggedIn');
session_unset();     // unset $_SESSION variable for the run-time 
session_destroy();   // destroy session data in storage*/
header('location:login.php');
?>