    <div class="navbar navbar-inverse navbar-fixed-top hidden-print" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
		  <img src='<?php echo ROOT_PATH;?>/img/cre8logo.jpg' />
          <a class="navbar-brand" href="#">CRIS</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
			<li><a href="<?php echo ROOT_PATH;?>/index.php">Home</a></li>
			<li><a href="<?php echo ROOT_PATH;?>/yp_view.php">Young People</a></li>
			<li><a href="<?php echo ROOT_PATH;?>/staff_view.php">Workers</a></li>
			<?php if($admin) echo "<li><a href='".ROOT_PATH."/contacts_view.php' >Contacts</a></li>"; ?>		
			<?php if($admin) echo "<li><a href='".ROOT_PATH."/projects_view.php'>Projects</a></li>"; ?>
			<li><a href="<?php echo ROOT_PATH;?>/sessions_view.php">Sessions</a></li>
			<li><a href="<?php echo ROOT_PATH;?>/measures_view.php">Measures</a></li>
			<li><a href='<?php echo ROOT_PATH;?>/reports_view.php'>Reports</a></li>
			<li><a href="<?php echo ROOT_PATH;?>/logout.php">Logout</a></li>			
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>