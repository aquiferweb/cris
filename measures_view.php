<?php

require('header.php');

require('navmenutop.php');


if(isset($_GET['sortby'])){
	$sortby = $_GET['sortby'];
}else{
	$sortby = 'category';
}
if(isset($_GET['direction'])){
	$direction = $_GET['direction'];
}else{
	$direction = 'ASC';
}

$q = "SELECT firstname,lastname,id FROM participants ORDER BY lastname ASC";
	if($r = mysql_query($q)){
		while($yp = mysql_fetch_array($r)){
			$yplist .= "{label:'".$yp['firstname']." ".$yp['lastname']."',value: '".$yp['id']."'},";
		}
		$yplist = trim($yplist, ",");
	}else{echo "<div class='alert alert-warning'>Problem getting young people.".mysql_error()."</div>";}

?>

<div id=container style=''>
	<div id=toolbar class=hidden-print>

		<div class=first><span style="font-weight: bold;font-size: 200%;vertical-align: middle;">Measures Database</span></div>
		<div><button class='btn btn-med btn-primary link-btn' onclick='javascript:window.print()'>Print</a></button></div>	
		<?php
		if($admin){
		?>
		<div><button class='btn btn-med btn-primary link-btn'>Add New Measure</button></div>
		<?php
		}	
		?>
		<div><a href="measures_record.php"><button class='btn btn-med btn-primary link-btn'>Record Measure Interview</button></a></div>
		<div><input type=text id=searchyp class=form-control style='display:inline-block;width:auto;' /> <span>View Measures for a YP (type name in box)</span>	</div>
	</div>
	<div style='text-align:center; float:left;padding-right:20px;width:40%;' class='hidden-print'>
		

		<?php
			$q = "SELECT 
			measures_data.participant_id AS pid, 
			participants.firstname, 
			participants.lastname, 
			MAX(measures_data.date) AS mdate, 
			COUNT(DISTINCT measures_data.date) AS num_int, 
			COUNT(DISTINCT(measures_data.measure_id)) AS num_measures, 
			(SELECT GROUP_CONCAT('proj_',projects_consented.project_id SEPARATOR ' ') FROM projects_consented WHERE projects_consented.participant_id = pid) AS projid
			FROM measures_data 
			LEFT JOIN participants ON measures_data.participant_id = participants.id 
			LEFT JOIN projects_consented ON projects_consented.participant_id = measures_data.participant_id
			GROUP BY measures_data.participant_id 
			ORDER BY mdate DESC";
			if($r = mysql_query($q)){
				if(mysql_num_rows($r) > 0){
				?>
					<h2>Latest Measure Interviews</h2>
					<p>Showing latest interview for each YP with measures recorded (<?php echo mysql_num_rows($r); ?> young people)</p>
					<p>
					
					<?php
					
					$query = "SELECT id, name FROM projects WHERE active = '1' ORDER BY name ASC";
					if($result = mysql_query($query)){
						echo "Filter by Project: <select id='filterproj'>";
						echo "<option disabled selected value=''> -- Choose Project -- </option>";
						while($project = mysql_fetch_array($result)){
							echo "<option value='".$project['id']."'>".$project['name']."</option>";						
						}
						echo "</select>";
					}else{echo "<div class='alert alert-warning'>Failed to get project list.</div>";}
				
					?>
					
					<table class='table cre8table' id='latest_interviews'>
					<thead><tr><th>Name</th><th>Date of<br />latest interview</th><th>Num<br />Interviews</th><th>Num<br />Measures</th></tr></thead>
					<tbody>
					<?php
					while($interview = mysql_fetch_assoc($r)){
						echo "<tr id='yp_".$interview['pid']."' class='previnterviews ".$interview['projid']."'><td>".$interview['firstname']." ".$interview['lastname']."</td><td>".$interview['mdate']."</td><td>".$interview['num_int']."</td><td>".$interview['num_measures']."</td></tr>";
					}
					?>
					</tbody>
					</table>
					<?php
				}
			}else{
				echo "Error: ".mysql_error();
			}
		?>
		


	</div>
	<div style='text-align:center;float:left;width:60%;' id=ypdetails class=start-hidden >
		
		<h2 id=ypdetails_title>Showing Interviews for</h2>




















		<table id=yptable class='table cre8table start-hidden'>
		<thead>
		<tr><th>Measure</th><th>Description</th><th>Date</th><th>Score</th><th>Recorded By</th><th>Interview By</th><th>Notes</th></tr>
		</thead>
		<tbody>
		</tbody>
		</table>

		<canvas id="myChart" width="800" height="800" class='hidden-print'></canvas>
		<div id=chartlegend class='hidden-print'></div>

	</div>
	
	<div id=activemeasures style='text-align:center;float:left;'>
	<?php
	
	echo "<h2>List of Active Measures</h2>";
	echo "<table class='cre8table table table-striped hidden-print'>";
	echo "<thead><tr><th>ID</th><th>Category</th><th>Text</th><th>Description</th></tr></thead>";
	$q = "SELECT * FROM measures ORDER BY $sortby $direction";
	if($r = mysql_query($q)){
		while($measure = mysql_fetch_array($r)){
			

			echo "<tr>";
			echo "<td>".$measure['id']."</td>";
			echo "<td>".$measure['category']."</td>";
			echo "<td>".$measure['measure_text']."</td>";
			echo "<td>".$measure['description']."</td>";
			echo "</tr>";
		}
	}else{
		echo "<div class='alert alert-warning'>Error accessing measures list. ".mysql_error()."</div>";
	}




	echo "</table>";	?>
	
	</div>




</div>
<?php
?>
<script src='<?php echo ROOT_PATH;?>/js/measures_functions.js'></script>
<script src="<?php echo ROOT_PATH;?>/js/Chart.js"></script>
<script>
yplist = [<?echo $yplist ?>];
$('#searchyp').autocomplete({
		source: yplist,
		select: function( event, ui ) {
			event.preventDefault();
       		id = ui.item.value;
			$('#searchyp').val(ui.item.label);
			showYp(id);
		}
		
});
</script>
<?php
require('footer.php');
?>