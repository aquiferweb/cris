$(document).ready(function(){




	$('#choosemeasure').change(function(){
		mid = $(this).val();
		if($('#currentmeasures tbody tr#measure_'+mid).length < 1){
			$('#currentmeasures tbody').prepend("<tr id=measure_"+mid+" class='measure_row'><td>"+$('#choosemeasure option[value="'+mid+'"]').text()+"</td><td><input type=number min=0 max=10 name=score_"+mid+" class=form-control /></td><td><textarea name=notes_"+mid+" class=form-control ></textarea></td><td class='remove_measure'>X</td></tr>");
			$('#choosemeasure option').first().prop('selected',true);
		}
	
	});
	
	$('#clearform').click(function(e){
		$('#currentmeasures tbody tr:not(.choosemeasure)').remove();
	
	})
	
	$('#currentmeasures tbody').on('click','.remove_measure',function(){
		$(this).parent('tr').remove();
	
	});
	
	$('.previnterviews').click(function(){
		ypid = $(this).attr('id').split('_');
		showYp(ypid[1]);
	
	});
	
	
	$('#uselastmeasures').click(function(e){
		$('#currentmeasures tbody tr:not(.choosemeasure)').remove();
		ypid = $('#ypid').val();
		getlast = $.ajax({
			url: "crisapi.php",
			type: "POST",
			data: "q=getlastmeasures&ypid="+ypid,
			dataType: 'json'
			
		});
		getlast.done(function( msg ) {
			console.log(msg);
			if(msg !== null){
				for(var i = 0; i < msg.length; i++){
					$('#currentmeasures tbody').prepend("<tr id=measure_"+msg[i].mid+" class='measure_row'><td>"+$('#choosemeasure option[value="'+msg[i].mid+'"]').text()+"</td><td><input type=number min=0 max=10 name=score_"+msg[i].mid+" class='form-control measure_score'/></td><td><textarea name=notes_"+msg[i].mid+" class=form-control ></textarea></td><td class='remove_measure'>X</td></tr>");
				}
			}else{
				alert("No previous measure recordings found.");
			}
		});
		getlast.fail(function(jqXHR, textStatus){
			console.log('failed to get record. '+textStatus);
			alert("There was a problem getting the record. Please check your connection and try again");
		});
	
	});
	
	
	$('#recordmeasure').submit(function(e){
		//e.preventDefault();
		return submitMeasureInterview();
		});
		
	$('#filterproj').change(function(){
		projid = $('#filterproj').val();
		
		//$('table#latest_interviews tr.previnterviews').hide();
		if(!projid){
			$('table#latest_interviews tr.previnterviews').show();
		}else{
			$('table#latest_interviews tr.previnterviews').hide();
		}
		$('table#latest_interviews tr.proj_'+projid).show();
		
	});

});

function submitMeasureInterview(){
		if($('#ypid').val() == '' || $('#ypid').val() == '0' || !($('#ypid').val())){
			alert("Please specify a young person!");
			$('#yp').focus();
			return false;
		}else if($('#interviewed_by').val() == '' || $('#interviewed_by').val() == '0' || !($('#interviewed_by').val())){
			alert("Please specify a worker who carried out the interview");
			$('#interviewed_by').focus();
			return false;
		}else if($('#project_id').val() == '' || $('#project_id').val() == '0' || !($('#project_id').val())){
			alert("Please specify a project to assign the interview to.");
			$('#project_id').focus();
			return false;
		}else if($('tr.measure_row').length == 0){
			alert("You have not selected any measures!");
			$('#choosemeasure').focus();
		}else{
			ypname = $('#yp').val();
			iby = $('#interviewed_by option:selected').text();
			project = $('#project_id option:selected').text();
			r = confirm("Submit interview for "+ypname+", by "+iby+" at "+project+"?");
			if (r == true) {
				return true;
			} else {
				return false;
			}
		}
		return false;
}

function showYp(ypid){
		showYP = $.ajax({
			url: "crisapi.php",
			type: "POST",
			data: "q=showypmeasures&ypid="+ypid,
			dataType: 'json'
			
		});
		showYP.done(function( msg ) {
			//console.log(msg);
			//console.log(msg.m);
			//console.log(msg.m.length);
			$('#ypdetails').show();
			$('#yptable').show();
			$('#ypdetails_title').html("Showing Interviews for "+msg.m[0].ypfn+" "+msg.m[0].ypln);
			$('#activemeasures').hide();
			$('#yptable tbody').html('');
			var oldmid = "";
			var labellist = [];
			
						
			// do table of measures
			for(var i = 0;i < msg.m.length; i++){
				var mid = msg.m[i].measure_id;
				console.log(msg.m[i]);
				if(mid == oldmid){
					// carry on with same measure
					$('#yptable tbody').append('<tr><td></td><td></td><td>'+msg.m[i].date+'</td><td>'+msg.m[i].score+'</td><td>'+msg.m[i].rfirstname+' '+msg.m[i].rlastname+'</td><td>'+msg.m[i].ifirstname+' '+msg.m[i].ilastname+'</td><td>'+msg.m[i].notes+'</td></tr>');			
				}else{
					// new measure
					$('#yptable tbody').append('<tr id=m_'+msg.m[i].measure_id+'><td>'+msg.m[i].measure_text+'</td><td>'+msg.m[i].description+'</td><td>'+msg.m[i].date+'</td><td>'+msg.m[i].score+'</td><td>'+msg.m[i].rfirstname+' '+msg.m[i].rlastname+'</td><td>'+msg.m[i].ifirstname+' '+msg.m[i].ilastname+'</td><td>'+msg.m[i].notes+'</td></tr>');
					labellist.push(msg.m[i].measure_text);
				}
				oldmid = mid;
			}
			
			// do graph
			var chartdset = "";
			var oldmdate = "";
			var dataset = [];
			var datalist = [];
			var col = 0;
			var collarray = ['rgba(140,45,4,0.8)','rgba(217,72,1,0.8)','rgba(241,105,19,0.8)','rgba(253,141,60,0.8)','rgba(253,174,107,0.8)','rgba(253,208,162,0.8)','rgba(254,230,206,0.8)','rgba(255,245,235,0.8)'];
			for(var i = 0;i < msg.d.length; i++){
				
				var mdate = msg.d[i].date;
				//console.log(mdate);
				if(mdate == oldmdate){
					// carry on with same date
					datalist.push(msg.d[i].score);		
				}else{
					if(datalist.length > 0){
						// store data from old date
						dataset.push({
							label:oldmdate, 
							data: datalist,			
							fillColor: collarray[col],
							strokeColor:  collarray[col],
							pointColor:  collarray[col],
							pointStrokeColor: "#fff",
							pointHighlightFill: "#fff",
							pointHighlightStroke:  collarray[col]
						});
					}
					// new date
					datalist = [];
					datalist.push(msg.d[i].score);
					col = col + 1;	
				}
				oldmdate = mdate;
				
			}
			col = col + 1;	
			dataset.push({
				label:oldmdate, 
				data: datalist,
				fillColor: collarray[col],
				strokeColor:  collarray[col],
				pointColor:  collarray[col],		
				pointStrokeColor: "#fff",
				pointHighlightFill: "#fff",
				pointHighlightStroke:  collarray[col]
			});
			
			//console.log(dataset);
			//console.log(labellist);
			
			//$('#yptable').after('<canvas id="myChart" width="800" height="800"></canvas>');
			// Get context with jQuery - using jQuery's .get() method.
			var ctx = $("#myChart").get(0).getContext("2d");
			// This will get the first returned node in the jQuery collection.
			
			var data = {
				labels: labellist,
				datasets: dataset
			};
			

			//console.log(data);
			if(myRadarChart){myRadarChart.destroy();}
			$('#myChart').show();
			var myRadarChart = new Chart(ctx).Radar(data,{datasetFill : false,scaleShowLabels : true});
			
			$('#chartlegend').html(myRadarChart.generateLegend());
		});
		showYP.fail(function(jqXHR, textStatus){
			console.log('failed to get record. '+textStatus);
			alert("There was a problem getting the record. Please check your connection and try again");
		});
	

}