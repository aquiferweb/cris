// Reports functions

$(document).ready(function(){
	$( ".sortable" ).sortable({ items: "div.reportfield" });
    //$( ".sortable" ).disableSelection();
	var sb = $('#sortby').text();
	$('#'+sb).append('*');

});


$('#savereportform').submit(function(e){
	e.preventDefault();
	var reportname = prompt("Please enter a title for this report:", "Enter Title Here");
    
    if (reportname != null && reportname != "Enter Title Here") {
        $(this).append("<input type=hidden name=reportname value='"+reportname+"' />").submit();
		return true;
    }else{
		alert("You need to enter a project title!");
		return false;
	}

})

$('#choosecolsdiv').delegate('#addcol', 'click', function() {
	colname = $('#choosecol').val();
	coltype = $("#choosecol option:selected").attr('coltype');	
	table = $('#choosetable').val();
	coltype = coltype.split("(");
	addcol(colname,coltype[0],table);
	console.log(colname);
	console.log(coltype[0]);

});


$('#choosetable').change(function(){
	table = $(this).val();
	tabletitle = $(this).children('option:selected').html();
	$('#choosecolsdiv').html("<img src='img/ajax-loader-sm.gif' />")
	getcols = $.ajax({
		url: "crisapi.php",
		type: "POST",
		data: "q=getcolumns&table="+table
	});
	getcols.done(function( msg ) {
		
		$('div.reportfield').remove();
		$('#choosecolsdiv').html( msg );
		if(table == 'participants' || table == 'staff') {	
			addcol('firstname','varchar',table);
			addcol('lastname','varchar',table);
		}else if(table == 'projects'){
			addcol('name','varchar',table);
		
		}
		$('#reportfields h1').html("Report on "+tabletitle);
		$('.start-hidden').show();
	});
	getcols.fail(function(jqXHR, textStatus){
		alert("There was a problem getting the table columns from the database. Please check your connection and try again. "+textStatus);
		
	});
	
	

});

$('#reportfields').delegate('.delcol','click',function(){
	$(this).parent('div.reportfield').remove();
});

function addcol(colname,coltype,table){
	
	$('#reportfields').append("<div class='reportfield' id='"+colname+"_col'><div class=delcol>X</div><h3>"+colname+"</h3><div class='sortdiv'>Sort by this field <input type=radio name=sortby value='"+colname+"' /><br />Show this field in the results <input type=checkbox checked name='"+colname+"_show' /></div><input name=reportfields[] type=hidden value='"+colname+"::"+coltype+"' /></div>");
	switch(coltype){
		
		case 'int':
			
			$('#'+colname+"_col").append("<div class='field_radio' style='margin-left:20px;'><input type=radio name='"+colname+"_radio' value=all checked onclick=\"$(this).parent().nextAll('div').hide();\" /> Show all values<br /><input type=radio name='"+colname+"_radio' value=range onclick=\"$(this).parent().nextAll('div').hide();$(this).parent().nextAll('div.range').show();\" /> Select Range<br /><input type=radio name='"+colname+"_radio' value=specific onclick=\"$(this).parent().nextAll('div').hide();$(this).parent().nextAll('div.specific').show();\" /> Select specific values</div><div style='display:none;margin-left:20px;' class='specific'>Enter Values to search for, separated by commas. <input name='"+colname+"_specific' type=text /></div><div style='display:none;margin-left:20px;' class='range'>Enter Range. <br /><span style='width:100px'>Greater than</span><input name='"+colname+"_from' type=number step=1 /><br /><span style='width:100px'>Less than</span><input name='"+colname+"_to' type=number step=1 /></div>");
			
			break;
	
		case 'varchar':
			if(table == 'projects' && colname == 'name'){
				$('#'+colname+"_col").append("Choose projects to filter the report by.");
				$('#chooseproject').clone().prop('id','').attr('name','project_name[]').prop('multiple',true).appendTo($('#'+colname+"_col"));
			}else{
				$('#'+colname+"_col").append("Enter text to search for: <input name='"+colname+"_varchar' type=text />");
			}
			break;

		case 'date':
			$('#'+colname+"_col").append("Enter Range. <br /><span style='width:100px'>Greater than</span><input name='"+colname+"_from' type=date /><br /><span style='width:100px'>Less than</span><input name='"+colname+"_to' type=date />");
			break;	
			
		case 'tinyint':
			$('#'+colname+"_col").append("<input type=radio name='"+colname+"_radio' value=all checked /> Show all values<br /><input type=radio name='"+colname+"_radio' value=yes  /> Show only 'Yes'<br /><input type=radio name='"+colname+"_radio' value=no /> Show only 'No'");
			break;	
			
		case 'text':
			$('#'+colname+"_col").append("Enter text to search for: <input name='"+colname+"_varchar' type=text />");
			break;
			
		case 'interestproj':
			$('#'+colname+"_col").append("Choose projects to filter the report by.");
			$('#chooseproject').clone().prop('id','').attr('name',coltype+'_project[]').prop('multiple',true).appendTo($('#'+colname+"_col"));
			break;	
			
		case 'regproj':
			$('#'+colname+"_col").append("Choose projects to filter the report by.");
			$('#chooseproject').clone().prop('id','').attr('name',coltype+'_project[]').prop('multiple',true).appendTo($('#'+colname+"_col"));
			
			break;
			
		case 'activeproj':
			$('#'+colname+"_col").append("Choose projects to filter the report by.");
			$('#chooseproject').clone().prop('id','').attr('name',coltype+'_project[]').prop('multiple',true).appendTo($('#'+colname+"_col"));
			break;
		case 'attendproj':
			$('#'+colname+"_col").append("Choose projects to filter the report by.");
			$('#chooseproject').clone().prop('id','').attr('name',coltype+'_project[]').prop('multiple',true).appendTo($('#'+colname+"_col"));
			$('#'+colname+"_col").append("<br />");
			$('#'+colname+"_col").append("Define Date Range (leave blank to ignore). <br /><span style='width:100px'>Greater than</span><input name='"+coltype+"_from' type=date /><br /><span style='width:100px'>Less than</span><input name='"+coltype+"_to' type=date />");
			break;
		case 'regyp':
			//$('#'+colname+"_col").append();
			//$('#chooseproject').clone().prop('id','').attr('name',coltype+'_project[]').prop('multiple',true).appendTo($('#'+colname+"_col"));
			break;
	}	
	$('#'+colname+"_col").append("<div class='floatbreak'></div>");
	$( ".sortable" ).sortable("option", "items", "div.reportfield" );

}