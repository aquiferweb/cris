	// Participants Page
	
	$(document).ready(function(){
		$('#addnewyp .session_input').hide();
		$('#addnewyp .session_input').first().show().addClass('current');
		$('#addnewyp .session_input').each(function(){
			$(this).append("<div class=buttons><span style='display:inline-block;width:200px;text-align:center;'><button class='prev_section btn btn-primary btn-sm'>Previous Section</button></span><b>Section "+$(this).index()+" of "+$('.session_input').length+"</b><span style='display:inline-block;width:200px;text-align:center;'><button class='next_section btn btn-primary btn-sm'>Next Section</button></span></div>");
		if($('.session_input.current').index() == 1){
			$('.prev_section').hide();
		}else{
			$('.prev_section').show();
		}
		});
		
		$('#container').hide();
		
		// hide inactive yp
		$('.clickable_yp').hide();
		//console.log($('#showCVA').prop('checked'));
		if($('#showCVA').prop('checked')){
			$('.clickable_yp').has('td.active.consented.valid').show();
		}
		if($('#showCA').prop('checked')){
			$('.clickable_yp').has('td.active.consented.old').show();
		}
		if($('#showA').prop('checked')){
			$('.clickable_yp').has('td.active:not(.consented)').show();
		}
		if($('#showCV').prop('checked')){
			$('.clickable_yp').has('td.project.consented.valid').show();
		}
		if($('#showC').prop('checked')){
			$('.clickable_yp').has('td.project.consented.old').show();
		}
		
		
		$(window).load(function(){
			$("#loading").hide();
			$("#container").fadeIn("slow");
			var stickyNavTop = $('table#participants thead').offset().top;
			window.snt = stickyNavTop;
			//console.log(stickyNavTop);
			col = new Array();
			// 'fix' the auto generated widths of the table
			$('table#participants tbody tr:first').children('td').each(function(){
				col.push($(this).css('width'));console.log($(this).css('width'));
				$(this).css('width',$(this).css('width'));
			});
		});
		
		// create a sticky header when the window is scrolled down
		$(window).scroll(function() {
			var scrollTop = $(window).scrollTop();
			//console.log(scrollTop);
			if(scrollTop > window.snt) {
			  $('table#participants thead').addClass('sticky');
			//  $('table#participants tr.stickyheaderB').addClass('stickyB');
			  var i = 0;
				$('table#participants tr.stickyheader th').each(function(){
					$(this).css('width',col[i]);
					i++;
				}); 
			} else {
			  $('table#participants thead').removeClass('sticky');
			  //$('table#participants tr.stickyheaderB').removeClass('stickyB');
			}
		});	

	});
	
	
	$('#addnewyp').delegate('.prev_section', 'click', function() {
		if($('.session_input.current').prev('.session_input').length > 0){
			$('.session_input.current').hide().removeClass('current').prev('.session_input').show().addClass('current');	
		}
		
		if($('.session_input.current').index() == 1){
			$('.prev_section').hide();
		}else{
			$('.prev_section').show();
		}
		if($('.session_input.current').index() == $('.session_input').length){
			$('.next_section').hide();
		}else{
			$('.next_section').show();
		}
		return false;
	});
	$('#addnewyp').delegate('.next_section', 'click', function() {
		if($('.session_input.current').next('.session_input').length > 0){
			$('.session_input.current').hide().removeClass('current').next('.session_input').show().addClass('current');
		}	

		if($('.session_input.current').index() == $('.session_input').length){
			$('.next_section').hide();
		}else{
			$('.next_section').show();
		}
		if($('.session_input.current').index() == 1){
			$('.prev_section').hide();
		}else{
			$('.prev_section').show();
		}
		return false;
	});
	
	$('.projecttick').change(function(){
		if(this.checked){
			$(this).next('input').attr('disabled',false);
		}else{
			$(this).next('input').attr('disabled',true);
		}
	
	});

	
	$('.ypfilter').change(function(){
		$('.clickable_yp').hide();
		//console.log($('#showCVA').prop('checked'));
		if($('#showCVA').prop('checked')){
			$('.clickable_yp').has('td.active.consented.valid').show();
		}
		if($('#showCA').prop('checked')){
			$('.clickable_yp').has('td.active.consented.old').show();
		}
		if($('#showA').prop('checked')){
			$('.clickable_yp').has('td.active:not(.consented)').show();
		}
		if($('#showCV').prop('checked')){
			$('.clickable_yp').has('td.project.consented.valid').show();
		}
		if($('#showC').prop('checked')){
			$('.clickable_yp').has('td.project.consented.old').show();
		}
	
	});
	
	$('.filterproj').change(function(){
		//console.log($(this));
		$('tr.clickable_yp').hide();
		$('.filterproj').each(function(){
			pid = $(this).attr('id');
			console.log(pid+" "+this.checked);
			pid = pid.split('_');
			if(this.checked){
				$('td.projclass_'+pid[1]).parent('tr').show();
				//console.log(pid[1]);
			}
		
		});
		
	});
	
	$('#checkall').change(function(){
		if(this.checked){
			$('.filterproj').prop('checked',true);
			//$('#checkalltext').html('Deselect all'); 
		}else{
			$('.filterproj').prop('checked',false);
			//$('#checkalltext').html('Select all');
		}
		$('tr.clickable_yp').hide();
		$('.filterproj').each(function(){
			pid = $(this).attr('id');
			pid = pid.split('_');
			if(this.checked){
				$('td.projclass_'+pid[1]).parent('tr').show();
			}
		});	
	});
	
	
	


	$('table#participants').delegate('.clickable_yp', 'click', function() {
		id = $(this).attr('id').split('_');
		//console.log(id);
		if(id[1]){

			$(this).children('td').last().append("<img src='img/ajax-loader-sm.gif' id=ajaxload style='margin-left:10px;' />");
			getYp(id[1]);	

		}
		return false;
	});

	$('#dob').change(function(){
		suggest_year = get_school_year($(this).val());
		$('#suggest_year').html(" Suggested Year "+suggest_year);
		$('#calc_age').html(" Age "+get_age($(this).val()));
		$("#addnewyp #school_year option[value='Year "+suggest_year+"']").attr('selected','true');
	});

	$('#edityp #consentlist').on('click','.delconsent',function(e){
		var cid = $(this).parent('li').attr('cid');
		$('#consentlist ul li[cid='+cid+']').remove();
		$('#delconsentlist').val($('#delconsentlist').val()+"::"+cid);

	});
	
	$('#edityp #interestlist').on('click','.delinterest',function(e){
		var cid = $(this).parent('li').attr('cid');
		$('#interestlist ul li[cid='+cid+']').remove();
		$('#delinterestlist').val($('#delinterestlist').val()+"::"+cid);

	});
	
	$('#addnewyp #consentlist').on('click','.delconsent',function(e){
		var pid = $(this).parent('li').attr('pid');
		$('#consentlist ul li[pid='+pid+']').remove();
		//$('#projectconsent').val($('#projectconsent').val()+"::"+pid);
		var list = $('#projectconsent').val();
		list = list.split('::');
		$('#projectconsent').val('');
		for(var i = 0; i < list.length; i++){
			listb = list[i].split('@');
			if(listb[0] != pid){
				$('#projectconsent').val($('#projectconsent').val()+"::"+listb[0]+"@"+listb[1]);
			}
		}

	});
	
	$('#addnewyp #interestlist').on('click','.delinterest',function(e){
		var pid = $(this).parent('li').attr('pid');
		$('#interestlist ul li[pid='+pid+']').remove();
		//$('#projectconsent').val($('#projectconsent').val()+"::"+pid);
		var list = $('#projectinterest').val();
		list = list.split('::');
		$('#projectinterest').val('');
		for(var i = 0; i < list.length; i++){
			listb = list[i].split('@');
			if(listb[0] != pid){
				$('#projectinterest').val($('#projectinterest').val()+"::"+listb[0]+"@"+listb[1]);
			}
		}

	});
	
	$('#edityp #newinterestsubmit').click(function(e){
		e.preventDefault();
		var projectid = $('#newinterestp').val();
		var participantid = $('#ypid').val();
		var cdate = $('#newinterestdate').val();
		var projname = $('#newinterestp option[value="'+projectid+'"]').html();
		$('#newinterestsubmit').button('loading');
		if(projectid != '' && projectid != '0' && cdate != ''){
			addinterest = $.ajax({
				url: "crisapi.php",
				type: "POST",
				data: "q=addinterest&proj="+projectid+"&yp="+participantid+"&date="+cdate,
				dataType: 'json'
				
			});
			addinterest.done(function( msg ) {

				$('#interestlist ul').append("<li cid="+msg+">"+projname+" - "+cdate+" <span class='delinterest'>X</span></li>");
				$('#newinterestsubmit').button('reset');
				$('#newinterestp').val('');
				$('#newinterestdate').val('');
			});
			addinterest.fail(function(jqXHR, textStatus){
				console.log('failed to get record. '+textStatus);
				alert("There was a problem updating the record. Please check your connection and try again");
				$('#newinterestsubmit').button('reset');
			});
		}else{
			alert('Please enter a valid project and date');
			$('#newinterestdate').focus();
		}
	
	});
	
	$('#edityp #newconsentsubmit').click(function(e){
		e.preventDefault();
		var projectid = $('#newconsentp').val();
		var participantid = $('#ypid').val();
		var cdate = $('#newconsentdate').val();
		var projname = $('#newconsentp option[value="'+projectid+'"]').html();
		if(projectid != '' && projectid != '0' && cdate != ''){
			$('#newconsentsubmit').button('loading');
			addconsent = $.ajax({
				url: "crisapi.php",
				type: "POST",
				data: "q=addconsent&proj="+projectid+"&yp="+participantid+"&date="+cdate,
				dataType: 'json'
				
			});
			addconsent.done(function( msg ) {

				$('#consentlist ul').append("<li pid="+projectid+" cid="+msg+">"+projname+" - "+cdate+" <span class='delconsent'>X</span></li>");
				$('#newconsentsubmit').button('reset');
				$('#newconsentp').val('');
				$('#newconsentdate').val('');
			});
			addconsent.fail(function(jqXHR, textStatus){
				console.log('failed to get record. '+textStatus);
				alert("There was a problem updating the record. Please check your connection and try again");
				$('#newconsentsubmit').button('reset');
			});
		}else{
			alert('Please enter a valid project and date');
			$('#newconsentdate').focus();
		}
	});
	
	$('#addnewyp #newconsentsubmit').click(function(e){
		e.preventDefault();
		var projectid = $('#newconsentp').val();
		var participantid = $('#ypid').val();
		var cdate = $('#newconsentdate').val();
		if(projectid != '' && projectid != '0' && cdate != ''){
			var projname = $('#newconsentp option[value="'+projectid+'"]').html();
			$('#consentlist ul').append("<li pid="+projectid+">"+projname+" - "+cdate+" <span class='delconsent'>X</span></li>");
			$('#projectconsent').val($('#projectconsent').val()+"::"+projectid+"@"+cdate);
			$('#newconsentp').val('');
			$('#newconsentdate').val('');
		}else{
			alert('Please enter a valid project and date');
			$('#newconsentdate').focus();
		}
		
	
	});

	$('#addnewyp #newinterestsubmit').click(function(e){
		e.preventDefault();
		var projectid = $('#newinterestp').val();
		var participantid = $('#ypid').val();
		var cdate = $('#newinterestdate').val();
		console.log("date = "+cdate);
		if(projectid != '' && projectid != '0' && cdate != ''){
			var projname = $('#newinterestp option[value="'+projectid+'"]').html();
			$('#interestlist ul').append("<li pid="+projectid+">"+projname+" - "+cdate+" <span class='delinterest'>X</span></li>");
			$('#projectinterest').val($('#projectinterest').val()+"::"+projectid+"@"+cdate);
			$('#newinterestp').val('');
			$('#newinterestdate').val('');
		}else{
			alert('Please enter a valid project and date');
			$('#newinterestdate').focus();
		}
	
	});
	
	$('#edityp').on('shown.bs.modal',function (e){
		if($('#summarytable').is(":hidden"))	$('.edittoggle').toggle();
	
	});
	
	
	function getYp(id){
		
		getyp = $.ajax({
			url: "crisapi.php",
			type: "POST",
			dataType: "json",
			data: "q=getyp&table=participants&id="+id+"&idfield=participant_id"
		});
		getyp.done(function( msg ) {

			console.log(msg);
			
			$('#summarytable').html(msg.summary);
			$('#ypid').val(msg.id);
			$('#dob').val(msg.dob);
			
			$('#calc_age').html(' Age '+get_age(msg.dob));

			$('#start_date').val(msg.start_date);
			$('#key_worker option[value="'+msg.key_worker+'"]').attr('selected',true);
			$('#firstname').val(msg.firstname);
			$('#lastname').val(msg.lastname);
			$('#address').val(msg.address);
			$('#postcode').val(msg.postcode);
			$('#area').val(msg.area);
			$('#yp_phone').val(msg.yp_phone);
			$('#guardian_phone').val(msg.guardian_phone);
			$('#guardian_name').val(msg.guardian_name);
			
			//consented projects
			$('#referrer').val(msg.referrer);
			$('#consentlist ul').html('');
			$('#newconsentp').val('');
			$('#newconsentdate').val('');
			$('#delconsentlist').val('');
			if(msg.projects_consented){
				for(var i = 0; i < msg.projects_consented.length; i++){
					//$('#project_'+msg.projects_consented[i]['id']).attr('checked',true);
					//$('#pdate_'+msg.projects_consented[i]['id']).val(msg.projects_consented[i]['date']).attr('disabled',false);
					$('#consentlist ul').append("<li pid="+msg.projects_consented[i]['pid']+" cid="+msg.projects_consented[i]['cid']+">"+msg.projects_consented[i]['name']+" - "+msg.projects_consented[i]['date']+" <span class='delconsent'>X</span></li>");
				}
			}
			
			// interested projects
			$('#interestlist ul').html('');
			$('#newinterestp').val('');
			$('#newinterestdate').val('');
			$('#delinterestlist').val('');
			if(msg.projects_interested){
				for(var i = 0; i < msg.projects_interested.length; i++){
					$('#interestlist ul').append("<li pid="+msg.projects_interested[i]['pid']+" cid="+msg.projects_interested[i]['cid']+">"+msg.projects_interested[i]['name']+" - "+msg.projects_interested[i]['date']+" <span class='delinterest'>X</span></li>");
				}
			}
			
			
			$('#school_year option[value="'+msg.school_year+'"]').attr('selected',true);
			$('#school_year option').each(function(){
				var adjustSY = $(this).val();
				var text = "";
				if(adjustSY > 0) text = "+"+adjustSY+" year(s)";
				if(adjustSY == 0) text = "(Normal)";
				if(adjustSY < 0) text = adjustSY+" year(s)";
				$(this).html("");
				$(this).html(text+" - Year "+(parseInt(get_school_year(msg.dob)) + parseInt(adjustSY)));
			
			});
			
			$('#suggest_year').html(' Calculated Year '+get_school_year(msg.dob));
			
			$('#school').val(msg.school);
			$('.genderopt[value="'+msg.gender+'"]').attr('checked',true);
			$('.pconsent[value="'+msg.photo_consent+'"]').attr('checked',true);
			$('.fconsent[value="'+msg.facebook_consent+'"]').attr('checked',true);
			$('.cafopt[value="'+msg.caf+'"]').attr('checked',true);
			$('#guardian_signature_date').val(msg.guardian_signature_date);
			$('#health_form_received').val(msg.health_form_received);
			if(msg.health_form_received == "1") {
				$('#health_form_checkbox').attr('checked',true);
				$('#dhfr').show();
				$('#date_health_form_received').val(msg.date_health_form_received);
			}else{
				$('#health_form_checkbox').attr('checked',false);
				$('#dhfr').hide();
				$('#date_health_form_received').val('');
			}
			$('#medical').val(msg.medical);
			$('#allergies').val(msg.allergies);
			$('#issues').val(msg.issues);
			$('#notes').val(msg.notes);

			$('#ajaxload').remove();
			$('#edityp').modal();
			
		});
		getyp.fail(function(jqXHR, textStatus){
			console.log('failed to get record '+textStatus);
			$('#ajaxload').remove();
		});
		
		return false;
	}
	
	function deleteYp(){
		id = $('#ypid').val();
		tablename = 'participants';
		if(confirm("Are you sure you want to delete Young Person id "+id) == true){
			// do ajax call
			delyp = $.ajax({
				url: "crisapi.php",
				type: "POST",
				data: "q=deleterow&id="+id+"&tablename="+tablename+"&idfield=participant_id"
			});
			delyp.done(function( msg ) {
				$('tr#yp_'+id).remove();
				console.log("Attempted to delete Young Person id "+id+". "+msg);
				$('#edityp').modal( "hide" );
			});
			delyp.fail(function(jqXHR, textStatus){
				console.log("Failed to delete yp id "+id+" "+textStatus);
				alert("There was a problem deleting the young person from the database. Please check your connection and try again");
				
			});
		}
	}
	
	function saveYp(){
		var str = $('#editypform').serialize();
		str = str + "&q=changeyp";
		//console.log(str);
		changeyp = $.ajax({
			url: "crisapi.php",
			type: "POST",
			dataType: 'json',
			data: str
		});
		changeyp.done(function( msg ) {
			id = $('#ypid').val();
			console.log(msg);
			if(msg.consent) {location.reload(); }
			
			
			$('#ajaxload').remove();
			//console.log($('#yp_'+msg.ypid));
			// update display
			$('#yp_'+msg.ypid+' td.firstname').html(msg.firstname);
			$('#yp_'+msg.ypid+' td.lastname').html(msg.lastname);
			// Calculate Ages and School Year
			ypdob = new Date(msg.dob);
			$('#yp_'+msg.ypid+' td.dob').html(get_age(msg.dob)+ " yrs old"+ "<br />"+ypdob.getDate()+"/"+(ypdob.getMonth()+1)+"/"+ypdob.getFullYear() );
			$('#yp_'+msg.ypid+' td.school').html(msg.school);
			$('#yp_'+msg.ypid+' td.school_year').html("Year "+(parseInt(get_school_year(ypdob))+parseInt(msg.school_year)));
			$('#yp_'+msg.ypid+' td.phone').html(msg.yp_phone+"<br />"+msg.guardian_phone);
			$('#yp_'+msg.ypid+' td.gname').html(msg.guardian_name);
			$('#yp_'+msg.ypid+' td.address').html(msg.address+" "+msg.postcode);
			
			$('#updatespan').css('top',$('#yp_'+msg.ypid).offset().top);
			$('#updatespan').css('left',$('#yp_'+msg.ypid).offset().left+$('#yp_'+msg.ypid).width()).fadeIn(1000,function(){$(this).fadeOut(1000)});
			$('#yp_'+msg.ypid).animate({ borderColor: 'green'}, 'slow',function(){$(this).animate({ borderColor: '#ccc'}, 1000)});
			$('#yp_'+msg.ypid).prev('tr').animate({ borderColor: 'green'}, 'slow',function(){$(this).animate({ borderColor: '#ccc'}, 1000)});
			
			$('#edityp').modal('hide');
		});
		changeyp.fail(function(jqXHR, textStatus){
			console.log('failed to get record. '+textStatus);
			alert("There was a problem updating the record. Please check your connection and try again");
			$('#ajaxload').remove();
		});
		
		
	}
	
	
	function printtable(){
		$('#summarytable').appendTo('body');
		$('body').children().each(function(){
			if($(this).hasClass('hidden-print')){	
			}else{
				$(this).addClass('hidden-print').addClass('addedhp');
			}
		});
		$('#summarytable').removeClass('hidden-print');
		window.print();
		$('#summarytable').prependTo('#edityp .modal-body');
		$('body').children().each(function(){
			if($(this).hasClass('addedhp')){	
				$(this).removeClass('hidden-print').removeClass('addedhp');
			}
		});
	}