	// Contacts Page
	
	$(document).ready(function(){
	
		if($('#filtered li').length > 0){
			$('table#contacts tr').hide();
			$('table#contacts tr:first').show();
			$('table#contacts tr.odd').each(function(){$(this).removeClass('odd');});
			$('#filtered li').each(function(){
				$('.type'+$(this).attr('fv')).show();
				$('#filter_type option[value='+$(this).attr('fv')+']').attr('selected',true);
			});
		}
	});
	
	$('#filtered').delegate('button#clear_filter', 'click', function() {	
		
		$('table#contacts tr').show();
		$('#filtered').html('');
		$('#filter_type').val('');
		currurl = window.location.protocol + "//" + window.location.host + window.location.pathname;
		currquery = window.location.search;
		history.pushState(null,null,currurl+updateQueryStringParameter(currquery, 'filter',""));
	
	});
	
	
	$('#filter_button').click(function(){

		// Hide whole table
		$('table#contacts tr').hide();
		
		// Show header
		$('table#contacts tr:first').show();
		
		//remove odd shading
		$('table#contacts tr.odd').each(function(){$(this).removeClass('odd');});
		
		//Apply the filter
		$('#filtered').html("Filtered by:<ul></ul>");
		var filter_list = "";
		var types=document.getElementById("filter_type");
		for (var i = 0; i < types.options.length; i++) {
		 if(types.options[i].selected ==true){
			  $('.type'+types.options[i].value).show();
			  contact_type = $("#filter_type option[value="+types.options[i].value+"]").text();
			  $('#filtered ul').append("<li>"+contact_type+ "</li>");
			  
			  filter_list = filter_list+"::"+types.options[i].value;
			 

		  }
		}
		
		filter_list = filter_list.trim("::");
		
		col = new Array();
			$('table#contacts tr.stickyheader th').each(function(){
				col.push($(this).css('width'));
			});
		currquery = window.location.search;
		console.log(updateQueryStringParameter(currquery, 'filter', filter_list));
		currurl = window.location.protocol + "//" + window.location.host + window.location.pathname;
		history.pushState(null,null,currurl+updateQueryStringParameter(currquery, 'filter', filter_list));
		
		$('#filtered').append("</ul> &nbsp;<button class='btn btn-med btn-primary' id=clear_filter>Clear Filter</button>");
		
	});


	


	$('table#contacts').delegate('.clickable_contact', 'click', function() {
		id = $(this).attr('id').split('_');
		console.log(id);
		if(id[1]){
			if(id[1] == 'staff'){
				if(confirm("This contact is listed as a worker / volunteer and should be edited in the staff database. Would you like to go there now?")== true){
					location = "viewstaff.php";
				}
			}else{
				$(this).children('td').last().append("<img src='/img/ajax-loader-sm.gif' id=ajaxload style='margin-left:10px;' />");
				getContactDetails(id[1]);
			}
			

		}
		return false;
	});
	
	function getContactDetails(id){
		
		getcontact = $.ajax({
			url: "crisapi.php",
			type: "POST",
			dataType: "json",
			data: "q=getcontact&table=contacts&id="+id+"&idfield=contact_id"
		});
		getcontact.done(function( msg ) {
			console.log(msg);
			$('#summarytable').html(msg.summary);
			$('#contactid').val(msg.id);
			$('#contact_type').val(msg.category);
			//reset checkboxes
			$('#contact_type_area input').attr('checked',false);
			if(msg.category != ""){
				types = msg.category.split("::");
				for(i = 0; i < types.length; i++){
					$('#contact_type_area input[value='+types[i]+']').attr('checked',true);
				}
			}
			$('#organisation').val(msg.organisation);
			$('#website').val(msg.website);
			$('#title').val(msg.title);
			$('#firstname').val(msg.firstname);
			$('#lastname').val(msg.lastname);
			$('#job_title').val(msg.job_title);
			$('#email').val(msg.email);
			$('#telephone').val(msg.telephone);
			$('#address').val(msg.address);
			$('#address2').val(msg.address2);
			$('#address3').val(msg.address3);
			$('#postcode').val(msg.postcode);
			$('#notes').val(msg.notes);
			
			$( "#editcontact" ).modal();
			$('#ajaxload').remove();			
		});
		getcontact.fail(function(jqXHR, textStatus){
			console.log('failed to get record. '+textStatus);
			$('#ajaxload').remove();
		});
		
		return false;
	}
	
	function deleteContact(){
					id = $('#contactid').val();
					tablename = 'contacts';
					if(confirm("Are you sure you want to delete contact id "+id) == true){
						// do ajax call
						delcontact = $.ajax({
							url: "crisapi.php",
							type: "POST",
							data: "q=deleterow&id="+id+"&tablename="+tablename+"&idfield=contact_id"
						});
						delcontact.done(function( msg ) {
							$('tr#contact_'+id).remove();
							console.log("Deleted contact id "+id+". "+msg);
							$('#editcontact').modal( "hide" );
						});
						delcontact.fail(function(jqXHR, textStatus){
							console.log("Failed to delete contact id "+id+" "+textStatus);
							alert("There was a problem deleting the contact. Please check your connection and try again");
							
						});
					}	
	
	}
	
	
	function saveContact(){
					var str = $('#editcontactform').serialize();
					str = str + "&q=changecontact";
					changecontact = $.ajax({
						url: "crisapi.php",
						type: "POST",
						data: str
					});
					changecontact.done(function( msg ) {
						id = $('#contactid').val();
						console.log(id + " " + msg);
						$( '#editcontact' ).modal('hide');
						location.reload(); 
						
					});
					changecontact.fail(function(jqXHR, textStatus){
						console.log('failed to get record. '+textStatus);
						alert("There was a problem updating the record. Please check your connection and try again");
					});
	
	
	}