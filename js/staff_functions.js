	$(document).ready(function(){
		$('#addstaff .session_input').hide();
		$('#addstaff .session_input').first().show().addClass('current');
		$('#addstaff .session_input').each(function(){
			$(this).append("<div class=buttons><span style='display:inline-block;width:200px;text-align:center;'><button class='prev_section btn btn-primary btn-sm'>Previous Section</button></span><b>Section "+$(this).index()+" of "+$('.session_input').length+"</b><span style='display:inline-block;width:200px;text-align:center;'><button class='next_section btn btn-primary btn-sm'>Next Section</button></span></div>");
		if($('.session_input.current').index() == 1){
			$('.prev_section').hide();
		}else{
			$('.prev_section').show();
		}
		});
		//window.crisadm = check_admin();
		
	});
	
	$('#addstaff').delegate('.prev_section', 'click', function() {
			
		if($('.session_input.current').prev('.session_input').length > 0){
			$('.session_input.current').hide().removeClass('current').prev('.session_input').show().addClass('current');	
		}
		
		if($('.session_input.current').index() == 1){
			$('.prev_section').hide();
		}else{
			$('.prev_section').show();
		}
		if($('.session_input.current').index() == $('.session_input').length){
			$('.next_section').hide();
		}else{
			$('.next_section').show();
		}
		return false;
	});
	$('#addstaff').delegate('.next_section', 'click', function() {
		

		if($('.session_input.current').next('.session_input').length > 0){
			$('.session_input.current').hide().removeClass('current').next('.session_input').show().addClass('current');
		}	

		if($('.session_input.current').index() == $('.session_input').length){
			$('.next_section').hide();
		}else{
			$('.next_section').show();
		}
		if($('.session_input.current').index() == 1){
			$('.prev_section').hide();
		}else{
			$('.prev_section').show();
		}
		return false;
	});

	$('.filterbox').change(function(){
		do_filter();
	});
	

	$('#editstaff').on('shown.bs.modal',function (e){
		if($('#summarytable').is(":hidden"))	$('.edittoggle').toggle();
	
	});
	$('table#staff').delegate('tr.clickable_staff', 'click', function() {
		id = $(this).attr('id').split('_');
		if(id[1]){
			$(this).children('td').last().append("<img src='img/ajax-loader-sm.gif' id=ajaxload style='margin-left:10px;' />");
			getStaffDetails(id[1]);
		}
		return false;
	});
	
	
function getStaffDetails(id){
		
			// ajax call
			getstaff = $.ajax({
				url: "crisapi.php",
				type: "POST",
				dataType: "json",
				data: "q=getstaff&table=staff&id="+id+"&idfield=staff_id"
			});
			getstaff.done(function( msg ) {
				console.log(msg);
				console.log('Got record '+id);
				$('#summarytable').html(msg.summary);
				$('#staffid').val(msg.id);
				$('#firstname').val(msg.firstname);
				$('#lastname').val(msg.lastname);
				$('#status'+msg.status).attr('checked',true);
				if(msg.staff == 1) {$('#stafftypestaff').attr('checked',true);}else{$('#stafftypestaff').attr('checked',false);}
				if(msg.volunteer == 1) {$('#stafftypevol').attr('checked',true);}else{$('#stafftypevol').attr('checked',false);}
				$('#username').val(msg.username);
				if(msg.username != ''){$('#logindetails').show();$('#needloginyes').prop('checked',true);}else{$('#logindetails').hide();$('#needloginno').prop('checked',true);}
				$('#pw1').val('');
				$('#pw2').val('');
				$('#date_started').val(msg.date_started);
				$('#line_manager option[value = '+msg.line_manager+']').attr('selected',true);
				$('#job_title').val(msg.job_title);
				$('#email').val(msg.email);
				$('#telephone').val(msg.telephone);
				$('#mobile').val(msg.mobile);
				$('#address').val(msg.address);
				$('#postcode').val(msg.postcode);
				$('#health_notes').val(msg.health_notes);
				$('#notes').val(msg.notes);
				$('#app_form_date').val(msg.app_form_date);
				$('#app_form_seen_by').val(msg.app_form_seen_by);
				$('#ref1_date').val(msg.ref1_date);
				$('#ref1_seen_by').val(msg.ref1_seen_by);
				$('#ref2_date').val(msg.ref2_date);
				$('#ref2_seen_by').val(msg.ref2_seen_by);
				$('#selfdec_form_date').val(msg.selfdec_form_date);
				$('#selfdec_form_seen_by').val(msg.selfdec_form_seen_by);
				$('#dbs_number').val(msg.dbs_number);
				$('#dbs_expiry_date').val(msg.dbs_expiry_date);
				if(msg.driving_licence == 1) {$('#licence').attr('checked',true);}else{$('#licence').attr('checked',false);}
				if(msg.induction == 1) {$('#induction').attr('checked',true);}else{$('#induction').attr('checked',false);}
				
				//$("#editstaffform .session_input").first().show().addClass('current');
				$( "#editstaff" ).modal();	
				$('#ajaxload').remove();
				
					
			});
			getstaff.fail(function(jqXHR, textStatus){
				console.log('failed to get record.'+textStatus);
				$('#ajaxload').remove();
			});

}

function deleteStaff(){
	id=$('#staffid').val();
	tablename = 'staff';

	if(confirm("Are you sure you want to delete staff member "+$('tr#staff_'+id+' td:eq(2)').html()+" "+$('tr#staff_'+id+' td:eq(3)').html()+" (id "+id+")?") == true){
		// do ajax call
		delstaff = $.ajax({
			url: "crisapi.php",
			type: "POST",
			data: "q=deleterow&id="+id+"&tablename="+tablename+"&idfield=staff_id",
			dataType: "text"
		});
		delstaff.done(function( msg ) {	
			console.log(msg);
			
			if(msg.substr(0,3) == 'Not'){
				alert("You are not authorised to delete workers!");
			}else{
				$('tr#staff_'+id).remove();
				console.log("Deleted staff id "+id+". "+msg);
				alert("Deleted staff id "+id+". ");
				$( '#editstaff' ).modal( "hide" );
			}
		});
		delstaff.fail(function(jqXHR, textStatus){
			console.log("Failed to delete staff id "+id+" "+textStatus);
			alert("There was a problem deleting the staff member. Please check your connection and try again");
			
		});
	}
}

function saveStaff() {
						
						
	// need to validate password change
	$('div.alert').remove();
	
	if($('#pw1')){
		if($('#pw1').val() != $('#pw2').val()){
			$('#pw1').focus().after("<div class='alert alert-warning'>Your passwords do not match!</div>");
			return false;
		}
	}
	

		var str = $('#editstaffform').serialize();
		str = str + "&q=changestaff";
		changerecord = $.ajax({
			url: "crisapi.php",
			type: "POST",
			dataType:"json",
			data: str
		});
		changerecord.done(function( msg ) {
			id = $('#staffid').val();
			
			console.log(msg);
			if(msg.report == 'Success') {
				switch(msg.status) {
					case 0:
						$('#staff_'+msg.id+' td.status').html('');
						$('#staff_'+msg.id).removeClass('activeworker').removeClass('ei').addClass('inactive');
						break;
					case 1:
						$('#staff_'+msg.id+' td.status').html('<b>Y</b>');
						$('#staff_'+msg.id).removeClass('inactive').removeClass('ei').addClass('active');
						break;
					case 2:
						$('#staff_'+msg.id+' td.status').html('E');
						$('#staff_'+msg.id).removeClass('activeworker').removeClass('inactive').addClass('ei');
						break;
				}
				
				$('#staff_'+msg.id+' td.firstname').html(msg.firstname);
				$('#staff_'+msg.id+' td.lastname').html(msg.lastname);
				if(msg.staff){
					$('#staff_'+msg.id+' td.staff').html('&#10004;');
					$('#staff_'+msg.id).addClass('staff');
				}else{
					$('#staff_'+msg.id+' td.staff').html('');
					$('#staff_'+msg.id).removeClass('staff');
				}
				if(msg.volunteer){
					$('#staff_'+msg.id+' td.vol').html('&#10004;');
					$('#staff_'+msg.id).addClass('vol');
				}else{
					$('#staff_'+msg.id+' td.vol').html('');
					$('#staff_'+msg.id).removeClass('vol');
				}
				$('#staff_'+msg.id+' td.tel').html(msg.telephone+"<br />"+msg.mobile);
				$('#staff_'+msg.id+' td.email').html(msg.email);
				
				
				$('#updatespan').css('top',$('#staff_'+msg.id).offset().top);$('#updatespan').css('left',$('#staff_'+msg.id).offset().left+$('#staff_'+msg.id).width()).fadeIn(1000,function(){$(this).fadeOut(1000)});
				$('#staff_'+msg.id).animate({ borderColor: 'green'}, 'slow',function(){$(this).animate({ borderColor: '#ccc'}, 1000)});
				$( '#editstaff' ).modal( "hide" );
			}else{
				alert("There was a problem updating this record. Your system administrator has been informed.");
				mail("tim.sparks@cre8macclesfield.org","Error in CRIS","Error when updating a staff record. ".msg);
			}
			
		});
		changerecord.fail(function(jqXHR, textStatus){
			console.log('failed to get record. '+textStatus);
			alert("There was a problem updating the record. Please check your connection and try again");
		});
	
}


function do_filter() {

		active = $('#filter_active').prop('checked');
		inactive = $('#filter_inactive').prop('checked');
		ei = $('#filter_ei').prop('checked');
		staff = $('#filter_staff').prop('checked');
		vol = $('#filter_vol').prop('checked');
	
		$('table#staff tr').hide();
		$('table#staff tr:first').show();
		$('table#staff tr.odd').each(function(){$(this).removeClass('odd');});
		
		
		filter_list = '';
		if(active){
			if(staff){
				$('tr.activeworker.staff').show();				
			}
			if(vol){
				$('tr.activeworker.vol').show();				
			}			
		}
		
		if(inactive){
			if(staff){
				$('tr.inactive.staff').show();			
			}
			if(vol){
				$('tr.inactive.vol').show();			
			}
		}
		
		if(ei){
			if(staff){
				$('tr.ei.staff').show();			
			}
			if(vol){
				$('tr.ei.vol').show();		
			}		
		}
		
		if(!staff) filter_list = filter_list + "::staff";
		if(!vol)filter_list = filter_list + "::vol";
		if(!active)filter_list = filter_list + "::active";
		if(!inactive) filter_list = filter_list + "::inactive";
		if(!ei) filter_list = filter_list + "::ei";
		
		filter_list = filter_list.trim("::");
		
		currquery = window.location.search;
		console.log(updateQueryStringParameter(currquery, 'filter', filter_list));
		currurl = window.location.protocol + "//" + window.location.host + window.location.pathname;
		history.pushState(null,null,currurl+updateQueryStringParameter(currquery, 'filter', filter_list));
		console.log(filter_list);
}

	function printtable(){
		$('#summarytable').appendTo('body');
		$('body').children().each(function(){
			if($(this).hasClass('hidden-print')){	
			}else{
				$(this).addClass('hidden-print').addClass('addedhp');
			}
		});
		$('#summarytable').removeClass('hidden-print');
		window.print();
		$('#summarytable').prependTo('#editstaff .modal-body');
		$('body').children().each(function(){
			if($(this).hasClass('addedhp')){	
				$(this).removeClass('hidden-print').removeClass('addedhp');
			}
		});
	}
		
