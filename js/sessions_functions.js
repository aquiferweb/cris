	
// ------ Session Recording Listeners ---- //


	$('#submitNewSession').click(function(e){
		e.preventDefault();

		$('#newsession').submit();
		
		//alert('AERG');
		
	});

	$('#submiteditsession').click(function(){
		editvars = $('#editsessionform').serialize();
		console.log(editvars);
		$(this).html('Saving...');
		editsession = $.ajax({
			url: "crisapi.php",
			type: "POST",
			dataType: "text",
			data: editvars+"&q=editsession"		
		
		});
		editsession.done(function( msg ) {
			console.log(msg);
			$('.edittoggle').toggle();
			getSession($('#sessionid').val());
			//console.log('here');
			$('#submiteditsession').html('Save Changes');
		});
		editsession.fail(function(jqXHR, textStatus){
			console.log('failed to edit session. '+textStatus);
			$('#submiteditsession').html('Save Changes');
		});
	
	});

	$('#project_id').change(function(){
		
		$('.session_input.start_hidden').hide();
		$('.select_session').show();
		var projectid = $(this).val();
		getstypes = $.ajax({
			url: "crisapi.php",
			type: "POST",
			dataType: "json",
			data: "q=getsessiontypes&projectid="+projectid
		});
		getstypes.done(function( msg ) {
			console.log(msg);
			//clear old values
			$('#session_type').html('');
			$('#session_type').append("<option value='' disabled selected> - Choose Session Type - </option>");
			//add new values
			for (var i = 0; i < msg.length; i++){
				$('#session_type').append("<option value="+msg[i].id+" groupact="+msg[i].group_activity+">"+msg[i].session_name+"</option>");			
			}
			if(msg.length == 1){
				// only one session type - go straight to full form.
				$('.session_input.start_hidden').show();
				$('#session_type option[value="'+msg[0].id+'"]').prop('selected',true);
				if($('#session_type option[value="'+msg[0].id+'"]').attr('groupact') == 1){
				// group session
					$(".group_guidance").show();
					$(".indiv_guidance").hide();
				}else{
				// individual session
					$(".group_guidance").hide();
					$(".indiv_guidance").show();
				}
				
				// get registered yp list
				getRegisteredYp(msg[0].id);
				
			}
			
			if(projectid == 10) {
				$('.splitnotes').show().prop('required',true);
				$('.jointnotes').hide().prop('required',false);
				$('#splitnotes').val(1);
			}else{
				$('#splitnotes').val(0);
				$('.splitnotes').hide().prop('required',false);
				$('.jointnotes').show().prop('required',true);
			}
		});
		getstypes.fail(function(jqXHR, textStatus){
			console.log('failed to get record. '+textStatus);
		});
		
	});
	
	
	$('#session_type').change(function(){
		var st = $(this).val();
		$('.session_input.start_hidden').show();
		if($('#session_type option[value="'+st+'"]').attr('groupact') == 1){
			// group session
			$(".group_guidance").show();
			$(".indiv_guidance").hide();
		}else{
			// individual session
			$(".group_guidance").hide();
			$(".indiv_guidance").show();
		}
		
		// get registered yp list
		getRegisteredYp(st);
	});

	
	$('#session_date').change(function(){
		// if session end date is blank or before the new session start date
		if($('#session_end_date').val() == '' || parseDate($('#session_end_date').val()) < parseDate($('#session_date').val())){
			// set session end date to same as start
			$('#session_end_date').val($('#session_date').val());
		}
		//$('#stime').focus();
	});
	
	$('#stime').blur(function(){
		// if end time is not set or is less than start time
		if($('#etime').val() == ''){
			// take start time and add two hours
			stime = $(this).val().split(':');
			etimeh = parseInt(stime[0]) + 2;
			etimem = parseInt(stime[1]);
			if(etimem < 10) etimem = "0"+etimem;
			etime = "" + etimeh +":"+etimem;
			//console.log(stime);
			//console.log(etime);
			$('#etime').val(etime);
		}else if($('#etime').val() < $(this).val()){
			$('#etime').val( $(this).val());
		}
	});
	

	
// Validate Session Fields **********************************************************
	$('#newsession').submit(function(e){
		
		var failed = false;
		// remove old warnings
		$('div.alert').remove();
		
		// manually check required fields
		$('#newsession textarea').each(function(){
			if($(this).prop('required') && $(this).val() == ''){
				$(this).parents('.session_input').append("<div class='alert alert-warning'>This field is required.</div>");
				$(this).focus();
				failed = true;
			}
		});
		
		// checks required fields in reverse order
		$($('#newsession input').get().reverse()).each(function(){
			if($(this).prop('required') && $(this).val() == ''){
				$(this).parents('.session_input').append("<div class='alert alert-warning'>This field is required.</div>");
				$(this).focus();
				failed = true;
			}
		});
		
		// check if young people are selected
		if($('#parti_list').val() == ""){		
			$('#addp').focus().after("<div class='alert alert-warning'>Please select some young people!</div>");
			failed = true;	
		}
		
			
		// check if staff are selected
		if($('#staff_list').val() == ""){		
			$('#additionalstaff').focus().after("<div class='alert alert-warning'>Please select some staff!</div>");
			$('#add_staff').focus();
			failed = true;		
		}else{
			// check if a leader is selected
			if($('#staff_leader').val() == "" || isNaN($('#staff_leader').val())){
				$('#additionalstaff').after("<div class='alert alert-warning'>Please select a leader!</div>");
				$('#add_staff').focus();
				failed = true;	
			}
		}
		
			
		// check dates
		var sdate = parseDate($('#session_date').val());
		var edate = parseDate($('#session_end_date').val());
		var today = new Date();	
		
		if(isNaN(sdate)){
			$('#session_date').parents('.session_input').append("<div class='alert alert-warning'>Please enter a valid date.</div>");
			$('#session_date').focus();
			failed = true;	
		}else if(isNaN(edate)){
			$('#session_end_date').parents('.session_input').append("<div class='alert alert-warning'>Please enter a valid date.</div>");
			$('#session_end_date').focus();
			failed = true;
		}else if(sdate > today || edate > today){
			$('#sessionDatePair').append("<div class='alert alert-warning'>Both Session dates must be in the past.</div>");
			$('#session_date').focus();
			failed = true;
		}
		
		var stime = $('#stime').val().split(':');
		var etime = $('#etime').val().split(':');
		sdate.setHours(stime[0]);		edate.setHours(etime[0]);
		sdate.setMinutes(stime[1]);		edate.setMinutes(etime[1]);
		if(edate < sdate){
			$('#sessionDatePair').append("<div class='alert alert-warning'>The end date/time is before the start date/time!</div>");
			$('#session_date').focus();
			failed = true;
		}
		
		
			
			
			
		// do summary
		
		if(failed){
			$('div.alert').css('border','2px solid transparent').animate({'border-color':'red','border-width':'2px'},500);
			return false;
		}else{
			$('#submitNewSession').attr('disabled',true).prop('disabled',true);
			$('#submitNewSession').html('Submitting...');
			return true;
		}
		
	});
	
	
	
	$('#add_upload').click(function(){
		var num_uploads = $('.upload_input').length;
		var new_upload = num_uploads + 1;
		$('#upload_'+num_uploads).after("<br /><input type=file name=upload_"+new_upload+" id=upload_"+new_upload+" class=upload_input />");
	
	});

	

	
	$('table').delegate('button.remove', 'click', function(e) {	
		e.preventDefault();
		rowid = $(this).parents('tr').attr('id').split('_');
		id = rowid[1];
		name = $(this).parent().prev().html();
		console.log("remove "+id+' - '+name);
		listid = $(this).attr('sid');

		// remove from hidden input
		list = $('#'+listid).val().split("::");
		liststr = "";
		for (var i = 0; i < list.length; i++) {
			if(list[i] == id || list[i] == ""){
				// don't add to string
			}else{
				// do add to string
				liststr = liststr + "::"+list[i];
			}
		}
		liststr = liststr.trim("::");
		$('#'+listid).val(liststr);
		
		
		// remove from table
		$(this).parent().parent('tr').remove();
		
		// if staff table
		if(listid == "staff_list"){		
			// change session leader if necessary
			if($('#staff_leader').val() == id){
				if($('#additionalstaff td.id').length > 1){
					newleaderid = $('#additionalstaff td.id:first').parent().attr('id').split('_');
					console.log("new leader "+newleaderid[1]);
					$('#staff_leader').val(newleaderid[1]);
					$('#additionalstaff tr#staff_'+newleaderid[1]+' td.ml').html('<b>Session Leader</b>');
					$('#additionalstaff tr#staff_'+newleaderid[1]+' ').addClass('leader');
				}else{
					$('#staff_leader').val("");
				}
			}
		}
		
		$('#num_yp').html($('#participants td.id').length);
		//if($(this).parent().parent.parent('tr.item').length == 0) $(this).parent().pa
		
		return false;
	});
	
	$('table').delegate('button.makeleader', 'click', function() {	
		id = $(this).parent().siblings('.id').html();
		make_leader(id);	
		return false;
	});
	

	
	$('#uselast').click(function(){
		sessiontypeid = $('#session_type').val();
		if(sessiontypeid == '' || !sessiontypeid){
			alert('Please select a session type first!');
			$('#session_type').focus();
		}else{
			get_latest_p(sessiontypeid);
		}
		return false;
	});
	
	$('#uselaststaff').click(function(){
		sessiontypeid = $('#session_type').val();
		if(sessiontypeid == '' || !sessiontypeid){
			alert('Please select a session type first!');
			$('#session_type').focus();
		}else{
			get_latest_staff(sessiontypeid);
		}
		return false;
	});
	
	$('#useconsent').click(function(){
		sessiontypeid = $('#session_type').val();
		if(sessiontypeid == '' || !sessiontypeid){
			alert('Please select a session type first!');
			$('#session_type').focus();
		}else{
			get_consented(sessiontypeid);
		}
		return false;
	});
	
	
	$('table#sessionresults').delegate('.clickable_session', 'click', function() {
		id = $(this).attr('id').split('_');
		console.log(id);
		if(id[1]){

			$(this).children('td').last().append("<img src='img/ajax-loader-sm.gif' id=ajaxload style='margin-left:10px;' />");
			getSession(id[1]);	

		}
		return false;
	});
	
	$('#editsession').on('shown.bs.modal',function (e){
		if($('#summarytable').is(":hidden"))	$('.edittoggle').toggle();
	
	});
	
	$('#splitnotes').click(function(){
		console.log($(this).is(":checked"));
		if($(this).is(":checked")){
			$('.splitnotes').show().prop('required',true);
			$('.jointnotes').hide().prop('required',false);
		}else{
			$('.splitnotes').hide().prop('required',false);
			$('.jointnotes').show().prop('required',true);	
		}
	
	});

	
	
	
	
	
	
	function getSession(id){
	
		getsess = $.ajax({
			url: "crisapi.php",
			type: "POST",
			dataType: "json",
			data: "q=getsession&id="+id
		});
		getsess.done(function( msg ) {

			console.log(msg);
			
			$('#summarytable').html(msg.summarytable);
			
			var allowedit = $('#isadmin').val();
			
			if(allowedit){
			
				clear_table('participants','parti_list');
				clear_table('additionalstaff','staff_list');
				
				$('.start_hidden').show();
				$('#recordedbyid').val(msg.recbyid);
				$('#editedbyid').val(msg.editbyid);
				$('#sessionid').val(id);			
				$("#project_id option[value="+msg.projectid+"]").prop('selected',true);
				$('#session_type').html('');
				$('#session_type').append("<option value='' disabled> - Choose Session Type - </option>");
				//add new values
				for (var i = 0; i < msg['sister_sessiontypes'].length; i++){
					$('#session_type').append("<option value="+msg['sister_sessiontypes'][i].id+" groupact="+msg['sister_sessiontypes'][i].group_activity+">"+msg['sister_sessiontypes'][i].session_name+"</option>");			
				}
				
				
				$("#session_type option[value="+msg.session_type+"]").prop('selected',true);
				
				
				if($("#session_type option[value="+msg.session_type+"]").attr('groupact') == '1') {
					$('.group_guidance').show();
					$('.indiv_guidance').hide();
				}else{
					$('.group_guidance').hide();
					$('.indiv_guidance').show();
				}
				
				
				// leaders
				 console.log(msg['workers']);
				for (var i = 0; i < msg['workers'].length; i++){
						add_staff(msg['workers'][i].staffid, msg['workers'][i].stafffn+" "+msg['workers'][i].staffln);		
				}
				
				make_leader(msg.sleaderid);
				
				$('#registered_list').val('');
				rlist = '';
				for(i = 0; i < msg.registered.length; i++){
					console.log("get consented list "+msg.registered[i].id+ " "+msg.registered[i].name);
					rlist = rlist+"::"+msg.registered[i].id;
				}
				rlist = rlist.trim('::');
				$('#registered_list').val(rlist);
				
				// participants
				console.log(msg['yps']);
				for (var i = 0; i < msg['yps'].length; i++){
						add_p(msg['yps'][i].ypid, msg['yps'][i].ypfn+" "+msg['yps'][i].ypln);		
				}
				
				
				
				$('#session_date').val(msg.session_date);
				$('#session_end_date').val(msg.session_end_date);
				$('#stime').val(msg.session_start_time);
				$('#etime').val(msg.session_end_time);
				$('#location').val(msg.location);
				$('#notes').val(msg.notes);
				
				// files
				console.log(msg['uploaded_files']);
				//console.log(msg['uploaded_files'].length);
				if(msg['uploaded_files']){
					for (var i = 0; i < msg['uploaded_files'].length; i++){
						$('#already_uploaded div#files table').append("<tr id='file_"+msg['uploaded_files'][i].id+"'><td>"+msg['uploaded_files'][i].filename+"</td><td>"+msg['uploaded_files'][i].date+"</td><td>"+msg['uploaded_files'][i].notes+"</td><td><img src='uploads/"+msg['uploaded_files'][i].filename+"' style='width:50px' /></td><td><button type=button id=removefile_"+msg['uploaded_files'][i].id+">Remove</button></td></tr>");		
					}
				}
				
				
				if(msg.action == '1') {
					$('#actionyes').prop('checked',true).attr('checked',true);
					$('#actiontext').show();
				}else{
					$('#actionno').prop('checked',true).attr('checked',true);
					$('#actiontext').hide();
				}
				$('#actions').val(msg.action_text);
				if(msg.sg == '1') {
					$('#sgyes').prop('checked',true).attr('checked',true);
					$('#sgtext').show();
				}else{
					$('#sgno').prop('checked',true).attr('checked',true);
					$('#sgtext').hide();
				}
				$('#sgdetails').val(msg.sgdetails);
				
				
				

				$('#addp').autocomplete({
					source: "crisapi.php?q=getypautocomplete",
					select: function( event, ui ) {
						event.preventDefault();
						id = ui.item.value;
						$('#addp').val('');
						if(!isNaN(id)){
							add_p(id,ui.item.label);			
						}
					}
				});
				
				$('#add_staff').autocomplete({
					source:"crisapi.php?q=getstaffautocomplete",
					select: function( event, ui ) {
						event.preventDefault();
						id = ui.item.value;
						$('#add_staff').val('');
						if(!isNaN(id)){
							console.log(isNaN(id));
							add_staff(id,ui.item.label);			
						}
					}
				});
				
				
				
				
				
			}
			
			
			
				
			$( "#editsession" ).modal();
			$('#ajaxload').remove();		
		});
		getsess.fail(function(jqXHR, textStatus){
			console.log('failed to get record '+textStatus);
			$('#ajaxload').remove();
		});
		
		return false;
	
	
	}
	
	
	
	function add_p(pid,pname){
		if(!isNaN(pid)){
		plist = $('#parti_list').val().split('::');
		rlist = $('#registered_list').val().split('::');
		console.log(pid);
		console.log(plist);
		console.log(rlist);
		if($.inArray(pid,plist) >= 0){
			alert("You have already selected this participant!");
			$("#addp").val('');
			return false;
		}

		if($.inArray(pid,rlist) < 0){
			alert(pname +"\n\nThis participant is not registered for the selected project!\n\nNew participants for a project MUST fill in a registration form.\nPlease get a relevant registration form filled in for "+ $('#project_id option:selected').html() + " - " +$('#session_type option:selected').html() + ", and then add this registration information to the Young Persons Database.\n\nYou will not be able to complete this session record until this is done.");
			$("#addp").val('');
			return false;
		}		
		if($('#parti_list').val() == "") {pidstr = pid;}else{pidstr="::"+pid;}
		$('#participants tr').last().after("<tr id='staff_"+pid+"' class=item><td class='id'>"+pid+"</td><td>"+pname+"</td><td><button class='remove btn btn-sm btn-primary' sid='parti_list'>Remove</button></td></tr>");
		$('#parti_list').val($('#parti_list').val()+pidstr);
		$('table#participants tr.noitem').hide();
		$('#num_yp').html($('#participants td.id').length);
		//console.log('added '+pid);
		return true;
		}
	}
	
	function add_staff(staffid,staffname){
		if(!isNaN(staffid)){
		// get already selected staff
		stafflist = $('#staff_list').val().split("::");
		
		// check if staff already selected
		for (var i = 0; i < stafflist.length; i++) {
			if(stafflist[i] == staffid){
				alert("You have already selected this member of staff!");
				$("#add_staff").val('');
				return false;
			}
		}

		//add staff to selected list
		if($('#staff_list').val() == "") {staffidstr = staffid;}else{staffidstr="::"+staffid;}
		$('#additionalstaff tr').last().after('<tr id=staff_'+staffid+' class=item><td class=id>'+staffid+'</td><td>'+staffname+'</td><td><button class="remove btn btn-sm btn-primary" sid="staff_list">Remove</button></td><td class=ml><button class="makeleader btn btn-sm btn-primary">Make Session Leader</button></td></tr>');
		$('#staff_list').val($('#staff_list').val()+staffidstr);
		
		//if no leader, then set as session leader
		if($('#staff_leader').val() == ""){
			$('#staff_leader').val(staffid);
			$('#additionalstaff tr#staff_'+staffid+' td.ml').html('<b>Session Leader</b>');
			$('#additionalstaff tr#staff_'+staffid+' ').addClass('leader');
		} 
		$('table#additionalstaff tr.noitem').hide();
	
		}
	}
	
	function get_latest_p(sessiontypeid){
		clear_table('participants','parti_list');
		$('#participants .loadingrow').show();
		getlist = $.ajax({
			url: "crisapi.php",
			type: "POST",
			dataType: "json",
			data: "q=getlatest&sessiontypeid="+sessiontypeid
		});
		getlist.done(function( msg ) {
			$('.loadingrow').hide();
			console.log(msg);
			if(msg != 'no sessions found'){
				console.log("get latest p "+msg+ " - "+msg.length);
				for(i = 0; i < msg.length; i++){
					console.log("get latest p "+msg[i].id+ " "+msg[i].name);
					add_p(msg[i].id,msg[i].name);
				}
			}else{
				alert("No sessions found!");
			}					
		});
		getlist.fail(function(jqXHR, textStatus){
			console.log('failed to get record. '+textStatus);
		});
	
	}
	
	function get_consented(sessiontypeid){
		clear_table('participants','parti_list');
		$('#participants .loadingrow').show();
		getlist = $.ajax({
			url: "crisapi.php",
			type: "POST",
			dataType: "json",
			data: "q=getconsented&sessiontypeid="+sessiontypeid
		});
		getlist.done(function( msg ) {
			$('.loadingrow').hide();
			for(i = 0; i < msg.length; i++){
				console.log("get consented list"+msg[i].id+ " "+msg[i].name);
				add_p(msg[i].id,msg[i].name);
			}
						
		});
		getlist.fail(function(jqXHR, textStatus){
			console.log('failed to get record');
		});
	
	}
	
	function getRegisteredYp(sessiontypeid){
		// set up registered young people list
		getregisteredlist = $.ajax({
			url: "crisapi.php",
			type: "POST",
			dataType: "json",
			data: "q=getconsented&sessiontypeid="+sessiontypeid
		});
		getregisteredlist.done(function( msg ) {
			$('#registered_list').val('');
			rlist = '';
			for(i = 0; i < msg.length; i++){
				console.log("get consented list "+msg[i].id+ " "+msg[i].name);
				rlist = rlist+"::"+msg[i].id;
			}
			rlist = rlist.trim('::');
			$('#registered_list').val(rlist);
						
		});
		getregisteredlist.fail(function(jqXHR, textStatus){
			console.log('failed to get record');
		});
	
	}
	
	
	function get_latest_staff(sessiontypeid){
		clear_table('additionalstaff','staff_list');
		$('table#additionalstaff .loadingrow').show();
		getlist = $.ajax({
			url: "crisapi.php",
			type: "POST",
			dataType: "json",
			data: "q=getlateststaff&sessiontypeid="+sessiontypeid
		});
		getlist.done(function( msg ) {
			$('.loadingrow').hide();
			console.log(msg);
			if(msg != 'no sessions found'){
				for(i = 0; i < msg.staff.length; i++){
					console.log("get latest list "+msg.staff[i].id+ " "+msg.staff[i].name);
					add_staff(msg.staff[i].id,msg.staff[i].name);
				}
				make_leader(msg.leader);
			}else{
				alert("No sessions found!");
			}
						
		});
		getlist.fail(function(jqXHR, textStatus){
			console.log('failed to get record. '+textStatus);
		});
	
	}
	
	function make_leader(id){
		// remove old leader
		oldleader = $('#staff_leader').val();
		$('#additionalstaff tr#staff_'+oldleader+' td.ml').html('<button class="makeleader btn btn-sm btn-primary">Make Session Leader</button>');
		$('#additionalstaff tr#staff_'+oldleader+' ').removeClass('leader');
		//set new leader
		$('#staff_leader').val(id);
		$('#additionalstaff tr#staff_'+id+' td.ml').html('<b>Session Leader</b>');
		$('#additionalstaff tr#staff_'+id).addClass('leader');
	}
	
	
	function clear_table(table,list){
		
		$('table#'+table+' tr.item').remove();
		$('#'+list).val('');
		$('table#'+table+' tr.noitem').show();
		if(table == 'participants') $('#num_yp').html('0');
		
	
	}
	
	function printtable(){
		$('#summarytable').appendTo('body');
		$('body').children().each(function(){
			if($(this).hasClass('hidden-print')){	
			}else{
				$(this).addClass('hidden-print').addClass('addedhp');
			}
		});
		$('#summarytable').removeClass('hidden-print');
		window.print();
		$('#summarytable').prependTo('#editsession .modal-body');
		$('body').children().each(function(){
			if($(this).hasClass('addedhp')){	
				$(this).removeClass('hidden-print').removeClass('addedhp');
			}
		});
	}