$(document).ready(function(){

	if (!Modernizr.inputtypes['date']) {
		$('input[type=date]').datepicker({	dateFormat: 'yy-mm-dd'	});
		$('input[type=date]').datepicker({	dateFormat: 'yy-mm-dd'	});
		$(".date").datepicker({ dateFormat: "yy-mm-dd" });
		$(".dateY").datepicker({ dateFormat: "yy-mm-dd",changeMonth: true,changeYear: true});
		$(".dateDOB").datepicker({ dateFormat: "yy-mm-dd",changeMonth: true,changeYear: true,defaultDate:-5475});
	}
	
	
	path = window.location.pathname;
	path = path.split('/');
	len = path.length;
	$("ul.nav li a[href='"+path[len-1]+"']").parent('li').addClass('active');
	//console.log(path[len-1]+ " "+len);
	

});

function check_admin(){
				checkadmin = $.ajax({
					url: "crisapi.php",
					type: "POST",
					data: "q=checkadmin",
					dataType: "json"
				});
				checkadmin.done(function( msg ) {				
					console.log('Admin '+msg);
					return msg;
				});
				checkadmin.fail(function(jqXHR, textStatus){
					console.log("Failed to check for admin status. "+textStatus);	
					return false;
				});

}

function updateQueryStringParameter(uri, key, value) {
  var re = new RegExp("([?&])" + key + "=.*?(&|$)", "i");
  var separator = uri.indexOf('?') !== -1 ? "&" : "?";
  if (uri.match(re)) {
    return uri.replace(re, '$1' + key + "=" + value + '$2');
  }
  else {
    return uri + separator + key + "=" + value;
  }
}


String.prototype.trimLeft = function(charlist) {
  if (charlist === undefined)
    charlist = "\s";
 
  return this.replace(new RegExp("^[" + charlist + "]+"), "");
};


String.prototype.trimRight = function(charlist) {
  if (charlist === undefined)
    charlist = "\s";
 
  return this.replace(new RegExp("[" + charlist + "]+$"), "");
};

String.prototype.trim = function(charlist) {
  return this.trimLeft(charlist).trimRight(charlist);
};


		
function sort_by_query(key,value,refresh,event){
	event.preventDefault();
	currquery = window.location.search;
	currurl = window.location.protocol + "//" + window.location.host + window.location.pathname;
	history.pushState(null,null,currurl+updateQueryStringParameter(currquery, key, value));
	if(refresh) location.reload();
}

function get_school_year(dob){
	today = new Date();
	if(today.getMonth() >= 8 ) {sep_year = today.getFullYear(); } else { sep_year = today.getFullYear() - 1;}
	sep_date = new Date(sep_year, 8,1);
	dob_date = new Date(dob);
	suggest_year = Math.floor((sep_date - dob_date) / (1000*60*60*24*365.25)) - 4;
	return suggest_year;
}

function get_age(dob){
	today = new Date();
	dob_date = new Date(dob);
	age = Math.floor((today - dob_date) / (1000*60*60*24*365.25));
	return age;
}

// parse a date in yyyy-mm-dd format
function parseDate(input) {
  var parts = input.split('-');
  // new Date(year, month [, day [, hours[, minutes[, seconds[, ms]]]]])
  return new Date(parts[0], parts[1]-1, parts[2]); // Note: months are 0-based
}