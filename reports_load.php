<?php

require('header.php');
require('navmenutop.php');
?>

<div id=container>

<?php

$sortby = $_GET['sortby'] ? $_GET['sortby'] : "date";
$q = "SELECT * FROM savedreports LEFT JOIN staff ON savedreports.saved_by = staff.id ORDER BY $sortby DESC";
if($r = mysql_query($q)){
	
}else{

}
?>

<div id=toolbar class='hidden-print'>
	<div class=first>
		<span style="font-weight: bold;font-size: 200%;vertical-align: middle;">Reports Generator</span>
	</div>
	<div>
		<a href="<?php echo ROOT_PATH;?>/reports_view.php"><button class='btn btn-med btn-primary link-btn'>New Report</button></a>
	</div>
	<div>
		<a href="<?php echo ROOT_PATH;?>/reports_load.php"><button class='btn btn-med btn-primary link-btn'>Run a Saved Report</button></a>
	</div>
	<?php if(isset($_POST['submitreportform'])){?>
	<div>
		<form method=post action='reports_view.php' id=savereportform>
		<?php
			
			echo "<input type=hidden name=querystr value='".serialize($_POST)."' />";
			echo "<input type=hidden name=date value='".date("Y-m-d H:i:s",time())."' />";
			echo "<input type=hidden name=saved_by value='".$_SESSION['CrisUserID']."' />";
			echo "<input type=hidden name=summary value='".mres($summary)."' />";
		?>
		<button class='btn btn-med btn-primary link-btn' type=submit name=savereport>Save this Report</button></form>
	</div>
	<?php
	}
	?>
	<div>
		<a href="javascript:window.print()"><button class='btn btn-med btn-primary link-btn'>Print</button></a>
	</div>
	
	
</div>

<div id=container style='max-width:90%'>


	<h1>Run a Saved Report</h1>
	<p>Click on a saved report to re-run it on the current database.<br />Click on a table heading to sort the table by that heading.</p>

	
	<span id=sortby style='display:none;'><?php echo $sortby;?></span>
	<table class=table>
	<thead>
	<tr>
	<th><a href='?sortby=id' id='id'>ID</a></th>
	<th><a href='?sortby=saved_by' id='saved_by'>Saved by</a></th>
	<th><a href='?sortby=title' id='title'>Title</a></th>
	<th><a href='?sortby=querytable' id='querytable'>Table</a></th>
	<th><a href='?sortby=date' id='date'>Date</a></th>
	<th><a href='?sortby=summary' id='summary'>Summary</a></th>
	<th>Run Report</th>
	</tr>
	</thead>
	<tbody>
	<?php
		while($report = mysql_fetch_assoc($r)){
			echo "<tr><td>".$report['id']."</td><td>".$report['firstname']." ".$report['lastname']."</td><td>".$report['title']."</td><td>".$report['querytable']."</td><td>".$report['date']."</td><td>".substr($report['summary'],0,100)."</td><td>
			<form action='reports_view.php' method=post>
			<input type=hidden name=querystr value='".$report['querystr']."' />
			<input type=hidden name=querytitle value='".$report['title']."' />
			<input type=submit name=loadreport value='Run Report' />
			</form></tr>";
		}
	?>
	</tbody>
	</table>
	
</div>
<script src='<?php echo ROOT_PATH;?>/js/reports_functions.js'></script>
<!--  <script src="//code.jquery.com/jquery-1.10.2.js"></script>
  <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script>

</script>-->
<?php
require('footer.php');
?>