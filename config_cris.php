<? 
require_once('crisconnect.php');

function db_backup(){

	// get time of last backup
	if($r = mysql_query("SELECT * FROM variables WHERE k = 'last_backed_up' ")){
		if($vars = mysql_fetch_array($r)){
			$last_backup = $vars['v'];
			// 86400 = 24 hours
			if((time() - strtotime($last_backup)) > 86400){
				// do backup
				
				//get all of the tables

				$tables = array();
				$result = mysql_query('SHOW TABLES');
				while($row = mysql_fetch_row($result))
				{
					$tables[] = $row[0];
				}

				
				//cycle through
				foreach($tables as $table)
				{
					$result = mysql_query('SELECT * FROM '.$table);
					$num_fields = mysql_num_fields($result);
					
					$return.= 'DROP TABLE '.$table.';';
					$row2 = mysql_fetch_row(mysql_query('SHOW CREATE TABLE '.$table));
					$return.= "\n\n".$row2[1].";\n\n";
					
					for ($i = 0; $i < $num_fields; $i++) 
					{
						while($row = mysql_fetch_row($result))
						{
							$return.= 'INSERT INTO '.$table.' VALUES(';
							for($j=0; $j<$num_fields; $j++) 
							{
								$row[$j] = addslashes($row[$j]);
								$row[$j] = ereg_replace("\n","\\n",$row[$j]);
								if (isset($row[$j])) { $return.= '"'.$row[$j].'"' ; } else { $return.= '""'; }
								if ($j<($num_fields-1)) { $return.= ','; }
							}
							$return.= ");\n";
						}
					}
					$return.="\n\n\n";
				}
				
				//save file
				$fn = 'db-backup-'.date('Y-m-d',time()).'-'.time().'-'.(md5(implode(',',$tables))).'.sql';
				$handle = fopen("/mnt/array1/webserver/dbbackups/".$fn,'w+');
				fwrite($handle,$return);
				fclose($handle);
				
				$r = mysql_query("UPDATE variables SET v = '".date('Y-m-d',time())."' WHERE k = 'last_backed_up' ");
				
				if(date('a',time()) == 'am' ) {
					return "Good morning! You're the first one to log in today - the database has been backed up.";
				}else{
					return "Good afternoon! You're the first one to log in today - the database has been backed up.";
				}
				//return "Good morning! You're the first one to log in today - the database has been backed up.";
			}else{
				// don't run backup
				//return "Database already backed up today";
			}
		}else{
			// couldn't get time of last backup
			
			return "Couldn't get time of last backup ".mysql_error();
		}
	}else{
		echo "problem getting data ".mysql_error();
	}
		

}

function mres($q) {
    if(is_array($q)) 
        foreach($q as $k => $v) 
            $q[$k] = mres($v); //recursive
    elseif(is_string($q))
        $q = mysql_real_escape_string($q);
    return $q;
}

function loginuser($username,$password){

	$username = mres($username);
	
	if($username == NULL OR $password == NULL){
		$final_report.="Please complete all the fields below...";
	}else{
		$check_user_data = mysql_query("SELECT * FROM staff WHERE `username` = '$username'") or die(mysql_error());
		if(mysql_num_rows($check_user_data) == 0){
			$final_report.="<div class='alert alert-warning'>This username does not exist.</div>";
		}else{
			$get_user_data = mysql_fetch_array($check_user_data);
			$dbpw = $get_user_data['password'];
			if( crypt($password,$dbpw) != $dbpw){
				// bad password
				$final_report.= "<div class='alert alert-warning'>Your password was incorrect - please try again.</div>";
			}else{
				// Successful Login
				$_SESSION['CrisUsername'] = "".$get_user_data['username']."";
				$_SESSION['CrisLoggedIn'] = TRUE;
				$_SESSION['CrisUserID'] = $get_user_data['id'];
				$_SESSION['CrisAdmin'] = $get_user_date['admin'];
				$_SESSION['CrisFullName'] = $get_user_data['firstname']." ".$get_user_data['lastname'];
				
				$final_report.="<div class='alert alert-success'>Username and Password accepted, please wait while we log you in...<br /><img src='".ROOT_PATH."/img/ajax-loader.gif' /><meta http-equiv='Refresh' content='1; URL=".ROOT_PATH."/index.php'/></div>";
				$r = mysql_query("UPDATE staff SET last_logged_in = '".date('Y-m-d H:i:s',time())."' WHERE id = '".$get_user_data['id']."'");
				$success = TRUE;
			}
		}
	}
	$rt = array('success'=>$success,'report'=>$final_report);
	return $rt;
}

function check_login(){

	session_start();

	if(!(isset($_SESSION['CrisUsername']) && $_SESSION['CrisLoggedIn'])){
		header('location:login.php');
		return false;
	}else{
		return true;
	}
}

function check_admin(){
		$check_user_data = mysql_query("SELECT * FROM staff WHERE id = '".$_SESSION['CrisUserID']."'");
		$get_user_data = mysql_fetch_array($check_user_data);
		if($get_user_data['admin'] == 1 ) {return true;}else{return false;}
}

function sortable_header($dbcol,$title,$sortby,$direction,$rowspan=1){
	$str = "<th rowspan=$rowspan onclick='sort_by_query(\"sortby\",\"".$dbcol."\",false,event);sort_by_query(\"direction\",";
	$str .= ($sortby == $dbcol && $direction=='asc')?"\"desc\"":"\"asc\"";
	$str .= ",true,event);'>$title";
	$str .= ($sortby == $dbcol) ? " *</th>":"</th>";
	return $str;
}


	
function super_unique($array,$key)
{
   $temp_array = array();
   foreach ($array as &$v) {
       if (!isset($temp_array[$v[$key]]))
       $temp_array[$v[$key]] =& $v;
   }
   $array = array_values($temp_array);
   return $array;

}

function build_active_table(){
/*			session_attendance.participant_id, 
			session_attendance.session_id, 
			sessions.id, 
			sessions.session_type, 
			sessions.session_date
			session_types.id, 
			session_types.project_id, */
			
	$r = mysql_query("TRUNCATE TABLE active_participants");		
	$r = mysql_query("TRUNCATE TABLE attended_participants");		
	$pq = "SELECT * FROM projects";
	if(!($projects = mysql_query($pq))){echo "Error getting project list";}else{
		while($project = mysql_fetch_array($projects)){
			$ypq = "SELECT * FROM (SELECT participants.id AS participantid, projects.id AS projectid, sessions.session_date AS sdate
			FROM participants
			INNER JOIN session_attendance ON session_attendance.participant_id = participants.id
			INNER JOIN sessions ON sessions.id = session_attendance.session_id
			INNER JOIN session_types ON session_types.id = sessions.session_type
			INNER JOIN projects ON projects.id = session_types.project_id
			WHERE projects.id = '".$project['id']."'
			ORDER BY participants.id ASC, sessions.session_date DESC) AS temp GROUP BY participantid";
			if($yr = mysql_query($ypq)){
				$last_ypid = 0;//reset last_ypid
				while($active = mysql_fetch_array($yr)){		
					if((time() - strtotime($active['sdate'])) < (30*24*60*60)){
						// if the date was less than a month ago
						// then add the yp to active table and attended table
						if($active['participantid'] != $last_ypid){
							//if we've not already found a date for this yp
							$ir = mysql_query("INSERT INTO active_participants SET project_id = '".$active['projectid']."', participant_id = '".$active['participantid']."', latest_activity_date = '".$active['sdate']."'");
							//echo "pid ".$project['id']." project_id = '".$active['projectid']."', participant_id = '".$active['participantid']."', latest_activity_date = '".$active['sdate']."' <br />";
							$last_ypid = $active['participantid'];
							$ir = mysql_query("INSERT INTO attended_participants SET project_id = '".$active['projectid']."', participant_id = '".$active['participantid']."', latest_activity_date = '".$active['sdate']."'");
						}
					}else{
						// just add yp to attended table
						$last_ypid = $active['participantid'];
						$ir = mysql_query("INSERT INTO attended_participants SET project_id = '".$active['projectid']."', participant_id = '".$active['participantid']."', latest_activity_date = '".$active['sdate']."'");				
					}			
				}
					
					
				//}
			}else{
				echo "Error getting active list. ".mysql_error();
			}
		}
	}
}
?>