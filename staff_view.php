<?php

require('header.php');
require('navmenutop.php');

$activetick = true;
$inactivetick = false;
$eitick = true;
$stafftick = true;
$voltick = true;

if($_REQUEST['filter']){
	$filters = explode("::",$_REQUEST['filter']);
	foreach($filters as $k => $v){
		if($v == 'active') $activetick = false;
		if($v == 'inactive') $inactivetick = false;
		if($v == 'ei') $eitick = false;
		if($v == 'staff') $stafftick = false;
		if($v == 'vol') $voltick = false;
	}
}

if(isset($_GET['sortby'])){
	$sortby = $_GET['sortby'];
}else{
	$sortby = 'lastname';
}
if(isset($_GET['direction'])){
	$direction = $_GET['direction'];
}else{
	$direction = 'ASC';
}


?>

<div id=container>

<?php


if($_REQUEST['added']){$report .= "<p>New staff member added (ID = ".$_REQUEST['added'].")</p>";
echo $report;
}


?>
<div id=toolbar class='hidden-print'>
	<div class=first>
		<span style="font-weight: bold;font-size: 200%;vertical-align: middle;">Worker Database</span>
	</div>
	<div>
		<a href="javascript:window.print()"><button class='btn btn-med btn-primary link-btn'>Print</button></a>
	</div>
	<div>
		<a href="<?php echo ROOT_PATH;?>/staff_add.php"><button class='btn btn-med btn-primary link-btn'>Add New Worker</button></a>
	</div>
	<div>
		Search Workers: <input type=text id=searchstaff class=form-control style='display:inline-block;width:auto;' />
	</div>
	<div>
		<fieldset><legend>Status</legend>
		<input type=checkbox id=filter_active value=1 class=filterbox <?php echo $activetick ? "checked" : "" ?> /> Active
		<input type=checkbox id=filter_inactive value=1 class=filterbox <?php echo $inactivetick ? "checked" : "" ?> /> Inactive<br />
		<input type=checkbox id=filter_ei value=1 class=filterbox <?php echo $eitick ? "checked" : "" ?> /> Expressed Interest
		</fieldset>
	</div>
	<div>
		<fieldset>
		<legend>Type</legend>
		<input type=checkbox id=filter_staff value=1 class=filterbox <?php echo $stafftick ? "checked" : "" ?> /> Workers<br />
		<input type=checkbox id=filter_vol value=1 class=filterbox <?php echo $voltick ? "checked" : "" ?> /> Volunteers<br />
		</fieldset>
			
	</div>
	
</div>

<p id=report></p>
<span id=updatespan style='display:none;position:absolute;padding:10px'>Updated</span>

<?php


$stafflist = "";
$q = "SELECT * FROM staff ORDER BY $sortby $direction";
if(!($r = mysql_query($q))){
	echo "Error getting staff. ".mysql_error();
}else{
	echo "<table id=staff style='' class='table'><thead>";
	echo "<tr class=stickyheader>";
	echo sortable_header('status','Active',$sortby,$direction);
	echo sortable_header('id','ID',$sortby,$direction);
	echo sortable_header('firstname','First Name',$sortby,$direction);
	echo sortable_header('lastname','Last Name',$sortby,$direction);
	echo sortable_header('staff','Worker',$sortby,$direction);
	echo sortable_header('volunteer','Volunteer',$sortby,$direction);
	echo sortable_header('telephone','Phone',$sortby,$direction);
	echo sortable_header('email','Email',$sortby,$direction);
	echo "</tr>";
	echo "<tr class='floatheader sticky' style='display:none;'>";
	echo sortable_header('status','Active',$sortby,$direction);
	echo sortable_header('id','ID',$sortby,$direction);
	echo sortable_header('firstname','First Name',$sortby,$direction);
	echo sortable_header('lastname','Last Name',$sortby,$direction);
	echo sortable_header('staff','Worker',$sortby,$direction);
	echo sortable_header('volunteer','Volunteer',$sortby,$direction);
	echo sortable_header('telephone','Phone',$sortby,$direction);
	echo sortable_header('email','Email',$sortby,$direction);
	echo "</tr></thead>";
	$odd = false;
	while($staff = mysql_fetch_array($r)){
		$sc = "";
		if($staff['staff'] == 1) $sc .= " staff";
		if($staff['volunteer'] == 1) $sc .= " vol";
		if($staff['status'] == 1) $sc .= " activeworker";
		if($staff['status'] == 2) $sc .= " ei";
		if($staff['status'] == 0) $sc .= " inactive";
		if($odd) {$sc .= " odd"; $odd = false;}else{$odd = true;}
		echo "<tr id=staff_".$staff['id']." class='clickable_staff $sc'>";
		echo "<td class='status'>"; 
			echo $staff['status'] == 1 ? "<b>Y</b>":"";
			echo $staff['status'] == 2 ? "E":"";
		echo "</td>";
		echo "<td class='id'>".$staff['id']."</td>";
		echo "<td class=firstname>".$staff['firstname']."</td>";
		echo "<td class=lastname>".$staff['lastname']."</td>";
		echo "<td class=staff>"; echo $staff['staff'] == 1 ? "&#10004;":""; echo "</td>";
		echo "<td class=vol>"; echo $staff['volunteer'] == 1 ? "&#10004;":""; echo "</td>";
		echo "<td class=tel>".$staff['telephone']."<br />".$staff['mobile']."</td>";	
		echo "<td class=email>".$staff['email']."</td>";	
		//echo "<td style='min-width:100px;'><button class=edit id=edit_".$staff['id'].">View / Edit</button></td><td><button class=delete id=delete_".$staff['id'].">Delete</button></td>";
		
		echo "</tr>";
		$stafflist .= "{label:'".$staff['firstname']." ".$staff['lastname']."',value: '".$staff['id']."'},";

	}
	$stafflist = trim($stafflist, ",");
	//$stafflist .= "]";
	echo "</table>";
}

?>

<div class="modal fade" id=editstaff role="dialog" aria-labelledby="editstafflabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id='editstafflabel'>View/Edit Worker</h4>
      </div>
      <div class="modal-body">
			<div id='summarytable' class=edittoggle></div>
			<form id=editstaffform class=edittoggle style='display:none;'>
			<?php require('staff_form.php') ?>
			</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" onclick="$('.edittoggle').toggle();"><span class='edittoggle'>Edit</span><span class='edittoggle' style='display:none;'>Cancel Editing</span></button>
        <button type="button" class="btn btn-primary edittoggle" onclick="printtable();">Print</button>
        <button type="button" class="btn btn-primary edittoggle" style='display:none;' onclick='saveStaff();'>Save Changes</button>
		<? if($admin) echo "<button type='button' class='btn btn-danger edittoggle' style='display:none;' onclick='deleteStaff();'>Delete Worker</button>";?>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <!--<button type="button" class="btn btn-primary">Save changes</button>-->
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script src='<?php echo ROOT_PATH;?>/js/staff_functions.js'></script>

<script>
$( document ).ready(function() {
	staff = [<?echo $stafflist ?>];
	var stickyNavTop = $('table#staff tr.stickyheader').offset().top;
	col = new Array();
	$('table#staff tr.stickyheader th').each(function(){
		col.push($(this).css('width'));
	});
	
	

	$('#searchstaff').autocomplete({
		source: staff,
		select: function( event, ui ) {
			event.preventDefault();
       		id = ui.item.value;
			$('#searchstaff').val(ui.item.label);
			if(id){
				$('tr#staff_'+id).children('td').last().append("<img src='img/ajax-loader-sm.gif' id=ajaxload style='margin-left:10px;' />");
				getStaffDetails(id);
				
			}
      }
	});
	
	$('.ui-autocomplete').appendTo('#navsidemenu');
	do_filter();
	
	$(window).scroll(function() {
	  	var scrollTop = $(window).scrollTop();
		//console.log("NavTop:"+stickyNavTop+" ScrollTop:"+scrollTop);
		if(scrollTop > stickyNavTop) {
		  $('table#staff tr.floatheader').show();
		  var i = 0;
			$('table#staff tr.floatheader th').each(function(){
				$(this).css('width',col[i]);
				i++;
			}); 
		} else {
		  $('table#staff tr.floatheader').hide();
		}
	});
		

});
</script>
<?php
require('footer.php');
?>