<?php

require_once ("config_cris.php");
check_login();
dbconnect();
$admin = check_admin();

if(isset($_POST['session_type'])){


	foreach($_POST as $key=>$value){
		if($key == "session_date" || $key == 'session_end_date'){
			$$key = date("Y-m-d",strtotime($value));
		}else{
			$$key = mres($value);
		}
	}
	
	
	
	//validate
	if($_POST['splitnotes'] == 1){
		$notes = 
		"<h4>How was the young person?</h4>".nl2br($notesa).
		"<h4>What did you do?</h4>".nl2br($notesb).
		"<h4>What was good / challenging?</h4>".nl2br($notesc).
		"<h4>What did the young person produce?</h4>".nl2br($notesd).
		"<h4>What evidence of this was collected, and where can it be found?</h4>".nl2br($notese);
	}
	


	
	// record session

	$q = "INSERT INTO sessions SET ".
	"user_id = '".$_SESSION['CrisUserID']."', ".
	"session_type = '$session_type', ".
	"staff_leader = '$staff_leader', ".
	"session_date = '$session_date', ".
	"session_end_date = '$session_end_date', ".
	"session_start_time = '$session_start_time', ".
	"session_end_time = '$session_end_time', ".
	"location = '$location', ".
	"notes = '$notes', ".
	"action = '$actionrequired', ".
	"action_text = '$actions', ".
	"sg = '$sg', ".
	"sgdetails = '$sgdetails'";

	if($r = mysql_query($q)){
		
		$session_id = mysql_insert_id();
		$return_str .= "s=1&sid=$session_id&";
	}else{
		$return_str .="s=0&error=".urlencode(mysql_error())."&";
		exit;
	}
	
	
	
	// handle file uploads
	/*
	$file_uploaded = false;
	$uploaded_files = array();
	$i = 1;
	$q = "SELECT sessions.id, session_types.session_name, sessions.session_date, staff.firstname, staff.lastname, projects.name, sessions.location  FROM sessions
	LEFT JOIN session_types ON sessions.session_type = session_types.id
	LEFT JOIN projects ON session_types.project_id = projects.id
	LEFT JOIN staff ON sessions.staff_leader = staff.id
	WHERE sessions.id = '$session_id'
	";
	if($r = mysql_query($q)){	$session_info = mysql_fetch_array($r);}else{ echo "ARG".mysql_error();}

	while($_FILES['upload_'.$i]['name']){

		if(!$_FILES['upload_'.$i]['error']){
			// no errors
			//print_r($session_info);
			//now is the time to modify the future file name and validate the file
			$new_file_name = $session_info['name']."-".$session_info['session_name']."-".time()."-".$session_id."-".strtolower(str_replace(" ","_",$_FILES['upload_'.$i]['name'])); //rename file
			if($_FILES['upload_'.$i]['size'] > (5120000)) //can't be larger than 5 MB
			{
				$valid_file = false;
				echo "<div class='alert alert-error hidden-print'>One of your files was too large. Files should be less than 5Mb in size.</div>";
				//$message = 'Oops!  Your file\'s size is too large.';
			}else{
				$valid_file = true;
			}
			//if the file has passed the test
			if($valid_file)
			{
				//move it to where we want it to be
				if(move_uploaded_file($_FILES['upload_'.$i]['tmp_name'], 'uploads/'.$new_file_name)){
					
					
					$file_uploaded = true;
				}else{
					// problem moving file
					echo "<div class='alert alert-error hidden-print'>Error when moving the uploaded file.</div>";
					
				}
			}
		
		}else{
			// errors with file upload
			switch ($_FILES['upload_'.$i]['error']) {
				case UPLOAD_ERR_OK:
					break;
				case UPLOAD_ERR_NO_FILE:
					echo "<div class='alert alert-error hidden-print'>No file sent.</div>";
				case UPLOAD_ERR_INI_SIZE:
				case UPLOAD_ERR_FORM_SIZE:
					echo "<div class='alert alert-error hidden-print'>Exceeded filesize limit.</div>";
				default:
					echo "<div class='alert alert-error hidden-print'>Unknown Error.</div>";
			}
			echo "<div class='alert alert-error hidden-print'>Error with upload. Error = ".$_FILES['upload_'.$i]['error']."</div>";
		}
		
		// add file info to database
		if($file_uploaded){
			$q = "INSERT INTO uploaded_files SET 
			session_id = '$session_id',
			uploaded_by = '".$_SESSION['CrisUserID']."',
			date_uploaded = '".date('Y-m-d H:i:s',time())."',
			filename = '$new_file_name',
			filetype = '".$_FILES['upload_'.$i]['type']."'";
			if($r = mysql_query($q)){
				echo "<div class='alert alert-success hidden-print'>File $i successfully uploaded and added to database - <a href='uploads/".$new_file_name."'>$new_file_name - ".round($_FILES['upload_'.$i]['size'] / 1024,2)." KB</a></div>";
				
				$uploaded_files[] = array('filename' => $new_file_name, 'type' => $_FILES['upload_'.$i]['type']);
			}else{
				echo "<div class='alert alert-error hidden-print'>Error adding filename to database</div>";
			}
		
		}
		
		$i++;	
	}
	*/
	
	// record attendance
	
	// staff
	$staff = explode("::",$staff_list);
	foreach($staff as $staffid){
		if( $r != mysql_query("INSERT INTO session_attendance SET session_id = '$session_id', participant_id = '0', staff_id = '$staffid'")){
			$return_str .= "w=0&error=".urlencode(mysql_error())."&";
			exit;
		}else{
			$return_str .= "w=1&";
		}
	}
	
	// yp
	$yp = explode("::",$parti_list);
	foreach($yp as $ypid){
		if( $r != mysql_query("INSERT INTO session_attendance SET session_id = '$session_id', participant_id = '$ypid', staff_id = '0'")){
			//echo "<div class='alert alert-warning error'>Error when recording session attendance. ".mysql_error()."</div>";
			$return_str .= "yp=0&error=".urlencode(mysql_error())."&";
			exit;
		}else{
			$return_str .= "yp=1&";
		}
	}
	$return_str = trim($return_str,"&");
	mysql_close();
	header("Location: sessions_add.php?submit=1&$return_str");
	die();
	
}else{
	mysql_close();
	header("Location: sessions_add.php?submit=-1");
}
?>