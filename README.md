cris-development
================

Development git for the CRIS (Cre8 Recording and Information System)

Version 1.06
	- Fixed bugs in refering to new filenames from v1.01
	- added mysql_close to all files that open a db connection
	- removed some whitespace
	- moved all JS files locally to try to improve page load time


Version 1.05 10/12/2015
    
    New version encompassing changes over the last few months...
    

Version 1.01 20/3/2015

    new files
        - sessions_submit (separates submission of new session into separate php file - to prevent duplicate submissions)
        - sessions_remove_duplicates (php file that lists potential duplicates for manual removal)
    
    updated
        - Sessions module - all files

Version 1.0 18/3/2015

	php files are in root directory
	JS files are in js directory
	css files are in css directory
	
	- General files
		- index.php (main page with stats and menus)
		- config_cris.php (contains general php functions and database connections)
		- crisapi.php (file called by ajax functions to perform background php and database operations without reloading a page)
		- navmenutop.php (top navigation bar)
		- footer.php (footer called by index and login.php
		- header.php (header file that connects to DB, checks for login, checks for admin rights, and loads css and js files)
		- css/style.css (general styles)
		- crisconnect.php (db connection details - different versions for the development database and live database)
		- js/functions.js (general js functions)
		
	- Modules
		- login and index
			- index.php 
			- login.php (login user - all pages default to this if no login is detected)
			- logout.php (unsets session variables and logs user out)
		- young people 
			- yp_functions.js 
			- yp_add.php (adding a young person)
			- yp_view.php (view / edit young people)
			- yp_form.php (form used in editing and adding yp)
		- workers
			- staff_functions.js
			- staff_add.php
			- staff_view.php
			- staff_form.php
		- contacts
			- contacts_functions.js
			- contacts_add.php
			- contacts_view.php
			- contacts_form.php
		- projects
			- projects_view.php
		- sessions
			- sessions_view.php
			- sessions_functions.js
			- sessions_add.php
			- sessions_form.php
		- measures
			- measures_view.php
			- measures_functions.js
			- measures_record.php
		- reports
			- reports.php
			- reports_load.php
			- reports_functions.js		
		
Version 0.15 - Staff DB working as prototype.

Version 0.1 - Initial commit to github - work in progress - 
	- working
		- login procedure
	- part finished 
		- view contact
		- view staff 
		- add contact
		- add staff
		





