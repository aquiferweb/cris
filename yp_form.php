	<div class='session_input'>
		<h2>Name and Details</h2>
		<label for=firstname>First Name:</label><input type=text name=firstname id=firstname class=required /><br />
		<label for=lastname>Last Name:</label><input type=text name=lastname id=lastname class=required /><br />
		<label for=gender>Gender:</label><input type=radio name=gender value='M' class=genderopt checked /><label class=radio >Male</label><input type=radio name=gender value='F' class=genderopt /><label class=radio>Female</label><br />
		<label for=dob>Date of Birth:</label><input type=date name=dob id=dob value='' class='dateDOB' /><span id=calc_age></span><br />
		<!--<label>Current Status:</label>
		<div style='text-align:center;display:inline-block;'>
			<input type=radio name=status id=status0 value=0 /> <label for=status0 class=radio>Inactive</label>
			<input type=radio name=status id=status1 value=1 /> <label for=status1 class=radio>Active</label>
			<input type=radio name=status id=status2 value=2 /> <label for=status2 class=radio>Expressed Interest</label>
		</div><br />-->

		<label for=start_date>Start Date at Cre8</label><input type=date name=start_date id=start_date value='' class='dateY required' /><br />
		<label for=key_worker>Key Worker at Cre8:</label><?php
				$q = "SELECT * FROM staff WHERE staff='1'";
				if($r = mysql_query($q)){
					echo "<select name=key_worker id=key_worker>";
					echo "<option value='' disabled selected>Choose Key Worker</option>";
					while($st = mysql_fetch_array($r)){
						echo "<option value='".$st['id']."'>".$st['firstname']." ".$st['lastname']."</option>";
					}
					echo "</select>";
				}else{
					echo "<p class=error>Error selecting staff list. ".mysql_error()."</p>";
				}
			?>
		</select><br />
		
		
	</div>	
	<div class='session_input'>
		<h2>Details</h2>
		<label for=school>School:</label><input type=text name=school id=school /><br />
		<label for=school_year>School Year:</label><select name=school_year id=school_year class=short>
			<option value="-3">- 3 years</option>
			<option value="-2">- 2 years</option>
			<option value="-1">-1 year</option>
			<option value="0" selected>Normal</option>
			<option value="1">+ 1 year</option>
			<option value="2">+ 2 years</option>
			<option value="3">+ 3 years (really?)</option>
			
		</select><span id=suggest_year></span><br />
		<label for=address>Address:</label><input type='text' name='address' id=address /><br />	
		<label for=postcode>Postcode:</label><input type='text' name='postcode' id=postcode /><br />
		<label for=area>Area:</label><input type='text' name='area' id=area /><br />		
		<label for=guardian_name>Parent / Guardian:</label><input type='text' name='guardian_name' id=guardian_name /><br />
		<label for=yp_phone>Young Person's phone:</label><input type='text' name='yp_phone' id=yp_phone /><br />	
		<label for=guardian_phone>Home phone:</label><input type='text' name='guardian_phone' id=guardian_phone /><br />		
	</div>	
	
	<div class='session_input'>
		<h2>Registration Information</h2>
		<!--<label for=guardian_signature_date>Guardian Signature Date:</label><input type=text name=guardian_signature_date id=guardian_signature_date value='' class=dateY /><br />-->
		<label>Referrer:</label><input type=text name=referrer id=referrer /><br />
		<label>Projects Registered for (and date registered)</label><span id=consentlist><ul></ul></span><br />

		<label>Add registration to:</label><select id=newconsentp><option value='' disabled selected>--Choose a project--</option>
		
		<?php 
			//printr($projectlist);
			foreach($projectlist as $project){
			echo "<option value='".$project['id']."'>".$project['name']."</option>";
		}
		?>
		</select> on <input type=date class=dateY id=newconsentdate placeholder='Choose a date' /> <button id=newconsentsubmit type=button class='btn btn-primary btn-sm'>Add</button>
		<input type=hidden name=delconsentlist id=delconsentlist /><br />
		<input type=hidden name=projectconsent id=projectconsent /><br />
		<label for=photo_consent>Photo Consent:</label><input type=radio name=photo_consent value=0 id=photo_consent_no class=pconsent checked /><label class=radio for=photo_consent_no>No</label><input type=radio name=photo_consent class=pconsent value=1 id=photo_consent_yes /><label class=radio for=photo_consent_yes>Yes</label><br />
		<label for=facebook_consent>Facebook Consent:</label><input type=radio name=facebook_consent value=0 id=facebook_consent_no class=fconsent checked /><label class=radio for=facebook_consent_no>No</label><input type=radio name=facebook_consent value=1 class=fconsent id=facebook_consent_yes /><label class=radio for=facebook_consent_yes>Yes</label><br />
	</div>

	<div class='session_input'>
		<h2>Expression of Interest Information</h2>
		<label>Projects Expressed Interest in (and date expressed)</label><span id=interestlist><ul></ul></span><br />

		<label>Add Interest in:</label><select id=newinterestp><option value='' disabled selected>--Choose a project--</option>
		
		<?php 
			foreach($projectlist as $project){
			echo "<option value='".$project['id']."'>".$project['name']."</option>";
		}
		?>
		</select> on <input type=date class=dateY id=newinterestdate max="<?php echo date('Y-m-d'); ?>"  value="<?php echo date('Y-m-d'); ?>" /> <button id=newinterestsubmit type=button class='btn btn-primary btn-sm'>Add</button>
		<input type=hidden name=delinterestlist id=delinterestlist /><br />
		<input type=hidden name=projectinterest id=projectinterest /><br />
	</div>
	
	<div class='session_input'>
		<h2>Health and Other Details</h2>
		<label>Health Form received:</label><input type=checkbox name=health_form_received value=1 id=health_form_checkbox onclick="$('#dhfr').toggle()" />  <span id=dhfr style="display:none;width:300px;">Date: <input type=date name=date_health_form_received id=date_health_form_received /></span><br />
		<label for=medical>Medical / health notes</label><textarea name=medical id=medical></textarea>
		<label for=allergies>Allergies</label><textarea name=allergies id=allergies></textarea>
		<label for=notes>General Notes</label><textarea name=notes id=notes></textarea>
		<!--<label for=issues>Specific Issues</label><textarea name=issues id=issues></textarea>-->
		<label for=caf>Does this young person have a CAF:</label><input type=radio name=caf value=0 id=caf_no class=cafopt checked /> <label class=radio for=caf_no>No</label><input type=radio name=caf value=1 id=caf_yes class=cafopt /><label class=radio for=caf_yes>Yes</label><br />
	</div>