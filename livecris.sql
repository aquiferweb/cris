-- phpMyAdmin SQL Dump
-- version 3.2.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 18, 2016 at 03:07 PM
-- Server version: 5.0.70
-- PHP Version: 5.2.10-pl0-gentoo

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `livecris`
--

-- --------------------------------------------------------

--
-- Table structure for table `active_participants`
--

CREATE TABLE IF NOT EXISTS `active_participants` (
  `participant_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `latest_activity_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `attended_participants`
--

CREATE TABLE IF NOT EXISTS `attended_participants` (
  `participant_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `latest_activity_date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE IF NOT EXISTS `contacts` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `organisation` varchar(255) NOT NULL,
  `website` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `address2` varchar(256) NOT NULL,
  `address3` varchar(256) NOT NULL,
  `postcode` varchar(255) NOT NULL,
  `telephone` varchar(255) NOT NULL,
  `job_title` varchar(255) NOT NULL,
  `category` varchar(255) NOT NULL,
  `notes` text NOT NULL,
  `date_added` date NOT NULL,
  `date_updated` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=360 ;

-- --------------------------------------------------------

--
-- Table structure for table `contact_type`
--

CREATE TABLE IF NOT EXISTS `contact_type` (
  `id` int(11) NOT NULL auto_increment,
  `type` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=32 ;

-- --------------------------------------------------------

--
-- Table structure for table `measures`
--

CREATE TABLE IF NOT EXISTS `measures` (
  `id` int(11) NOT NULL auto_increment,
  `active` tinyint(1) NOT NULL default '1',
  `measure_text` text NOT NULL,
  `description` text NOT NULL,
  `category` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=56 ;

-- --------------------------------------------------------

--
-- Table structure for table `measures_data`
--

CREATE TABLE IF NOT EXISTS `measures_data` (
  `id` int(11) NOT NULL auto_increment,
  `participant_id` int(11) NOT NULL,
  `measure_id` int(11) NOT NULL,
  `date` date NOT NULL,
  `score` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `recorded_by` int(11) NOT NULL,
  `interviewed_by` int(11) NOT NULL,
  `notes` text NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=728 ;

-- --------------------------------------------------------

--
-- Table structure for table `participants`
--

CREATE TABLE IF NOT EXISTS `participants` (
  `id` int(11) NOT NULL auto_increment,
  `dob` date NOT NULL,
  `start_date` date NOT NULL,
  `firstname` varchar(128) NOT NULL,
  `lastname` varchar(128) NOT NULL,
  `address` varchar(512) NOT NULL,
  `postcode` varchar(128) NOT NULL,
  `area` varchar(128) NOT NULL,
  `yp_phone` varchar(128) NOT NULL,
  `guardian_name` varchar(128) NOT NULL,
  `school_year` int(11) NOT NULL default '0',
  `school` varchar(128) NOT NULL,
  `gender` varchar(2) NOT NULL,
  `guardian_signature_date` date NOT NULL,
  `guardian_phone` varchar(128) NOT NULL,
  `photo_consent` tinyint(1) NOT NULL,
  `facebook_consent` tinyint(1) NOT NULL,
  `health_form_received` tinyint(1) NOT NULL,
  `date_health_form_received` date NOT NULL,
  `medical` text NOT NULL,
  `allergies` text NOT NULL,
  `notes` text NOT NULL,
  `issues` varchar(128) NOT NULL,
  `caf` varchar(128) NOT NULL,
  `key_worker` int(11) NOT NULL,
  `referrer` varchar(256) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=376 ;

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
  `display_order` int(11) NOT NULL,
  `id` int(11) NOT NULL auto_increment,
  `active` tinyint(1) NOT NULL default '1',
  `name` varchar(255) NOT NULL,
  `project_leader` int(11) NOT NULL,
  `short_name` varchar(128) NOT NULL,
  `colour` varchar(10) NOT NULL,
  `black_text` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

-- --------------------------------------------------------

--
-- Table structure for table `projects_consented`
--

CREATE TABLE IF NOT EXISTS `projects_consented` (
  `id` int(11) NOT NULL auto_increment,
  `participant_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1089 ;

-- --------------------------------------------------------

--
-- Table structure for table `projects_interested`
--

CREATE TABLE IF NOT EXISTS `projects_interested` (
  `id` int(11) NOT NULL auto_increment,
  `project_id` int(11) NOT NULL,
  `participant_id` int(11) NOT NULL,
  `date` date NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=82 ;

-- --------------------------------------------------------

--
-- Table structure for table `savedreports`
--

CREATE TABLE IF NOT EXISTS `savedreports` (
  `id` int(11) NOT NULL auto_increment,
  `summary` text NOT NULL,
  `date` datetime NOT NULL,
  `saved_by` int(11) NOT NULL,
  `querystr` text NOT NULL,
  `title` varchar(128) NOT NULL,
  `querytable` varchar(128) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=34 ;

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE IF NOT EXISTS `sessions` (
  `id` int(11) NOT NULL auto_increment,
  `active` tinyint(1) NOT NULL default '1',
  `session_type` int(11) NOT NULL,
  `user_id` int(4) NOT NULL,
  `staff_leader` int(4) NOT NULL,
  `session_date` date NOT NULL,
  `session_end_date` date NOT NULL,
  `session_start_time` time NOT NULL,
  `session_end_time` time NOT NULL,
  `location` varchar(256) NOT NULL,
  `notes` text,
  `action` int(3) NOT NULL default '0',
  `action_text` text NOT NULL,
  `timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `notify` tinyint(1) NOT NULL default '0',
  `sg` tinyint(1) NOT NULL default '0',
  `sgdetails` text NOT NULL,
  `editedby` int(11) NOT NULL,
  `editeddate` datetime NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2344 ;





-- --------------------------------------------------------

--
-- Table structure for table `session_attendance`
--

CREATE TABLE IF NOT EXISTS `session_attendance` (
  `session_id` int(11) NOT NULL,
  `staff_id` int(11) NOT NULL,
  `participant_id` int(11) NOT NULL,
  KEY `session` (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `session_types`
--

CREATE TABLE IF NOT EXISTS `session_types` (
  `id` int(11) NOT NULL auto_increment,
  `active` tinyint(1) NOT NULL default '1',
  `session_name` varchar(256) NOT NULL,
  `group_activity` tinyint(1) NOT NULL,
  `extra_field` varchar(256) NOT NULL,
  `project_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=33 ;

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE IF NOT EXISTS `staff` (
  `id` int(11) NOT NULL auto_increment,
  `firstname` varchar(256) NOT NULL,
  `lastname` varchar(256) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `last_logged_in` datetime NOT NULL,
  `admin` tinyint(1) NOT NULL default '0',
  `staff` tinyint(1) NOT NULL default '0',
  `cre8works` tinyint(1) NOT NULL default '0',
  `volunteer` tinyint(1) NOT NULL default '0',
  `role` varchar(256) NOT NULL,
  `timestamp` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `date_started` date NOT NULL,
  `line_manager` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL default '1' COMMENT '0 - inactive, 1 - active, 2 - expressed interest',
  `category` varchar(255) NOT NULL,
  `job_title` varchar(255) NOT NULL,
  `projects` varchar(256) NOT NULL,
  `address` tinytext NOT NULL,
  `postcode` varchar(255) NOT NULL,
  `telephone` varchar(255) NOT NULL,
  `mobile` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `app_form_date` date NOT NULL,
  `app_form_seen_by` varchar(255) NOT NULL,
  `selfdec_form_date` date NOT NULL,
  `selfdec_form_seen_by` varchar(255) NOT NULL,
  `interview_date` date NOT NULL,
  `interview_staff` varchar(255) NOT NULL,
  `dbs_number` varchar(255) NOT NULL,
  `dbs_expiry_date` date NOT NULL,
  `ref1_date` date NOT NULL,
  `ref1_seen_by` varchar(255) NOT NULL,
  `ref2_date` date NOT NULL,
  `ref2_seen_by` varchar(255) NOT NULL,
  `vocational_quals` text NOT NULL,
  `induction` tinyint(1) NOT NULL,
  `agreement_given` tinyint(1) NOT NULL,
  `driving_licence` tinyint(1) NOT NULL,
  `qualifications` text NOT NULL,
  `experience` text NOT NULL,
  `health_notes` text NOT NULL,
  `notes` text NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=84 ;


--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`id`, `firstname`, `lastname`, `username`, `password`, `last_logged_in`, `admin`, `staff`, `cre8works`, `volunteer`, `role`, `timestamp`, `date_started`, `line_manager`, `status`, `category`, `job_title`, `projects`, `address`, `postcode`, `telephone`, `mobile`, `email`, `app_form_date`, `app_form_seen_by`, `selfdec_form_date`, `selfdec_form_seen_by`, `interview_date`, `interview_staff`, `dbs_number`, `dbs_expiry_date`, `ref1_date`, `ref1_seen_by`, `ref2_date`, `ref2_seen_by`, `vocational_quals`, `induction`, `agreement_given`, `driving_licence`, `qualifications`, `experience`, `health_notes`, `notes`) VALUES
(84, 'install', 'install', 'install', '$1$eNLjVLjk$g/OcTbhBtM/vjgaFfPLhO1', '2016-02-18 15:14:59', 1, 1, 0, 0, '', '2016-02-18 15:14:59', '0000-00-00', -1, 0, '', '', '', '', '', '', '', '', '0000-00-00', '', '0000-00-00', '', '0000-00-00', '', '', '0000-00-00', '0000-00-00', '', '0000-00-00', '', '', 0, 0, 0, '', '', '', '');
-- --------------------------------------------------------

--
-- Table structure for table `variables`
--

CREATE TABLE IF NOT EXISTS `variables` (
  `id` int(11) NOT NULL auto_increment,
  `k` varchar(255) NOT NULL,
  `v` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;
