<?php

require('header.php');
require('navmenutop.php');

?>

<div id=container><div style='max-width:1000px;margin:auto;'>
	<h1>Cre8 Information and Recording System</h1>
	<p>Welcome to the recording system. A summary of latest information is below. Please click on an option in the navigation menu to proceed.</p>
	
	
	<div id='bignav'>
		<button type="button" class="btn btn-primary"><a href='<?php echo ROOT_PATH;?>/yp_view.php'>Young People<br /><img src='<?php echo ROOT_PATH;?>/img/children.png' /></a></button>
		<button type="button" class="btn btn-primary"><a href='<?php echo ROOT_PATH;?>/staff_view.php'>Workers / Volunteers<br /><img src='<?php echo ROOT_PATH;?>/img/staff.png' /></a></button>
		<?php if($admin) echo "<button type='button' class='btn btn-primary'><a href='".ROOT_PATH."/contacts_view.php'>Contacts<br /><img src='".ROOT_PATH."/img/contact.png' /></a></button>"; ?>
		<?php if($admin) echo "<button type='button' class='btn btn-primary'><a href='".ROOT_PATH."/projects_view.php'>Projects<br /><img src='".ROOT_PATH."/img/projects.png' /></a></button>"; ?>
		<button type="button" class="btn btn-primary"><a href='<?php echo ROOT_PATH;?>/sessions_view.php'>Sessions<br /><img src='<?php echo ROOT_PATH;?>/img/session.png' /></a></button>
		<button type="button" class="btn btn-primary"><a href='<?php echo ROOT_PATH;?>/measures_view.php'>Measures<br /><img src='<?php echo ROOT_PATH;?>/img/path.png' /></a></button>
		<button type="button" class="btn btn-primary"><a href='<?php echo ROOT_PATH;?>/reports_view.php'>Reports<br /><img src='<?php echo ROOT_PATH;?>/img/package_icon_sm.png' /></a></button>
	</div>
	
	<?php
		echo "<div class='alert alert-info'>";
		$r= mysql_query("select count(*) as total from participants");
		$data=mysql_fetch_assoc($r);
		echo "<h2>Stats!</h2>";
		echo "<p>Number of Young People in Database: ".$data['total']."</p>";

		$r= mysql_query("SELECT DISTINCT participant_id FROM active_participants");
		$data=mysql_num_rows($r);
		echo "<p>Current number of Active Young People: ".$data."</p>";
		
		$r= mysql_query("select count(*) as total from staff");
		$data=mysql_fetch_assoc($r);
		echo "<p>Number of Workers / Volunteers in Database: ".$data['total']."</p>";
		
		$r= mysql_query("select count(*) as total from contacts");
		$data=mysql_fetch_assoc($r);
		echo "<p>Number of Contacts in Database: ".$data['total']."</p>";
		echo "</div>";
	
		if(!($sessions = mysql_query("SELECT * FROM sessions ORDER BY session_date ASC"))){
			echo "<p class=warning>Error selection session database. ".mysql_error()."</p>";
		}else{
			$s = 0;
			$sd = 0;
			$ct = 0;
			$s_this_week = 0;
			$sd_this_week =  0;
			$ct_this_week = 0;
			while($session = mysql_fetch_array($sessions)){
				$duration = strtotime($session['session_end_time']) - strtotime($session['session_start_time']);
				$participants = count(split("::",$session['participants']));
				if(time() - strtotime($session['session_date']) < 7*24*60*60){
					$s_this_week ++;
					$sd_this_week = $sd_this_week + $duration;
					$ct_this_week = $ct_this_week + $duration*$participants;
				}
				$s++;
				$sd = $sd+$duration;
				$ct = $ct+$duration*$participants;
			}
			$sd_this_week = $sd_this_week / (60*60);
			$ct_this_week = $ct_this_week / (60*60);
			$sd = $sd / (60*60);
			$ct = $ct / (60*60);
		}
		
		?>
	<div class='alert alert-info'><h2>This week</h2>
	<p>Number of sessions entered in last 7 days: <?php echo $s_this_week ?> sessions</p>
	<p>Total session duration in last 7 days: <?php echo $sd_this_week ?> hours</p>
	<h2>All time</h2>
	<p>Total sessions recorded: <?php echo $s?> sessions</p>
	<p>Total session duration: <?php echo $sd?> hours</p>
	
	</div>
		<?php
		
		
		if($admin){
			$checkdate = date("Y-m-d",strtotime("+30 days"));
			$today = date('Y-m-d');
			$q = "SELECT * FROM staff WHERE dbs_expiry_date < '$checkdate' AND dbs_expiry_date > '$today' AND status = '1'  ORDER BY dbs_expiry_date ASC";
			$r = mysql_query($q);
			if(mysql_num_rows($r) > 0){
				echo "<div class='alert alert-warning''><h2>DBS Expiring</h2><p>".mysql_num_rows($r)." active worker(s)/volunteer(s) DBS will expire within 30 days.</p>";
				echo "<ul>";
				while($s = mysql_fetch_assoc($r)){
					echo "<li>".$s['firstname']." ".$s['lastname']." - ".date('j M Y',strtotime($s['dbs_expiry_date']))."</li>";
				}
				echo "</ul>";
				echo "</div>";
			}
			
			
			$q = "SELECT * FROM staff WHERE dbs_expiry_date < '$today' AND status = '1' ORDER BY dbs_expiry_date ASC";
			$r = mysql_query($q);
			if(mysql_num_rows($r) > 0){
				echo "<div class='alert alert-danger'><h2>DBS Expired!</h2><p>".mysql_num_rows($r)." active worker(s)/volunteer(s) DBS expired.</p>";
				echo "<ul>";
				while($s = mysql_fetch_assoc($r)){
					echo "<li>".$s['firstname']." ".$s['lastname']." - ".date('j M Y',strtotime($s['dbs_expiry_date']))."</li>";
				}
				echo "</ul>";
				echo "</div>";
			}

		
		}
	?>
	<style>
	.alert{width:47%;margin:10px;float:left;}
	</style>
	</div>
</div>
<?php
require('footer.php');

?>
