<div class='session_input'>
	<h2>Name and Status</h2>
	<input type=hidden name=id id=staffid />
	<input type=hidden name=tablename value=staff />
	<input type=hidden name=idfield value=staff_id />
	<label for=firstname>First Name:</label><input type=text name=firstname id=firstname class=required /><br />
	<label for=lastname>Last Name:</label><input type=text name=lastname id=lastname class=required />
	<label>Current Status:</label><div style='text-align:center;display:inline-block;'>
		<input type=radio name=status id=status0 value=0 required /> <label for=status0 class=radio>Inactive</label>
		<input type=radio name=status id=status1 value=1 /> <label for=status1 class=radio>Active</label>
		<input type=radio name=status id=status2 value=2 /> <label for=status2 class=radio>Expressed Interest</label>
	</div><br />

	<label>Worker or volunteer?</label>
	<input type=checkbox name=staff id=stafftypestaff value=1 /> <label for=stafftypestaff class=radio>Worker</label><input type=checkbox name=volunteer id=stafftypevol value=1 /> <label for=stafftypevol class=radio>Volunteer</label><br />
	<?php if($admin) echo "<label>Has a CRIS login?</label><input type=radio name=needlogin id=needloginyes value=1 onchange=\"$('#logindetails').slideToggle(100);\" /> <label for=needloginyes class=radio>Yes</label><input type=radio name=needlogin id=needloginno value=0 checked=checked onchange=\"$('#logindetails').slideToggle(100);\" /> <label for=needloginno class=radio>No</label>
	<div id=logindetails style=\"display:none;\">
	<label for=username>Username:</label><input type=text name=username id=username autocomplete=off /><br />
	<label>&nbsp;</label>(Leave password blank if you do not wish to change it)<br />
	<label for=pw1>Password:</label><input type=password name=pw1 id=pw1 autocomplete=off /><br />
	<label for=pw2>Confirm Password:</label><input type=password name=pw2 id=pw2 autocomplete=off />
	</div>"; ?>
</div>	
<div class='session_input'>
	<h2>Management</h2>
	<label for=date_started>Date started work:</label><input type=date name=date_started id=date_started value='' class=dateY /><br />
	<label for=line_manager>Line Manager:</label><?php
			$q = "SELECT * FROM staff WHERE staff='1'";
			if($r = mysql_query($q)){
				echo "<select name=line_manager id=line_manager>";
				echo "<option value='-1'>Choose Line Manager</option>";
				while($st = mysql_fetch_array($r)){
					echo "<option value='".$st['id']."'>".$st['firstname']." ".$st['lastname']."</option>";
				}
				echo "</select>";
			}else{
				echo "<p class=error>Error selecting staff list. ".mysql_error()."</p>";
			}
		?>
	</select><br />
	<label for=job_title>Job Title:</label><input type=text name=job_title id=job_title value='' />	<br />
</div>
	
<div class='session_input'>
	<h2>Contact Details</h2>
	<label for=email>Email:</label><input type='text' name='email' id=email /><br />	
	<label for=telephone>Telephone:</label><input type='text' name='telephone' id=telephone /><br />
	<label for=mobile>Mobile:</label><input type='text' name='mobile' id=mobile /><br />
	<label for=address>Address:</label><input type='text' name='address' id=address /><br />	
	<label for=postcode>Postcode:</label><input type='text' name='postcode' id=postcode />		
</div>
	
<div class='session_input'>
	<h2>Other Details</h2>
	<label for=health_notes>Medical / health notes</label><textarea name=health_notes id=health_notes></textarea>
	<label for=notes>General Notes</label><textarea name=notes id=notes></textarea>
</div>

<div class='session_input'>
	<h2>Application Process</h2>
	<label for=app_form_date>Application Form received on (date):</label><input type='date' name='app_form_date' id=app_form_date class=date /><br />
	<label for=app_form_seen_by>Application Form seen by:</label><input type='text' name='app_form_seen_by' id=app_form_seen_by /><br />
	<label for=ref1_date>Reference 1 received on (date):</label><input type='date' name='ref1_date' id=ref1_date class=date /><br />
	<label for=ref1_seen_by>Reference 1 seen by:</label><input type='text' name='ref1_seen_by' id=ref1_seen_by /><br />
	<label for=ref2_date>Reference 2 received on (date):</label><input type='date' name='ref2_date' id=ref2_date class=date /><br />
	<label for=ref2_seen_by>Reference 2 seen by:</label><input type='text' name='ref2_seen_by' id=ref2_seen_by /><br />
	<label for=selfdec_form_date>Self-declaration form received on (date):</label><input type='date' name='selfdec_form_date' id=selfdec_form_date class=date /><br />
	<label for=selfdec_form_seen_by>Self-declaration form seen by:</label><input type='text' name='selfdec_form_seen_by' id=selfdec_form_seen_by /><br />
	<label for=dbs_number>DBS Number:</label><input type='text' name='dbs_number' id=dbs_number /><br />
	<label for=dbs_expiry_date>DBS expiry date:</label><input type='date' name='dbs_expiry_date' id=dbs_expiry_date class=date /><br />
	<label for=licence>Driving Licence copy received:</label><input type=checkbox id=licence name=driving_licence value=1 /><br />
	<label for=induction>Health and Safety Induction completed:</label><input type=checkbox id=induction name=induction value=1 /><br />
	
</div>