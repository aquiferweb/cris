<? 
session_start();
require_once ("config_cris.php");
dbconnect();

$backupreport = db_backup();

$success = FALSE;
$final_report ="";

if(isset($_POST['login'])){
	$login = loginuser($_POST['username'],$_POST['password']);
	$success = $login['success'];
	$final_report = $login['report'];
}
if($success) build_active_table();

?> 

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

	<link rel="stylesheet" type="text/css" href="<?php echo ROOT_PATH;?>/css/style.css"/>	
	<link rel="stylesheet" type="text/css" href='<?php echo ROOT_PATH;?>/css/redmond/jquery-ui-1.10.3.custom.css' />
	<link rel="stylesheet" type="text/css" href='<?php echo ROOT_PATH;?>/css/jquery.timepicker.css' />
	<link href="<?php echo ROOT_PATH;?>/css/bootstrap.min.css" rel="stylesheet">

	<title>Cre8 Recording and Information System</title>


</head>
<body>

<div id=container>
	
<div><?php echo $backupreport; ?></div>
	
<form action="" method="post" id='loginform' class=form-signin role=form> 
<h1>CRIS Login Page</h1>
<p>Please enter your username and password to access the Cre8 Recording and Information System</p>
<?

echo $final_report;

if(!$success){
// no successful login
?>
  <div class='loginfield'> 
   <input type="text" name="username"  placeholder='Username' required autofocus class=form-control />
   <input type="password" name="password" placeholder='Password' required class=form-control />
  </div>
  <input type=hidden name=login value=login />
  <button class="btn btn-lg btn-primary btn-block" type="submit" data-loading-text='Signing in...' >Sign in</button>
<!--<input type="submit" name="login" value="Login" class='submitbutton' />-->

    <div class="container">


<?
}else{
// successful login
?>


<?
}
?>

</form>

</div>

	<script src="<?php echo ROOT_PATH;?>/js/jquery-1.11.1.min.js"></script>
	<script src='<?php echo ROOT_PATH;?>/js/jquery-ui-1.10.3.custom.min.js'></script>
	<script src='<?php echo ROOT_PATH;?>/js/jquery.timepicker.min.js'></script>
	<script src="<?php echo ROOT_PATH;?>/js/bootstrap.min.js"></script>
	<script src='<?php echo ROOT_PATH;?>/js/functions.js'></script>

<?php
require('footer.php');
?>
