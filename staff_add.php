<?php
require('header.php');
require('navmenutop.php');
?>

<div id=container>

<?php


if(isset($_POST['addnewstaff'])){
	foreach($_POST as $key=>$value){
		if(($key == "dob" || $key == 'date_started' || $key == 'app_form_date' || $key == 'selfdec_form_date' || $key == 'interview_date' || $key == 'dbs_expiry_date' || $key == 'ref1_date' || $key == 'ref2_date')&& ($value != ''&& $value != ' ')){
			$$key = date("Y-m-d",strtotime($value));
		}else if($key == 'password'){
			if($value != '')	$$key = crypt($value);
		}else{
			$$key = mres($value);
		}
	}
	
	// list of fields to insert
	$inserts = array('firstname','lastname','last_logged_in','staff','volunteer','role','timestamp','date_started','line_manager','status','category','job_title','projects','address','postcode','telephone','mobile','email','app_form_date','app_form_seen_by','selfdec_form_date','selfdec_form_seen_by','interview_date','interview_staff','dbs_number','dbs_expiry_date','ref1_date','ref1_seen_by','ref2_date','ref2_seen_by','vocational_quals','induction','agreement_given','driving_licence','qualifications','experience');
	if($needlogin) array_push($inserts,'username','password');
	//validate
	
	// NEEDS VALIDATION
	
	
	//insert into database
	$qs = "INSERT INTO staff SET ";
	foreach($inserts as $field){
		if($$field != '' && $$field != ' '){
			$qs .= "$field = '".$$field."', ";
		}
	}
	$q = trim($qs,", ");

	
	if($r = mysql_query($q)){echo "<meta http-equiv='refresh' content='0;url=".ROOT_PATH."/staff_view.php?added=".mysql_insert_id()."'>";exit;}else{echo "<p class=error>Error when adding staff member. ".mysql_error()."$qs $q</p>";}

}
//id	firstname	surname	username	password	last_logged_in	staff	volunteer	role	timestamp	date_started	line_manager

// id	firstname	lastname	username	password	last_logged_in	staff	volunteer	role	timestamp	date_started	line_manager	status 0 - inactive, 1 - active, 2 - expressed interest	category	job_title	projects	address	postcode	telephone	mobile	email	app_form_date	app_form_seen_by	selfdec_form_date	selfdec_form_seen_by	interview_date	interview_staff	dbs_number	dbs_expiry_date	ref1_date	ref1_seen_by	ref2_date	ref2_seen_by	vocational_quals	induction	agreement_given	driving_licence	qualifications	experience
?>

<form id=addstaff action="" method=post class=addnewform autocomplete=off>
<h1>Add New Worker or Volunteer</h1>
	<?php require('staff_form.php'); ?>
	
	<input type=hidden name='addnewstaff' value=1 />
	<div style='text-align:right;'><button type=submit class='btn btn-primary btn-medium'>Submit New Worker</button></div>
</form>
</div>
<script src='js/staff_functions.js'></script>
<script>
function validate(){
	//'firstname','lastname','username','password','last_logged_in','staff','volunteer','role','timestamp','date_started','line_manager','status','category','job_title','projects','address','postcode','telephone','mobile','email','app_form_date','app_form_seen_by','selfdec_form_date','selfdec_form_seen_by','interview_date','interview_staff','dbs_number','dbs_expiry_date','ref1_date','ref1_seen_by','ref2_date','ref2_seen_by','vocational_quals','induction','agreement_given','driving_licence','qualifications','experience'

}
$( "form#addstaff" ).submit(function( event ) {

	// make required fields if toggles are ticked
	
	if($('#app_form').prop('checked')){
		$('#app_form_date').addClass('required');
		$('#app_form_seen_by').addClass('required');
	}else{
		$('#app_form_date').removeClass('required');
		$('#app_form_seen_by').removeClass('required');
	}
	
	if($('#references').prop('checked')){
		$('#ref1_date').addClass('required');
		$('#ref1_seen_by').addClass('required');
		$('#ref2_date').addClass('required');
		$('#ref2_seen_by').addClass('required');
	}else{
		$('#ref1_date').removeClass('required');
		$('#ref1_seen_by').removeClass('required');
		$('#ref2_date').removeClass('required');
		$('#ref2_seen_by').removeClass('required');
	}
	
	if($('#selfdec').prop('checked')){
		$('#selfdec_form_date').addClass('required');
		$('#selfdec_form_seen_by').addClass('required');
	}else{
		$('#selfdec_form_date').removeClass('required');
		$('#selfdec_form_seen_by').removeClass('required');
	}
	
	if($('#dbs').prop('checked')){
		$('#dbs_number').addClass('required');
		$('#dbs_expiry_date').addClass('required');
	}else{
		$('#dbs_number').removeClass('required');
		$('#dbs_expiry_date').removeClass('required');
	}

	$('#addstaff :input').each(function(){
		if($(this).hasClass('required')){
			if($(this).val() == '' || $(this).val() == ' '){
				alert($(this).prev('label').html() +". This field is required!");
				$('.session_input.current').hide().removeClass('current');
				$(this).css('border','2px solid red').focus().parents('.session_input').show().addClass('current');
				event.preventDefault();
				return false;
			}
		}
		
		// check username
		
		// check passwords match
		if($('#needloginyes').prop('checked')){
		if($('#pw1').val() != $('#pw2').val()){
			alert("Your passwords do not match.");
			$('.session_input.current').hide().removeClass('current');
			$('#pw1').css('border','2px solid red').focus().parents('.session_input').show().addClass('current');
			event.preventDefault();
			return false;
		}
		}
		
		// check at least one of staff or volunteer are ticked
		if( !($('#stafftypestaff').prop('checked')) && !($('#stafftypevol').prop('checked'))){	
			alert("You must select at least one of Worker and Volunteer");
			$('.session_input.current').hide().removeClass('current');
			$('#stafftypestaff').focus().parents('.session_input').show().addClass('current');
		
			event.preventDefault();
			return false;
		}
		
		// check date validity

		
		
		return true;
	});
});
</script>
<?php
require('footer.php');
?>
