<?php

require('header.php');

require('navmenutop.php');


if(isset($_GET['sortby'])){
	$sortby = $_GET['sortby'];
}else{
	$sortby = 'display_order';
}
if(isset($_GET['direction'])){
	$direction = $_GET['direction'];
}else{
	$direction = 'ASC';
}


?>

<div id=container>

<?php
/*

if($_REQUEST['added']){$report .= "<p>New yp added (ID = ".$_REQUEST['added'].")</p>";
echo $report;
}*/

// add session type

if(isset($_REQUEST['ast'])){
	foreach($_POST as $key=>$value){
		$$key = mres($value);	
	}
	$q = "INSERT INTO session_types SET project_id = '".$ast_project."', session_name = '".$ast_name."', group_activity = '".$group."'";
	if($r = mysql_query($q)){$report = "Added new session type: $ast_name";

	}else{
		$report = "Error adding session type. $q".mysql_error();
	}
}

if(isset($_REQUEST['ap'])){
	foreach($_POST as $key=>$value){
		$$key = mres($value);	
	}
	$q = "INSERT INTO projects SET name = '".$ap_name."', active = '1', short_name = '".$ap_short_name."', project_leader = '".$ap_leader."'";
	if($r = mysql_query($q)){$report = "Added new project: $ap_name";

	}else{
		$report = "Error adding project. $q".mysql_error();
	}
}
?>

<div id=toolbar>
	<div class=first>
		<span style="font-weight: bold;font-size: 200%;vertical-align: middle;">Projects</span>
	</div>
	<div>
		<button class='btn btn-med btn-primary link-btn'><a href="javascript:window.print()">Print</a></button>
	</div>

</div>
<p id=report><?php echo $report?></p>


<?php

//get staff
$q = "SELECT * FROM staff WHERE staff = '1'";
$workers = array();
if($r = mysql_query($q)){
	while($worker = mysql_fetch_array($r)){
		$workers[] = $worker;
	}
}else{echo "Error getting staff. ".mysql_error();}

// show active projects
$projects = array();

echo "<table id=activeprojects class='cre8table table table-striped'>";
echo "<thead><tr><th>ID</th><th>Project Name</th><th>Short name</th><th>Session Types</th><th>Project Leader</th></tr></thead>";
$q = "
SELECT projects.id,projects.name,projects.short_name,staff.firstname as fn,staff.lastname as ln 
FROM projects 
LEFT JOIN staff ON staff.id = projects.project_leader 
WHERE projects.active = '1' 
ORDER BY projects.$sortby $direction";
if($r = mysql_query($q)){
	while($project = mysql_fetch_array($r)){
		$types = mysql_query("SELECT * FROM session_types WHERE project_id = '".$project['id']."'");
		$numtypes = mysql_num_rows($types);
		echo "<tr>";
		echo "<td rowspan=$numtypes>".$project['id']."</td>";
		echo "<td rowspan=$numtypes>".$project['name']."</td>";
		echo "<td rowspan=$numtypes>".$project['short_name']."</td>";
		$type = mysql_fetch_array($types);
		echo "<td>".$type['session_name']."</td>";

		echo "<td rowspan=$numtypes>".$project['fn']." ".$project['ln']."</td>";
		echo "</tr>";
		while($type = mysql_fetch_array($types)){echo "<tr><td>".$type['session_name']."</td></tr>";}
		$projects[] = $project;
	}
}else{
	echo "Error accessing project list. ".mysql_error();
}

echo "</table>";


// add session type
?>
<form id=add_session_type class=addproject method=post action='projects_view.php'>
	<fieldset><legend>Add Session Type</legend>
	<label>Add session type to project:</label><select name=ast_project class=form-control><option val='' disabled selected>- Select a project -</option>
	<?php
		foreach($projects as $project){
			echo "<option value = ".$project['id'].">".$project['name']."</option>";
		}
	?>
	</select><br />
	<label>Session Type name:</label><input type=text name=ast_name class=form-control /><br />
	<label>Is this an individual session or a group session?</label><input type=radio name=group value=0 /> Individual &nbsp;<input type=radio name=group value=1 /> Group<br />
	<button type=submit name=ast class='btn btn-med btn-primary'>Add new session type</button>
	</fieldset>
</form>

<form id=add_project_form class=addproject method=post action='projects_view.php'>
	<fieldset><legend>Add Project</legend>
	<label>Project Name:</label><input type=text name=ap_name class=form-control /><br />
	<label>Short Name:</label><input type=text name=ap_short_name class=form-control /><br />
	<label>Project Leader:</label><select name=ap_leader class=form-control><option val='' disabled selected>- Select a project leader -</option>
	<?php
		foreach($workers as $worker){
			echo "<option value = ".$worker['id'].">".$worker['firstname']." ".$worker['lastname']."</option>";
		}
	?>
	</select><br />

	<button type=submit name=ap class='btn btn-med btn-primary'>Add new project</button>
	</fieldset>
</form>
<?php
echo "<div class=floatbreak></div>";
// show session types
//view sessions based on project / session type / date / leader / young person


?>


<script src='<?php echo ROOT_PATH;?>/js/sessions_functions.js'></script>

<?php
require('footer.php');
?>