<?php
?>

		<div class='session_input'>
			<label>Project:</label>
			
				<?php
					$query = "SELECT id, name FROM projects WHERE active = '1' ORDER BY name ASC";
					if($result = mysql_query($query)){
						echo "<select name='project_id' id='project_id' required=true>";
						echo "<option disabled selected value=''> - Choose Project - </option>";
						while($project = mysql_fetch_array($result)){
							echo "<option value='".$project['id']."'>".$project['name']."</option>";						
						}
						echo "</select>";
					}else{echo "<div class='alert alert-warning'>Failed to get project list.</div>";}
				?>
		</div>
		<div class='session_input select_session start_hidden' >
			<label>Session Type:</label>
				<?php
					echo "<select name='session_type' id='session_type' required=true>";
					echo "<option disabled selected value=''> - Choose Session Type - </option>";
					echo "</select>";				
				?>
		</div>

		<div class='session_input start_hidden' id='sessionDatePair'>
			<label>Select Session Date and Time:</label>
			<input type="date" class="date start" name='session_date' id=session_date required max='<?echo date('Y-m-d',time());?>' value="" /> <input type="time" class="time start" step=900 id=stime name=session_start_time required=true /> to
			<input type="time" class="time end"  step=900 name='session_end_time' id=etime /> <input type="date" class="date end" name='session_end_date' id='session_end_date' max='<?echo date('Y-m-d',time());?>' value="" /> 
				
		</div>
		
		<div class='session_input start_hidden'>
			<label>Location:</label><input type='text' name='location' id=location required=true />
				
		</div>
		
		<div class='session_input start_hidden addsession'>

			<label>Workers and volunteers:</label>
			<?php
			echo "<input type=hidden name=staff_leader id=staff_leader value='".$_SESSION['CrisUserID']."' />";
			echo "<span style='display:none;' id=staff_leader_name>".$_SESSION['CrisFullName']."</span>";	
			?>
			<input type=text id=add_staff />
			<p class=helptext>Start typing the name of a worker/volunteer and then choose from the list.<br /><button id=uselaststaff class="btn btn-sm btn-primary">Use recent leaders</button></p>
			<table id='additionalstaff' class='sessiontable table'>
				<thead><tr class=toprow><th colspan=4>Leader List</th></tr></thead>
				<tbody>
				<tr class=noitem><td colspan=4>No leaders selected</td></tr>
				<tr class=loadingrow><td colspan=4>Loading... <img src='img/ajax-loader.gif' /></td></tr></tbody>
			</table>
			<input type=hidden name=staff_list id=staff_list />
				
		</div>

		<div class='session_input yp start_hidden addsession'>
			<label>Young People:</label><input id=addp type=text />
			<p class=helptext>Start typing the name of a young person and then choose from the list.<br />
			<button class='cleartable btn btn-sm btn-primary' onclick="clear_table('participants','parti_list');return false;">Clear list</button>
			<button class="btn btn-sm btn-primary" id=useconsent>Use this Project's YP</button>
			<button class="btn btn-sm btn-primary" id=uselast>Use Recent YP</button></p>
				<!--<button onclick='return false;' id='addpbutton'>Add Participant</button>-->
				<table id='participants' class='table sessiontable'>
					<tr class=toprow><th colspan=3>Participant List (<span id='num_yp' style='font-weight:bold;width:auto;display:inline;'>0</span> young people.)</th></tr>
					<tr>
					<tr class=noitem><td colspan=3>No participants selected</td></tr>
					<tr class=loadingrow><td colspan=3>Loading... <img src='img/ajax-loader.gif' /></td></tr>
				</table>
				<input type=hidden name=parti_list id=parti_list />
				<input type=hidden name=registered_list id=registered_list />
		</div>
		
		<div class='session_input start_hidden'>
			<label style='text-align:right;'>What Happened:<br />
			<ul class='indiv_guidance' style="font-size: 12px; font-weight: normal; text-align: left;">
				<li>How was the young person?</li>
				<li>What did you do?</li>
				<li>What was good / challenging?</li>
				<li>What did the young person produce?</li>
				<li>What evidence of this was collected, and where can it be found?</li>
			</ul>
			<ul class='group_guidance' style="font-size: 12px; font-weight: normal; text-align: left;">
				<li>Brief overview of activities.</li>
				<li>How was the group?</li>
				<li>Other significant details.</li>
			</ul>
			</label>
			<div style='display:inline-block;width:495px;vertical-align:middle;'>
			<input type=hidden id=splitnotes name=splitnotes value=0 />
			<textarea name=notesa id=notesa class=splitnotes placeholder='How was the young person?' title='How was the young person?'></textarea>
			<textarea name=notesb id=notesb class=splitnotes placeholder='What did you do?' title='What did you do?'></textarea>
			<textarea name=notesc id=notesc class=splitnotes placeholder='What was good / challenging?' title='What was good / challenging?'></textarea>
			<textarea name=notesd id=notesd class=splitnotes placeholder='What did the young person produce?' title='What did the young person produce?'></textarea>
			<textarea name=notese id=notese class=splitnotes placeholder='What evidence of this was collected, and where can it be found?'  title='What evidence of this was collected, and where can it be found?'></textarea>
			<textarea name=notes id=notes style='height:300px;' class=jointnotes required=true placeholder=""></textarea>
			</div>
			
			<!--
			// Uploading files section - not ready yet
			
			<label>Upload a picture, document or video as evidence?</label>
			<div style='display:inline-block;vertical-align:middle;' id=uploaddiv><input type=file name=upload_1 id=upload_1 class=upload_input /><br /><button type=button id=add_upload>Add another file</button>
			</div>
			<div id=already_uploaded>
			<label>Uploaded files already present:</label>
			<div style='display:inline-block;vertical-align:middle;width: 495px;' id=files><table></table></div>
			</div>
			
			-->
			<label></label>
			
			
		</div>
		<div class='session_input start_hidden'>
			<label>Are there any actions required after this session?</label>
			<input type=radio name=actionrequired id=actionyes value=1 onclick="$('#actiontext').show();" /> <label for=actionyes class=radio>Yes</label><input type=radio name=actionrequired id=actionno value=0 checked onclick="$('#actiontext').hide();" /> <label for=actionno class=radio>No</label>
		</div>
		<div class='session_input' id='actiontext' style='display:none;'>
			<label>Add actions required:</label><textarea name=actions id=actions></textarea>
			<!--<p class=helptext>Actions will be emailed to the project leader, and the session leader (if different).</p>-->
			<!--<label>Would you like to notify anyone else of this session and these actions?</label><input type=checkbox name=notify id=notifypllm value=pllm /> <label class=small for=notifylm>Line Manager of Project Leader</label><input type=checkbox name=notify id=notifysllm value=sllm /> <label class=small for=notifylm>Line Manager of Session Leader</label>
			<p class=helptext>This will cause the session record to be emailed to people you select.</p>-->
		</div>
		<div class='session_input start_hidden'>
			<label>Were there any safeguarding issues during this session?</label>
			<input type=radio name=sg id=sgyes value=1 onclick="$('#sgtext').show();" /> <label for=sgyes class=radio>Yes</label><input type=radio name=sg id=sgno value=0 checked onclick="$('#sgtext').hide();" /> <label for=sgno class=radio>No</label>
			<div id='sgtext' style='color:red;display:none;'>
				<p>You have said there were safeguarding issues during this session. You MUST follow the standard Safeguarding procedure and speak to the Project Leader after filling this form in (if you have not done so already). Do not enter sensitive information below, but use the standard Safeguarding Procedure. Enter the names or initials of the people involved in the box below, and where further details can be found. </p>
				<textarea name=sgdetails id=sgdetails></textarea>
			</div>
		</div>
		